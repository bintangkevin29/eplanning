<?php

namespace referensi\controllers;

use Yii;
use common\models\RefKomisiDprd;
use common\models\search\RefKomisiDprdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefKomisiDprdController implements the CRUD actions for RefKomisiDprd model.
 */
class RefKomisiDprdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefKomisiDprd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RefKomisiDprdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefKomisiDprd model.
     * @param string $Tahun
     * @param integer $Kd_Komisi
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Komisi)
    {
        return $this->render('view', [
            'model' => $this->findModel($Tahun, $Kd_Komisi),
        ]);
    }

    /**
     * Creates a new RefKomisiDprd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefKomisiDprd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Komisi' => $model->Kd_Komisi]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefKomisiDprd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param integer $Kd_Komisi
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Komisi)
    {
        $model = $this->findModel($Tahun, $Kd_Komisi);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Komisi' => $model->Kd_Komisi]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefKomisiDprd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param integer $Kd_Komisi
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Komisi)
    {
        $this->findModel($Tahun, $Kd_Komisi)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefKomisiDprd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param integer $Kd_Komisi
     * @return RefKomisiDprd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Komisi)
    {
        if (($model = RefKomisiDprd::findOne(['Tahun' => $Tahun, 'Kd_Komisi' => $Kd_Komisi])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
