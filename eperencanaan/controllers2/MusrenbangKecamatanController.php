<?php

namespace eperencanaan\controllers;

use Yii;
use eperencanaan\models\TaMusrenbang;
use eperencanaan\models\search\TaMusrenbangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\RefUrusan;
use common\models\TaSubUnit;
use common\models\RefKelurahan;
use common\models\RefLingkungan;
use common\models\RefKecamatan;
use common\models\RefBidangPembangunan;
use common\models\RefRPJMD;
use common\models\RefKecamatanKriteriaPembobotan;
use eperencanaan\models\TaMusrenbangRiwayat;
use common\models\RefSubUnit;

//use common\models\RefKamusUsulan;
use yii\helpers\Json;
use common\models\RefKamusUsulanBilbar;
use common\models\RefKamusUsulanBilhil;
use common\models\RefKamusUsulanBilhul;
use common\models\RefKamusUsulanPanhil;
use common\models\RefKamusUsulanPanhul;
use common\models\RefKamusUsulanPanteng;
use common\models\RefKamusUsulanPngkatan;
use common\models\RefKamusUsulanRansel;
use common\models\RefKamusUsulanRanut;


/**
 * TaMusrenbangController implements the CRUD actions for TaMusrenbang model.
 */
class MusrenbangKecamatanController extends Controller
{
    /**
     * @inheritdoc
     */

    public function NASarraymap($data) {
        $NASarray = [
            'Kd_Prov' => $data['Kd_Prov'],
            'Kd_Kab' => $data['Kd_Kab'],
            'Kd_Kec' => $data['Kd_Kec'],
            // 'Kd_Kel' => $data['Kd_Kel'],
            // 'Kd_Urut_Kel' => $data['Kd_Urut_Kel'],
            // 'Kd_Lingkungan' => $data['Kd_Lingkungan'],
        ];

        return $NASarray;
    }

    public function actionSatuan($satuan1) {
        $datasatuan = \common\models\RefStandardSatuan::findOne([ 'Kd_Satuan' => $satuan1 ]);
        echo Json::encode($datasatuan);
    }

    public function actionBidpem($bidpem1) {
        $databidpem = \common\models\RefBidangPembangunan::findOne([ 'Kd_Pem' => $bidpem1 ]);
        echo Json::encode($databidpem);
    }

     public function actionPriopem($priopem1) {
        $datapriopem = \common\models\TaRpjmdPrioritasPembangunanDaerah::findOne([ 'No_Prioritas' => $priopem1 ]);
        echo Json::encode($datapriopem);
    }

    public function actionPdaerah($skpd1) {
        $dataskpd = \common\models\Skpds::findOne([ 'id' => $skpd1 ]);
        //$dataskpd_arr = explode(".", $dataskpd->kode_skpd);

        // $Kd_Urusan = $dataskpd_arr[0];
        // $Kd_Bidang = $dataskpd_arr[1];
        // $Kd_Unit = $dataskpd_arr[2];

        // $unit = \common\models\RefUnit::find()
        //     ->where(['Kd_Urusan' => $Kd_Urusan])
        //     ->andwhere(['Kd_Bidang' => $Kd_Bidang])
        //     ->andwhere(['Kd_Unit' => $Kd_Unit]);
        //$dataskpd1 = $dataskpd->skpd;
            
        echo Json::encode($dataskpd);
    }

    public function actionUrusan1($skpd1) {
        $dataskpd = \common\models\Skpds::findOne([ 'id' => $skpd1 ]);
        $dataskpd_arr = explode("|", $dataskpd->kode_skpd);

        $Kd_Urusan = $dataskpd_arr[0];
        $Kd_Bidang = $dataskpd_arr[1];
        $Kd_Unit = $dataskpd_arr[2];
        $Kd_Sub = $dataskpd_arr[3];

            
        echo Json::encode($Kd_Urusan);
    }

    public function actionBidang1($skpd1) {
        $dataskpd = \common\models\Skpds::findOne([ 'id' => $skpd1 ]);
         $dataskpd_arr = explode("|", $dataskpd->kode_skpd);

        $Kd_Urusan = $dataskpd_arr[0];
        $Kd_Bidang = $dataskpd_arr[1];
        $Kd_Unit = $dataskpd_arr[2];
        $Kd_Sub = $dataskpd_arr[3];

            
        echo Json::encode($Kd_Bidang);
    }

    public function actionUnit1($skpd1) {
        $dataskpd = \common\models\Skpds::findOne([ 'id' => $skpd1 ]);
         $dataskpd_arr = explode("|", $dataskpd->kode_skpd);

        $Kd_Urusan = $dataskpd_arr[0];
        $Kd_Bidang = $dataskpd_arr[1];
        $Kd_Unit = $dataskpd_arr[2];
        $Kd_Sub = $dataskpd_arr[3];
            
        echo Json::encode($Kd_Unit);
    }

    public function actionSub1($skpd1) {
        $dataskpd = \common\models\Skpds::findOne([ 'id' => $skpd1 ]);
         $dataskpd_arr = explode("|", $dataskpd->kode_skpd);

        $Kd_Urusan = $dataskpd_arr[0];
        $Kd_Bidang = $dataskpd_arr[1];
        $Kd_Unit = $dataskpd_arr[2];
        $Kd_Sub = $dataskpd_arr[3];
            
        echo Json::encode($Kd_Sub);
    }

    
    // public function actionKamus($idKamus) {
    //     $kamus = \common\models\RefKamusUsulan::findOne([ 'id_kamus_usulan' => $idKamus ]);
    //     echo Json::encode($kamus);
    // }

    public function actionKamus1($idKamus1) {
        $kamus1 = \common\models\RefKamusUsulanRanut::findOne([ 'id_kamus_usulan' => $idKamus1 ]);
        echo Json::encode($kamus1);
    }

     public function actionKamus2($idKamus2) {
        $kamus2 = \common\models\RefKamusUsulanRansel::findOne([ 'id_kamus_usulan' => $idKamus2 ]);
        echo Json::encode($kamus2);
    }
     public function actionKamus3($idKamus3) {
        $kamus3 = \common\models\RefKamusUsulanBilbar::findOne([ 'id_kamus_usulan' => $idKamus3 ]);
        echo Json::encode($kamus3);
    }

    public function actionKamus4($idKamus4) {
        $kamus4 = \common\models\RefKamusUsulanBilhil::findOne([ 'id_kamus_usulan' => $idKamus4 ]);
        echo Json::encode($kamus4);
    }

     public function actionKamus5($idKamus5) {
        $kamus5 = \common\models\RefKamusUsulanBilhul::findOne([ 'id_kamus_usulan' => $idKamus5 ]);
        echo Json::encode($kamus5);
    }
     public function actionKamus6($idKamus6) {
        $kamus6 = \common\models\RefKamusUsulanPngkatan::findOne([ 'id_kamus_usulan' => $idKamus6 ]);
        echo Json::encode($kamus6);
    }

    public function actionKamus7($idKamus7) {
        $kamus7 = \common\models\RefKamusUsulanPanteng::findOne([ 'id_kamus_usulan' => $idKamus7 ]);
        echo Json::encode($kamus7);
    }

     public function actionKamus8($idKamus8) {
        $kamus8 = \common\models\RefKamusUsulanPanhil::findOne([ 'id_kamus_usulan' => $idKamus8 ]);
        echo Json::encode($kamus8);
    }
     public function actionKamus9($idKamus9) {
        $kamus9 = \common\models\RefKamusUsulanPanhul::findOne([ 'id_kamus_usulan' => $idKamus9 ]);
        echo Json::encode($kamus9);
    }

     public function Posisi() {
        $kelompok = Yii::$app->levelcomponent->getKelompok();
        $pos = [
            'Kd_Prov' => $kelompok['Kd_Prov'],
            'Kd_Kab' => $kelompok['Kd_Kab'],
            'Kd_Kec' => $kelompok['Kd_Kec'],
            // 'Kd_Kel' => $kelompok['Kd_Kel'],
            // 'Kd_Urut_Kel' => $kelompok['Kd_Urut_Kel']
        ];
        return $pos;
    }

    public function Kd_User() {
        $user = Yii::$app->user->identity->id;
        return $user;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaMusrenbang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaMusrenbangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaMusrenbang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaMusrenbang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $Tahun = Yii::$app->pengaturan->Kolom('Tahun');
        $Kd_Prov = Yii::$app->pengaturan->Kolom('Kd_Prov');
        $Kd_Kab = Yii::$app->pengaturan->Kolom('Kd_Kab');
        
        $user = (Yii::$app->user->identity->id);
        $model = new TaMusrenbang();
        $models = $this->NASarraymap(Yii::$app->levelcomponent->getKelompok());
        $Posisi = $this->Posisi();
     	
     	//$Iwankamus =  ArrayHelper::map(\common\models\RefKamusUsulan::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');

        $Iwankamus1 =  ArrayHelper::map(\common\models\RefKamusUsulanRanut::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus2 =  ArrayHelper::map(\common\models\RefKamusUsulanRansel::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus3 =  ArrayHelper::map(\common\models\RefKamusUsulanBilbar::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus4 =  ArrayHelper::map(\common\models\RefKamusUsulanBilhil::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus5 =  ArrayHelper::map(\common\models\RefKamusUsulanBilhul::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus6 =  ArrayHelper::map(\common\models\RefKamusUsulanPngkatan::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus7 =  ArrayHelper::map(\common\models\RefKamusUsulanPanteng::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus8 =  ArrayHelper::map(\common\models\RefKamusUsulanPanhil::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');
        $Iwankamus9 =  ArrayHelper::map(\common\models\RefKamusUsulanPanhul::find()->orderBy('nama_kamus_usulan')->all(),'id_kamus_usulan','nama_kamus_usulan');

        $Iwanskpd =  ArrayHelper::map(\common\models\Skpds::find()->orderBy('skpd')->all(),'id','skpd');

        $NASbidangpem = ArrayHelper::map(\common\models\RefBidangPembangunan::find()->all(), 'Kd_Pem', 'Bidang_Pembangunan');
        $NASsatuan = ArrayHelper::map(\common\models\RefStandardSatuan::find()->orderBy('Uraian')->all(), 'Kd_Satuan', 'Uraian');
        $NASrpjmd = ArrayHelper::map(\common\models\RefRPJMD::find()->where([ 'Tahun'=>$Tahun, 'Kd_Prov'=>$Kd_Prov, 'Kd_Kab'=>$Kd_Kab, ])->all(), 'Kd_Prioritas_Pembangunan_Kota', 'Nm_Prioritas_Pembangunan_Kota');
       
        // $unit =RefSubUnit::find()
        //     ->where(['NOT LIKE', 'Nm_Sub_Unit', 'Kecamatan'])
        //     ->andwhere(['!=', 'Nm_Sub_Unit', ''])
        //     ->orderby('Nm_Sub_Unit')
        //     ->all();

        // $dataunit = [];
        // foreach ($unit as $pil) {
        //     $val_skpd = $pil->Kd_Urusan."|".$pil->Kd_Bidang."|".$pil->Kd_Unit."|".$pil->Kd_Sub;
        //     $dataunit[$val_skpd]=$pil->Nm_Sub_Unit;
        // }
                    
        $model = new TaMusrenbang();
        $model->attributes = $models;
        $model->Tanggal = time();
        $model->Kd_User = $user;

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

         if ($model->load(Yii::$app->request->post())) {

            $Tahun = Yii::$app->pengaturan->getTahun();
          
            $model->Tahun = $Tahun;
            // print_r($model);exit;
            $model->Jumlah = str_replace(".", "", $model->Jumlah);
            $model->Harga_Satuan = str_replace(".", "", $model->Harga_Satuan);
            $model->Harga_Total = str_replace(".", "", $model->Harga_Total);
            $model->Kd_Asal_Usulan = '3';

            // $request = Yii::$app->request;
            // $skpd = $request->post('skpd');

            // $skpd_arr = explode("|", $skpd);
            
            // $model->Kd_Urusan = $skpd_arr[0];
            // $model->Kd_Bidang = $skpd_arr[1];
            // $model->Kd_Unit = $skpd_arr[2];
            // $model->Kd_Sub = $skpd_arr[3];


            if ($model->save(false)) {

                $model->Kd_User = $user;

                
                $ZULaftersimpan = new \eperencanaan\models\TaMusrenbangRiwayat();
                $ZULaftersimpan->attributes = $model->attributes;
                $ZULaftersimpan->Ta_Musrenbang_Id = $model->id;
                $ZULaftersimpan->Status_Survey = 5;
                $ZULaftersimpan->id_skpd = $model->id_skpd;
                $ZULaftersimpan->Keterangan = "Tambah Usulan";
                //$ZULaftersimpan->Tanggal = time();
                $ZULaftersimpan->save(false);



                Yii::$app->session->addFlash('success', 'Tambah Usulan Berhasil');

                return $this->redirect(['create']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'NASbidangpem' =>$NASbidangpem,
                'NASsatuan'=> $NASsatuan,
                'NASrpjmd' => $NASrpjmd,
                //'dataunit' => $dataunit,
                'NASsatuan' => $NASsatuan,
                //'Iwankamus' => $Iwankamus,
                'Iwankamus1' => $Iwankamus1,
                        'Iwankamus2' => $Iwankamus2,
                        'Iwankamus3' => $Iwankamus3,
                        'Iwankamus4' => $Iwankamus4,
                        'Iwankamus5' => $Iwankamus5,
                        'Iwankamus6' => $Iwankamus6,
                        'Iwankamus7' => $Iwankamus7,
                        'Iwankamus8' => $Iwankamus8,
                        'Iwankamus9' => $Iwankamus9,
                        'Iwanskpd' => $Iwanskpd,
            ]);
        }
    }

    /**
     * Updates an existing TaMusrenbang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaMusrenbang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaMusrenbang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaMusrenbang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaMusrenbang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //==============PADLI================//

    public function actionSkoring()
    {   
        $Tahun = Yii::$app->pengaturan->Kolom('Tahun');
        $Kd_Prov = Yii::$app->pengaturan->Kolom('Kd_Prov');
        $Kd_Kab = Yii::$app->pengaturan->Kolom('Kd_Kab');

        $posisi = $this->Posisi();

        $kelurahan = RefKelurahan::find()
                ->where($posisi)
                ->all(); 

        $bid_pem = RefBidangPembangunan::find()
                ->all();

        $rpjmd = RefRPJMD::find()
                ->where(['Tahun' => $Tahun, 'Kd_Prov' => $Kd_Prov, 'Kd_Kab' => $Kd_Kab])
                ->all();

        return $this->render('skoring', [
                    'kelurahan' => $kelurahan,
                    'bid_pem' => $bid_pem,
                    'rpjmd' => $rpjmd,
            ]);
    }

    public function actionGetUsulan()
    {   
        $Tahun = Yii::$app->pengaturan->Kolom('Tahun');
        $Kd_Prov = Yii::$app->pengaturan->Kolom('Kd_Prov');
        $Kd_Kab = Yii::$app->pengaturan->Kolom('Kd_Kab');
        
        $request = Yii::$app->request;

        $Kd_Asal_Usulan = $request->post('Kd_Asal_Usulan');
        $Kd_Kel = $request->post('Kd_Kel');
        $Kd_Lingkungan = $request->post('Kd_Lingkungan');
        $Kd_Pem = $request->post('Kd_Pem');
        $Kd_Prioritas_Pembangunan_Daerah = $request->post('Kd_Prioritas_Pembangunan_Daerah');

        $posisi = $this->Posisi();

        $data = TaMusrenbang::find()
                ->where($posisi)
                ->andwhere(['!=', 'Kd_Asal_Usulan', "3"]);
                //->andwhere(['IS', 'Status_Penerimaan_Kecamatan', NULL]);
        
        if ($Kd_Asal_Usulan != 0) {
            $data->andwhere(['=', 'Kd_Asal_Usulan', "$Kd_Asal_Usulan"]);
        }

        //echo $Kd_Asal_Usulan;
        //die();

        if ($Kd_Kel!=0) {
            $data->andwhere(['=', 'Kd_Urut_Kel', $Kd_Kel]);
        }

        if ($Kd_Lingkungan != 0) {
            $data->andwhere(['=', 'Kd_Lingkungan', $Kd_Lingkungan]);
        }

        if ($Kd_Pem!=0) {
            $data->andwhere(['=', 'Kd_Pem', $Kd_Pem]);
        }

        if ($Kd_Prioritas_Pembangunan_Daerah!=0) {
            $data->andwhere(['=', 'Kd_Prioritas_Pembangunan_Daerah', $Kd_Prioritas_Pembangunan_Daerah]);
        }

        $usulan = $data->all();

        $rpjmd = RefRPJMD::find()
                ->where(['Tahun' => $Tahun, 'Kd_Prov' => $Kd_Prov, 'Kd_Kab' => $Kd_Kab])
                ->all();

        $skpd = RefSubUnit::find()
                ->where(['NOT LIKE', 'Nm_Sub_Unit', 'Kecamatan'])
                ->andwhere(['!=', 'Nm_Sub_Unit', ''])
                ->orderby('Nm_Sub_Unit')
                ->all();

        return $this->renderpartial('get_usulan', [
                'data' => $usulan,
                'rpjmd' => $rpjmd,
                'skpd' => $skpd,
        ]);
    }

    public function actionSetPrioritas($id, $rpjmd, $alasan)
    {   
        $model = TaMusrenbang::findOne($id);
        $model->Kd_Prioritas_Pembangunan_Daerah = $rpjmd;
        $model->Alasan_Kecamatan = $alasan;
        // $model->Kd_Urusan = $Kd_Urusan;
        // $model->Kd_Bidang = $Kd_Bidang;
        // $model->Kd_Unit = $Kd_Unit;
        // $model->Kd_Sub = $Kd_Sub;

        if ($rpjmd == 0) {
            $model->Status_Penerimaan_Kecamatan = '3'; //ditolak oleh kecamatan
        }

        if($model->save()){
            $riwayat = new TaMusrenbangRiwayat();
            $riwayat->attributes = $model->attributes;
            $riwayat->Ta_Musrenbang_Id = $model->id;
            $riwayat->Keterangan = "Set Prioritas";
            $riwayat->save(false);
            echo "Prioritas Terpilih";
        }
        else{
            echo "Pilih Prioritas Gagal";
        }
    }

    public function actionSetSkpd($id, $skpd)
    {   
        $isi_skpd =  explode("|", $skpd);
        $Kd_Urusan = $isi_skpd[0];
        $Kd_Bidang = $isi_skpd[1];
        $Kd_Unit = $isi_skpd[2];
        $Kd_Sub = $isi_skpd[3];

        $model = TaMusrenbang::findOne($id);
        $model->Kd_Urusan = $Kd_Urusan;
        $model->Kd_Bidang = $Kd_Bidang;
        $model->Kd_Unit = $Kd_Unit;
        $model->Kd_Sub = $Kd_Sub;

        if($model->save()){
            $riwayat = new TaMusrenbangRiwayat();
            $riwayat->attributes = $model->attributes;
            $riwayat->Ta_Musrenbang_Id = $model->id;
            $riwayat->Keterangan = "Set SKPD";
            $riwayat->save(false);

            echo "SKPD Terpilih";
        }
        else{
            echo "Pilih SKPD Gagal";
        }
    }

    public function actionModalSkoring($id)
    {
        $kriteria = RefKecamatanKriteriaPembobotan::find()->all();

        $model = TaMusrenbang::findOne($id);
        $Kd_Pem = $model->Kd_Pem;
        $Bidang_Pembangunan = $model->bidangPembangunan->Bidang_Pembangunan;
        
        return $this->renderAjax('modal_skoring',[
                'kriteria' => $kriteria,
                'Kd_Pem' => $Kd_Pem,
                'Bidang_Pembangunan' => $Bidang_Pembangunan,
                'id' => $id,
           ]);
    }

    public function actionModalPrioritas($id, $rpjmd)
    {
        if ($rpjmd == 0) {
            $isi_pilihan = 'Non Prioritas';
        }
        else{
            $pilihan = RefRPJMD::findOne(['Kd_Prioritas_Pembangunan_Kota'=>$rpjmd]);
            $isi_pilihan = $pilihan->Nm_Prioritas_Pembangunan_Kota;
        }

        // $isi_skpd =  explode("|", $skpd);
        
        // $Kd_Urusan = $isi_skpd[0];
        // $Kd_Bidang = $isi_skpd[1];
        // $Kd_Unit = $isi_skpd[2];
        // $Kd_Sub = $isi_skpd[3];

        return $this->renderAjax('modal_prioritas',[
                'id' => $id,
                'rpjmd' => $rpjmd,
                'isi_pilihan' => $isi_pilihan,
                // 'Kd_Urusan' => $Kd_Urusan,
                // 'Kd_Bidang' => $Kd_Bidang,
                // 'Kd_Unit' => $Kd_Unit,
                // 'Kd_Sub' => $Kd_Sub,
           ]);
    }

    public function actionSkoringSimpan()
    {
        $request = Yii::$app->request;
        $bobot = $request->post('bobot');
        $id = $request->post('id');
        $skor = $request->post('skor');
        //echo $id;
        $data = serialize($bobot);
        //echo $data;
        $model = TaMusrenbang::findOne($id);
        //echo $model->id;
        $model->Rincian_Skor = $data;
        $model->Skor = $skor;
        $model->Status_Penerimaan_Kecamatan = '1'; //diterima
        //echo $model->Rincian_Skor;

        // $riwayat = new TaMusrenbangRiwayat();
        // $riwayat->attributes = $model->attributes;
        // $riwayat->Keterangan = "Skoring";
        // $riwayat->save(false);

        if ($model->save()) {
            $riwayat = new TaMusrenbangRiwayat();
            $riwayat->attributes = $model->attributes;
            $riwayat->Ta_Musrenbang_Id = $model->id;
            $riwayat->Keterangan = "Skoring";
            $riwayat->save(false);

            echo "Skor Disimpan";
        }
        else{
            echo "Simpan Skor Gagal";
        }
        
    }

    public function actionGetLingkungan($Kd_Kel)
    {
        $Posisi = $this->Posisi();
        $lingkungan = RefLingkungan::find()
                    ->where($Posisi)
                    ->andwhere(['=', 'Kd_Urut_Kel', $Kd_Kel])
                    ->all();
        
        echo '<option value="0">-Pilih Lingkungan-</option>';
        foreach ($lingkungan as $key => $value) {
            echo '<option value="'.$value->Kd_Lingkungan.'">'.$value->Nm_Lingkungan.'</option>';
        }
    }

    public function actionImportUsulan()
    {
        $Posisi = $this->Posisi();
        $usulan1 = \eperencanaan\models\TaMusrenbangKelurahan::find()
              ->leftJoin('Ta_Relasi_Musrenbang_Kelurahan', 'Ta_Relasi_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan = Ta_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan')
              ->where(['IS', 'Ta_Relasi_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan', NULL])
              ->andwhere($Posisi)
              ->all();

        $usulan2 = \eperencanaan\models\TaKelurahanVerifikasiUsulanLingkungan::find()
              ->where(['IN', 'Status_Penerimaan', [1,2]])
              ->andwhere($Posisi)
              ->all();

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            foreach ($usulan1 as $key => $value) {
                $musrenbang = new TaMusrenbang;
                $musrenbang->Tahun = $value->Tahun;
                $musrenbang->Kd_Prov = $value->Kd_Prov;
                $musrenbang->Kd_Kab = $value->Kd_Kab;
                $musrenbang->Kd_Kec = $value->Kd_Kec;
                $musrenbang->Kd_Kel = $value->Kd_Kel;
                $musrenbang->Kd_Urut_Kel = $value->Kd_Urut_Kel;
                $musrenbang->Kd_Lingkungan = $value->Kd_Lingkungan;
                $musrenbang->Kd_Jalan = $value->Kd_Jalan;
                $musrenbang->Kd_Urusan = $value->Kd_Urusan;
                $musrenbang->Kd_Bidang = $value->Kd_Bidang;
                $musrenbang->Kd_Prog = $value->Kd_Prog;
                $musrenbang->Kd_Keg = $value->Kd_Keg;
                $musrenbang->Kd_Unit = 0;
                $musrenbang->Kd_Sub = 0;
                $musrenbang->Kd_Pem = $value->Kd_Pem;
                $musrenbang->Nm_Permasalahan = $value->Nm_Permasalahan;
                $musrenbang->Kd_Klasifikasi = $value->Kd_Klasifikasi;
                $musrenbang->Jenis_Usulan = $value->Jenis_Usulan;
                $musrenbang->Jumlah = $value->Jumlah;
                $musrenbang->Kd_Satuan = $value->Kd_Satuan;
                $musrenbang->Harga_Satuan = $value->Harga_Satuan;
                $musrenbang->Harga_Total = $value->Harga_Total;
                $musrenbang->Kd_Sasaran = $value->Kd_Sasaran;
                $musrenbang->Detail_Lokasi = $value->Detail_Lokasi;
                $musrenbang->Latitute = $value->Latitute;
                $musrenbang->Longitude = $value->Longitude;
                $musrenbang->Tanggal = $value->Tanggal;
                $musrenbang->status = $value->status;
                $musrenbang->Status_Survey = $value->Status_Survey;
                $musrenbang->Kd_Prioritas_Pembangunan_Daerah = $value->Kd_Prioritas_Pembangunan_Daerah;
                //$musrenbang->Skor = NULL;
                //$musrenbang->Rincian_Skor = NULL;
                //$musrenbang->Status_Usulan = $value->Status_Usulan;
                $musrenbang->Status_Penerimaan_Kelurahan = 1;
                $musrenbang->Alasan_Kelurahan = '';
                //$musrenbang->Status_Penerimaan_Kecamatan = NULL;
                //$musrenbang->Alasan_Kecamatan = NULL;
                // $musrenbang->Status_Penerimaan_Skpd = NULL;
                // $musrenbang->Alasan_Skpd = NULL;
                // $musrenbang->Status_Penerimaan_Kota = NULL;
                // $musrenbang->Alasan_Kota = NULL;
                $musrenbang->Kd_User = $value->Kd_User;
                // $musrenbang->Kd_Asal = NULL;
                // $musrenbang->Kd1 = NULL;
                // $musrenbang->Kd2 = NULL;
                // $musrenbang->Kd3 = NULL;
                // $musrenbang->Kd4 = NULL;
                // $musrenbang->Kd5 = NULL;
                // $musrenbang->Kd6 = NULL;
                // $musrenbang->Uraian_Usulan = NULL;
                $musrenbang->Kd_Asal_Usulan = '2';
                
                $musrenbang->save(false);
            }

            foreach ($usulan2 as $key => $value) {
                $musrenbang = new TaMusrenbang;
                $musrenbang->Tahun = $value->Tahun;
                $musrenbang->Kd_Prov = $value->Kd_Prov;
                $musrenbang->Kd_Kab = $value->Kd_Kab;
                $musrenbang->Kd_Kec = $value->Kd_Kec;
                $musrenbang->Kd_Kel = $value->Kd_Kel;
                $musrenbang->Kd_Urut_Kel = $value->Kd_Urut_Kel;
                $musrenbang->Kd_Lingkungan = $value->Kd_Lingkungan;
                $musrenbang->Kd_Jalan = $value->Kd_Jalan;
                $musrenbang->Kd_Urusan = $value->Kd_Urusan;
                $musrenbang->Kd_Bidang = $value->Kd_Bidang;
                $musrenbang->Kd_Prog = $value->Kd_Prog;
                $musrenbang->Kd_Keg = $value->Kd_Keg;
                //$musrenbang->Kd_Unit = 0;
                //$musrenbang->Kd_Sub = 0;
                $musrenbang->Kd_Pem = $value->Kd_Pem;
                $musrenbang->Nm_Permasalahan = $value->Nm_Permasalahan;
                $musrenbang->Kd_Klasifikasi = $value->Kd_Klasifikasi;
                $musrenbang->Jenis_Usulan = $value->Jenis_Usulan;
                $musrenbang->Jumlah = $value->Jumlah;
                $musrenbang->Kd_Satuan = $value->Kd_Satuan;
                $musrenbang->Harga_Satuan = $value->Harga_Satuan;
                $musrenbang->Harga_Total = $value->Harga_Total;
                $musrenbang->Kd_Sasaran = $value->Kd_Sasaran;
                $musrenbang->Detail_Lokasi = $value->Detail_Lokasi;
                $musrenbang->Latitute = $value->Latitute;
                $musrenbang->Longitude = $value->Longitude;
                $musrenbang->Tanggal = $value->Tanggal;
                $musrenbang->status = $value->status;
                $musrenbang->Status_Survey = $value->Status_Survey;
                $musrenbang->Kd_Prioritas_Pembangunan_Daerah = $value->Kd_Prioritas_Pembangunan_Daerah;
                //$musrenbang->Skor = NULL;
                //$musrenbang->Rincian_Skor = NULL;
                //$musrenbang->Status_Usulan = $value->status;
                $musrenbang->Status_Penerimaan_Kelurahan = $value->Status_Penerimaan;
                $musrenbang->Alasan_Kelurahan = $value->Keterangan;
                //$musrenbang->Status_Penerimaan_Kecamatan = NULL;
                //$musrenbang->Alasan_Kecamatan = NULL;
                // $musrenbang->Status_Penerimaan_Skpd = NULL;
                // $musrenbang->Alasan_Skpd = NULL;
                // $musrenbang->Status_Penerimaan_Kota = NULL;
                // $musrenbang->Alasan_Kota = NULL;
                $musrenbang->Kd_User = $value->Kd_User;
                // $musrenbang->Kd_Asal = NULL;
                // $musrenbang->Kd1 = NULL;
                // $musrenbang->Kd2 = NULL;
                // $musrenbang->Kd3 = NULL;
                // $musrenbang->Kd4 = NULL;
                // $musrenbang->Kd5 = NULL;
                // $musrenbang->Kd6 = NULL;
                // $musrenbang->Uraian_Usulan = NULL;
                $musrenbang->Kd_Asal_Usulan = $value->Asal_Usulan;
                
                $musrenbang->save(false);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        echo "<h2>Load data Berhasil</h2>";
    }

    //==============AKHIR PADLI================//

    //==============NUGRA================//
    
    //==============AKHIR NUGRA================//
}
