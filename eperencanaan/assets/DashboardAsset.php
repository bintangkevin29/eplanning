<?php

namespace eperencanaan\assets;

use yii\web\AssetBundle;

/**
 * Main eperencanaan application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/default-blue-white.css',
        'css/dashboard-style.css',
    ];
    public $js = [
        'js/plugins/modernizr/modernizr.js',   
        'js/plugins/bootstrap/bootstrap.min.js',
        'js/dev-loaders.js',
        'js/dev-layout-default.js',
        'js/dev-app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
