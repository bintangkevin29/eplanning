<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\TaMusrenbang */

$this->title = 'Tambah Usulan Kecamatan';
$this->params['breadcrumbs'][] = ['label' => 'Musrenbang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-musrenbang-create">
  

    <?= $this->render('_form', [
        'model' => $model,
         'NASbidangpem' =>$NASbidangpem,
        'NASsatuan'=> $NASsatuan,
        'NASrpjmd' => $NASrpjmd,
        //'dataunit' => $dataunit,
        //'Iwankamus' => $Iwankamus,
        'Iwankamus1' => $Iwankamus1,
                        'Iwankamus2' => $Iwankamus2,
                        'Iwankamus3' => $Iwankamus3,
                        'Iwankamus4' => $Iwankamus4,
                        'Iwankamus5' => $Iwankamus5,
                        'Iwankamus6' => $Iwankamus6,
                        'Iwankamus7' => $Iwankamus7,
                        'Iwankamus8' => $Iwankamus8,
                        'Iwankamus9' => $Iwankamus9,
                        'Iwanskpd' => $Iwanskpd,
       
    ]) ?>

</div>
