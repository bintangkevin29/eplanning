<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model eperencanaan\models\TaMusrenbangKelurahan */

$Iwankamus = array();
?>

<div class="ta-musrenbang-kelurahan-create">

    <?= $this->render('_form', [
        'model' => $model,
        'NASbidangpem' =>$NASbidangpem,
        'NASsatuan'=> $NASsatuan,
        'NASrpjmd' => $NASrpjmd,
        'dataunit' => $dataunit,
        'data_kec' => $data_kec,
        'Iwankamus' => $Iwankamus,
    ]) ?>

</div>