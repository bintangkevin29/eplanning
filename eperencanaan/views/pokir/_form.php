<?php
$request = Yii::$app->request;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefStandardSatuan; 
use common\models\RefJalan; 
use eperencanaan\assets\MapAsset;

use common\models\RefKamusUsulan;
use yii\helpers\Json;
use kartik\select2\Select2;
//$user=Yii::$app->levelcomponent->getKelompok();


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\Lingkungan */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(
    '@web/js/sistem/jquery.number.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/lingkungan_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

MapAsset::register($this);
$this->registerJsFile(
    '@web/js/sistem/tambah_pokir_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

// $this->registerJsFile(
//     '@web/js/sistem/usulankec.js',
//     ['depends' => [\yii\web\JqueryAsset::className()]]
// );

// $Kd_Sub = array();
// $Kd_Bidang = array();
// $Kd_Asal = array();
// $dataSH = array();
// $Kd_1 = array();
// $Kd_2 = array();
// $Kd_3 = array();
// $Kd_4 = array();
// $Kd_5 = array();
// $Kd_6 = array();

$this->title = ($model->isNewRecord ? 'Tambah' : 'Edit').' Usulan';
$this->params['subtitle'] = 'Usulan';


$this->registerJs("
    $('#asalsatuan').on('click', function(e){
        e.preventDefault();
        $('#satuan').toggle();
    })
");

$this->registerJs("
    $('#asalmanual').on('click', function(e){
        e.preventDefault();
        $('#manual').toggle();
    })
");

$js="
$('#pilihkec').change(function(){
    pilihkec=$('#pilihkec').val();

     $.post('index.php?r=pokir/kamus1&pilihkec='+pilihkec, function(data, success){
         $('#kdKamus').html(data);
         alert(data);
     })
    //alert (pilihkec);
});


";
$this->registerJs($js, 4, 'pilihkec');
?>
<div class="ta-musrenbang-form">
<div class="row">
    <div class="col-md-12">
        <div class="box-widget widget-module">
            <div class="widget-container">
                <div class=" widget-block">
                   
               
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>

                    <?= $form->field($model, 'Kd_Kec')->dropdownList($data_kec,
                            ['prompt'=>'Pilih Kecamatan', 'class' => 'form-control select2-allow-clear','id'=>'pilihkec']);
                        ?>

                    <?= $form->field($model, 'Nm_Permasalahan')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'id_kamus_usulan')->dropDownList($Iwankamus,['prompt'=>'Kamus Usulan', 'id'=>'kdKamus'])->label('Kamus Usulan') ?>
                                     

                    <?= $form->field($model, 'Jenis_Usulan')->textInput(['maxlength' => true, 'id' => 'coba', 'readonly' => true]) ?>
        
                    <?= $form->field($model, 'Kd_Pem')->dropdownList(
                            $NASbidangpem, ['prompt' => 'Pilih Bidang', 'class' => 'form-control', 'id' => 'bidpem', 'readonly' => true]) 
                    ?>

                    <?= 
                        $form->field($model, 'Kd_Prioritas_Pembangunan_Daerah')->dropdownList(
                           $NASrpjmd,
                            ['prompt'=>'Pilih Prioritas', 'class' => 'form-control select2-allow-clear'])->label("Prioritas Pembangunan Daerah"); ?>
                    
                    <div class="form-group ">
                        <label class="control-label col-sm-3">OPD Penanggung jawab</label>
                        <div class="col-sm-6">
                            <?= Html::dropDownList('skpd', null, $dataunit, ['prompt'=>'Pilih SKPD','class' => 'form-control select2-allow-clear']) ?>
                        </div>
                    </div>

                        <?= $form->field($model, 'Jumlah')->textInput(['maxlength' => true, 'class' => 'form-control hitung', 'id' => 'jumlah', 'autocomplete' => 'off'])->label("Jumlah/Vol") ?>
                    
                        <?= $form->field($model, 'Kd_Satuan')->dropdownList($NASsatuan,
                            ['prompt'=>'Pilih Satuan', 'class' => 'form-control select2-allow-clear', 'id' => 'satuan', 'readonly' => true]);
                        ?>

                        <?= $form->field($model, 'Harga_Satuan')->textInput(['maxlength' => true, 'class' => 'form-control nomor hitung', 'id' => 'harga', 'autocomplete' => 'off', 'readonly' => true]) ?>

                    <?php /* $form->field($model, 'Harga_Satuan')->widget(MaskMoney::classname(), [
                                                                    'pluginOptions' => [
                                                                        'precision' => 2,
                                                                        'thousands' => '.',
                                                                        'decimal' => ',',
                                                                    ],
                                                                    'options' => [ 
                                                                        'id' => 'harga_satuan',
                                                                        'class' => 'hitung',
                                                                    ]
                                                                ]);
                         */                                       
                    ?>
 
                    <?= $form->field($model, 'Harga_Total')->textInput(['maxlength' => true, 'class' => 'form-control nomor', 'id' => 'total', 'readonly' => true]) ?>
                    <?php /* $form->field($model, 'Harga_Total')->widget(MaskMoney::classname(), [
                                                                    'pluginOptions' => [
                                                                        'precision' => 2,
                                                                        'thousands' => '.',
                                                                        'decimal' => ',',
                                                                    ],
                                                                    'options' => [ 
                                                                        'id' => 'total',
                                                                        'readonly' => true,
                                                                    ]
                                                                ]);  
                        */  
                    ?>

                        <?= $form->field($model, 'Detail_Lokasi')->textarea(['maxlength' => true]) ?>
                        
                        <div class="col-md-offset-3 col-md-6" id="peta" style="height: 300px;"></div>
                        <div class="clearfix"></div><br>

                        <?= $form->field($model, 'Latitute')->textInput(['maxlength' => true, 'id'=>'lat']) ?> 
                        <?= $form->field($model, 'Longitude')->textInput(['maxlength' => true, 'id'=>'lng']) ?>

                        <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <?= Html::submitButton('Simpan', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>    
    </div>
</div>
</div>

<?php
    
?>

<!-- modal musrenbang kelurahan -->
<!-- /.modal form -->
<?php
    $kamususulan = <<< JS
    $('#kdKamus').change(function(){
         var kdKamus = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kdKamus',{ kdKamus : kdKamus },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });

             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');    
                  
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                //$('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
                $('#priopem').attr('value',data3.No_Prioritas);
            });

           

         });
    });
JS;
$this->registerJs($kamususulan);
?>
