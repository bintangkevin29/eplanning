<?php

use yii\web\View;
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use kartik\select2\Select2;
use common\models\RefKecamatan;
use common\models\RefKelurahan;
use common\models\RefLingkungan;
use eperencanaan\models\TaForumLingkungan;
use common\components\Helper;
use yii\helpers\Url;

$this->registerCssFile(
        '@web/css/sistem/dashboard_style.css', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/sistem/jquery.number.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/sistem/dashboard_skrip.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerCssFile(
        '@web/css/dev-plugins/fullcalendar/fullcalendar.min.css', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerCssFile(
        '@web/css/calendar-style.css', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/plugins/fullcalendar/moment.min.js', ['depends' => [\yii\web\JqueryAsset::className()],
    'position' => View::POS_HEAD]
);

$this->registerJsFile(
        '@web/js/plugins/fullcalendar/jquery.min.js', ['depends' => [\yii\web\JqueryAsset::className()],
    'position' => View::POS_HEAD]
);

$this->registerJsFile(
        '@web/js/plugins/fullcalendar/fullcalendar.min.js', ['depends' => [\yii\web\JqueryAsset::className()],
    'position' => View::POS_HEAD]
);

$this->registerJsFile(
        '@web/js/plugins/fullcalendar/locale-all.js', ['depends' => [\yii\web\JqueryAsset::className()],
    'position' => View::POS_HEAD]
);

$this->registerJsFile(
        '@web/js/sistem/calendar_skrip.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/sistem/drepdrop-usulan.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);


$Kd_Kec = array();
$Kd_Kel = array();
$Kd_Lingkungan = array();
?>

<!-- page wrapper -->
<div class="dev-page">

    <!-- page header -->
    <div class="dev-page-header">

        <!--<div class="dph-logo">-->
<!--<marquee direction="left" align="center" scrollamount="5" scrolldelay="10" width="100%" height="30" onMouseout="this.start()" onMouseOver="this.stop()"><p style="text-align: center;"><span style="front-weight: bold;">Bapak/Ibu bisa mencoba aplikasi ini,kami akan mereset aplikasi pada jam yang telah ditentukan (12:00,16:00)di hari kerja</span></p></marquee>-->
	    <div class="dph-logo">
            <img src="img/logo.png" height="40">
            <span class="judul-logo">E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></span>
            <a class="dev-page-sidebar-collapse">
                <div class="dev-page-sidebar-collapse-icon">
                    <span class="line-one"></span>
                    <span class="line-two"></span>
                    <span class="line-three"></span>
                </div>
            </a>
        </div>

    </div>
    <!-- ./page header -->

    <!-- page container -->
    <div class="dev-page-container">
        <?php include "leftpage.php"; ?>
        <div class="dev-page-content"> 
            <div class="page-title" id="tour-step-4">
                <img src="img/headerperencanaan1.png" class="img-responsive">
            </div>
            <div class="wrapper-dashboard">
              <div class="row">
                <div class="col-md-3">
                    <div class="tile tile-success" data-toggle="modal" data-target="modal_usulan_lingkungan">
                        <?= number_format($usulanLing,"0", ",", ".") ?>
                        <p><h6>Total Usulan Rembuk Warga</h6></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile tile-success" data-toggle="modal" data-target="modal_musrenbang_kelurahan">
                        <?= number_format($usulanKel,"0", ",", ".") ?>
                        <p><h6>Total Usulan Musrenbang Desa/Kelurahan</h6></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile tile-success" data-toggle="modal" data-target="modal_musrenbang_kecamatan">
                        <?= number_format($usulanKec,"0", ",", ".") ?>
                        <p><h6>Total Usulan Musrenbang Kecamatan</h6></p>
                    </div>
                </div>
                <a href="index.php?r=dashboard/laporan-renja">
                    <div class="col-md-3">
                        <div class="tile tile-success" data-toggle="modal" data-target="modal_musrenbang_kecamatan">
                            <?= number_format($jumlahKegiatan,"0", ",", ".") ?>
                            <p><h6>Total Usulan Perangkat Daerah</h6></p>
                        </div>
                    </div>
                </a>
              </div>
            </div>

            <div class="wrapper-dashboard">
                <div class="row">
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <!-- <img src="http://www.kemendagri.go.id/media/logo/daerah/1271-kota-medan.jpg" alt="pemko medan"> -->
                      <div class="caption">
                      <p align="center"><span class="fa fa-comments fa-4x"></span></p>
                        <h3 align="center">Rembuk Warga</h3>
                        <p>Rembuk Warga merupakan wujud dari kedaulatan warga atau masyarakat untuk melakukan kontrol terhadap lembaga/ organisasi pengelola kegiatan pembangunan</p>
                        <p><button class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal_lingkungan">Login Dusun/Lingkungan</button></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                     <!--  <img src="http://www.kemendagri.go.id/media/logo/daerah/1271-kota-medan.jpg" alt="pemko medan"> -->
                      <div class="caption">
                      <p align="center"><span class="fa fa-users fa-4x"></span></p>
                        <h3 align="center">Musrenbang Desa/Kelurahan</h3>
                        <p>Musrenbang Desa/Kelurahan merupakan lembaga/ organisasi yang sebelumnya telah diberikan amanah oleh warga untuk mengelola kegiatan pembangunan.</p>
                        <p><button class="btn btn-info btn-block" data-toggle="modal" data-target="#modal_kelurahan">Login Desa/Kelurahan</button></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                    <!--   <img src="http://www.kemendagri.go.id/media/logo/daerah/1271-kota-medan.jpg" alt="pemko medan"> -->
                      <div class="caption">
                      <p align="center"><span class="fa fa-home fa-4x"></span></p>
                        <h3 align="center">Musrenbang Kecamatan</h3>
                        <p>Musrenbang Kecamatan adalah upaya untuk penanggulangan kemiskinan di lingkungan/desa dengan acuan Rencana Perencanaan Jangka menengah Daerah (RPJMD).</p>
                        <p><button class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_kecamatan">Login Kecamatan</button></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                   <!--    <img src="http://www.kemendagri.go.id/media/logo/daerah/1271-kota-medan.jpg" alt="pemko medan"> -->
                      <div class="caption">
                        <p align="center"><span class="fa fa-comment-o fa-4x"></span></p>
                        <h3 align="center">Pokok Pikiran</h3>
                        <p>Pokok Pikiran merupakan wujud dari kedaulatan warga untuk melakukan kontrol terhadap lembaga/ organisasi yang sebelumnya telah diberikan amanah oleh warga.</p>
                        <p><button class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_pokir">Login Pokir</button></p>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-3">
                    <div class="thumbnail">
                   <!--    <img src="http://www.kemendagri.go.id/media/logo/daerah/1271-kota-medan.jpg" alt="pemko medan"> -->
                      <div class="caption">
                        <p align="center"><span class="fa fa-calendar fa-4x"></span></p>
                        <h3 align="center">RKPD Dan KUA-PPAS</h3>
                        <p>Rencana Kerja Pemerintah Daerah (RKPD) merupakan penjabaran dari Rencana Pembangunan Jangka Menengah Daerah (RPJMD) dan dalam penyusunannya mengacu pada Rencana Kerja Pemerintah (RKP).</p>
                        <!--<p align="center"><?php //echo Html::a('Login RKPD', Url::toRoute('../../emusrenbang/web/'), ['class'=>'btn btn-primary btn-block']) ?></p>-->
                        <p align="center"><?php echo Html::Button('Login RKPD', array('onClick'=>"location='../../emusrenbang/web/'"), array('class'=>'btn btn-primary btn-block')) ?></p> 
                        <!-- <p><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_skpd">Login RKPD</button></a></p>-->
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            
            <!-- Sortable plugin -->
            <div class="wrapper  wrapper-white">
              <div class="col-md-12">
                <div class="page-subtitle">
                    <h3>Kalender Perencanaan</h3>
                    <p>Jadwal Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></p>
                </div>
                <div id='calendar'>
                    
                </div>
              </div>
            </div>
            <!-- summernote plugin -->
            <!-- Copyright -->
            <div class="copyright">
                <div class="pull-left">
                    &copy; 2017
                    <strong>BAPPEDA <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></strong>. All rights reserved.
                </div>
            </div>

        </div>
    </div>

    <!-- page footer -->    
    <div class="dev-page-footer dev-page-footer-fixed"> <!-- dev-page-footer-closed dev-page-footer-fixed -->
    </div>
    <!-- ./page footer -->
</div>

<!-- modal form -->
<div class="modal fade" id="modal_lingkungan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Rembuk Warga</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>

                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-lingkungan'],
                                ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_kelurahan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Musrenbang Desa/Kelurahan</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                 <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-musrenbang-kelurahan'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_kecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Musrenbang Kecamatan</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-musrenbang-kecamatan'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_skpd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Perangkat Daerah</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-skpd'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_pokir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login POKIR</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-pokir'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_kota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Kabupaten</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-kota'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_bappeda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Bappeda</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => ['dashboard/login-bappeda'],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<div class="modal fade" id="modal_rkpd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Perangkat Daerah</h4>
            </div>
            <div class="modal-body">
                <p>Gunakan Akun anda yang sudah terdaftar</p>
                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'action' => [''],
                            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php // $form->field($model, 'rememberMe')->checkbox()  ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- pagu indikatif kecamatan-->
<div class="modal fade" id="modal_pagu_kecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Rincian Pagu Indikatif Per Kecamatan</h4>
            </div>
            <div class="modal-body">
                <div>Total Pagu Indikatif : <span class="pagu">Rp. <span class="rupiah"><?= $paguKec ?></span></span></div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Kecamatan</th>
                            <th>Nominal (Rp.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
//print_r($PC_PaguIndikatifKecamatanGroup);
                        $no = 0;
                        $PC_data_penggunaan = [];/*
                        foreach ($PC_PaguIndikatifKecamatanGroup as $value) :
                            ?>
                            <?php $no++; ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $value->kdKec->Nm_Kec ?></td>
                                <td class='uang'><?= $value->Pagu_Indikatif ?></td>
                            </tr>
                        <?php endforeach */?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- pagu indikatif kecamatan -->
<div class="modal fade" id="modal_pagu_penggunaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Rincian Penggunaan Pagu Indikatif Per Kecamatan</h4>
            </div>
            <div class="modal-body">
                <div>
                    Total  Penggunaan Pagu Indikatif : <span class="pagu">Rp. <span class="rupiah"><?= $totalPaguLing ?></span></span>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Kecamatan</th>
                            <th>Nominal (Rp.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        /*
                          $no = 0;
                          foreach ($PC_PenggunaanPagu as $key => $value) {
                          $no++;
                          $Kd_Kec = $value['Kd_Kec'];
                          $spi = $value['spi'];
                          $PC_data_penggunaan[$Kd_Kec]=$spi;
                          echo "
                          <tr>
                          <td>".$no."</td>
                          <td>".$Kd_Kec."</td>
                          <td class='uang'>".$spi."</td>
                          </tr>
                          ";
                          }
                         */
                        ?>
                        <?php
                        $no = 0;/*
                        foreach ($PC_PenggunaanPagu as $val) :
                            ?>
                            <?php $no++; ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $val['Kd_Kec'] ?></td>
                                <td class='uang'><?= $val['cnt'] ?></td>
                            </tr>
                        <?php endforeach */?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- Sisa pagu indikatif per kecamatan
<div class="modal fade" id="modal_pagu_sisa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sisa Pagu Indikatif Per Kecamatan</h4>
            </div>
            <div class="modal-body">
                <?php
//print_r($NS_PenggunaanPagu);
//print_r($PC_PaguIndikatifKecamatanGroup);
//print_r($data_penggunaan);
                ?>
                <div>
                    Sisa Pagu Indikatif : <span class="pagu">Rp. <span class="rupiah"><?= $paguKec - $totalPaguLing ?></span></span>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Kecamatan</th>
                            <th>Nominal Pagu (Rp.)</th>
                            <th>Penggunaan</th>
                            <th>Sisa (Rp.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
//print_r($PC_PaguIndikatifKecamatanGroup);
                        /*
                          $no = 0;
                          foreach ($PC_PaguIndikatifKecamatanGroup as $key => $value) {
                          $no++;
                          $Kd_Kec = $value['Kd_Kec'];
                          $pi2 = $value['Pagu_Indikatif'];
                          $penggunaan=$PC_data_penggunaan[$Kd_Kec];
                          $sisa = $pi2 - $penggunaan;
                          echo "
                          <tr>
                          <td>".$no."</td>
                          <td>".$Kd_Kec."</td>
                          <td class='uang'>".$pi2." </td>
                          <td class='uang'>".$penggunaan." </td>
                          <td class='uang'>".$sisa." </td>
                          </tr>
                          ";
                          }
                         */
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal form -->
<div class="modal fade" id="modal_usulan_lingkungan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Usulan Lingkungan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Kecamatan</label>
                    <div class="col-md-8">
                        <select class="form-control" id="drop_kec">
                            <?php
                            echo "<option value=0>Pilih Kecamatan</option>";
                            foreach ($dataKec as $d) {
                                echo "<option value=" . $d['Kd_Kec'] . ">" . $d['Nm_Kec'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Kelurahan/Desa</label>
                    <div class="col-md-8">
                        <select class="form-control" id="drop_kel">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Lingkungan</label>
                    <div class="col-md-8">
                        <select class="form-control" id="drop_ling">
                        </select>
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Permasalahan</th>
                            <th>Usulan</th>
                            <th>Jumlah</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody id="isi_usulan"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal musrenbang kelurahan -->
<div class="modal fade" id="modal_musrenbang_kelurahan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Usulan Kelurahan/Desa</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kecamatan</label>
                        <div class="col-md-8">
                            <select class="form-control">
                                <option>Kecamatan A</option>
                                <option>Kecamatan B</option>
                                <option>Kecamatan C</option>
                                <option>Kecamatan D</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kelurahan/Desa</label>
                        <div class="col-md-8">
                            <select class="form-control">
                                <option>Kelurahan A</option>
                                <option>Kelurahan B</option>
                                <option>Kelurahan C</option>
                                <option>Kelurahan D</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <button type="submit" class="btn btn-default">Lihat</button>
                        </div>
                    </div>
                </form>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Permasalahan</th>
                            <th>Usulan</th>
                            <th>Jumlah</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->

<!-- modal musrenbang kelurahan -->
<div class="modal fade" id="modal_musrenbang_kecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Usulan Kelurahan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kecamatan</label>
                        <div class="col-md-8">
                            <select class="form-control">
                                <option>Kecamatan A</option>
                                <option>Kecamatan B</option>
                                <option>Kecamatan C</option>
                                <option>Kecamatan D</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <button type="submit" class="btn btn-default">Lihat</button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal form -->
