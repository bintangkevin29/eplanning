<?php

use yii\helpers\Html;
use common\components\Helper;
use yii\grid\GridView;
use yii\widgets\LinkPager;

$this->registerJsFile(
    '@web/js/musrenbang/lihat_usulan.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>

<!-- page wrapper -->
<div class="dev-page">
    
    <!-- page header -->    
    <div class="dev-page-header">
        <div class="dph-logo">
            <img src="img/logo.png" height="40">
            <span class="judul-logo">E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></span>
            <a class="dev-page-sidebar-collapse">
                <div class="dev-page-sidebar-collapse-icon">
                    <span class="line-one"></span>
                    <span class="line-two"></span>
                    <span class="line-three"></span>
                </div>
            </a>
        </div>
    </div>
    <!-- ./page header -->
    
    <!-- page container -->
    <div class="dev-page-container">

        <!-- page sidebar -->
        <?php include "leftpage.php"; ?>    
        <!-- ./page sidebar -->
        
        <!-- page content -->
        <div class="dev-page-content">                    
            <!-- page content container -->
            <div class="container">

                <!-- page title -->
                <div class="page-title" id="tour-step-4">
                    <h1>E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></h1>
                    <h5>E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?> merupakan sebuah aplikasi yang dibangun untuk membuat rencana pembangunan daerah mulai dari tingkat lingkungan sampai kota.</h5>
                </div>
                <!-- ./page title -->
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper wrapper-white">
                            <div class="page-subtitle">
                                <h3>Informasi Usulan Pokir</h3>
                            </div>
                            <!-- <div class="row">
                                <form id="form_cari">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >Daerah Pemilihan</label>
                                            <select class="form-control" name="Kd_Dapil" id="dapil">
                                                <option value="">- Pilih Daerah Pemilihan -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >User Daerah Pemilihan</label>
                                            <select class="form-control" name="Kd_User_Dapil" id="user-dapil">
                                                <option value="">- Pilih User Daerah Pemilihan -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label >&nbsp;</label>
                                            <button type="button" class="form-control btn btn-success" id="btn-lihat-pokir">
                                            <span><i class="fa fa-search pull-right"></i></span>Lihat</button>
                                        </div>
                                    </div>
                                </form>
                            </div> -->
                            
                            
                            <div class="row" id="isi-wrap">
                                <div class="box-body">
                                    <?= 
                                        GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            [   
                                                'attribute' => 'Nm_Permasalahan',
                                                'label' => 'Permasalahan',
                                            ],
                                            [   
                                                'attribute' => 'Jenis_Usulan',
                                                'label' => 'Usulan',
                                            ],
                                            [
                                                'attribute' => 'Kd_Pem',
                                                'value' => 'bidangPembangunan.Bidang_Pembangunan',
                                                'filter' => $data_bidpem,
                                            ],
                                            [
                                                'attribute' => 'Kd_Prioritas_Pembangunan_Daerah',
                                                'value' => 'rpjmd.Nm_Prioritas_Pembangunan_Kota',
                                                'filter' => $data_rpjmd,
                                            ],
                                            // [
                                            //     'attribute' => 'Kd_Sub',
                                            //     'value' => 'refSubUnit.Nm_Sub_Unit',
                                            // ],
                                            'Jumlah',
                                            [
                                                'label' => 'Satuan',
                                                'value' => 'satuan.Uraian',
                                            ],
                                            [
                                                'attribute' => 'Kd_Dapil',
                                                'value' => 'dapil.Nm_Dapil',
                                                'filter' => $data_dapil,
                                            ],

                                                     [
                                                'header' => 'Dokumen',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                   
                                                    if (isset($model->taPokirMedia->Kd_User))
                                                    {
                                                        
                                                        $tombols ='';

                                                        $foto = $model->taPokirMedia->all();

                                                        foreach ($foto as $value) {

                                                            $Jenis_Media = $value->kdMedia->Jenis_Media;
                                                            $Nm_Media = $value->kdMedia->Nm_Media;

                                                            $url = "index.php?r=dashboard/lihat-file&nama_file=".$Nm_Media;

                                                            $tombols .= '<button type="button" class="btn btn-primary btn-xs lihat_file" data-url="'.$url.'">'.$Jenis_Media.'</button>';
                                                        }

                                                        return $tombols;
                                                    }
                                                    else {
                                                        return '-';
                                                    }
                                                    
                                                },  
                                            ],
                                            //'Harga_Satuan',
                                            //'Harga_Total:ntext',
                                        
                                        ],
                                        ]); 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Copyright -->
                <div class="copyright">
                    <div class="pull-left">
                         &copy; <?= Yii::$app->pengaturan->getTahun() ?> <strong>BAPPEDA <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></strong>. All rights reserved.
                    </div>
                </div>
                <!-- ./Copyright -->

            </div>
            <!-- ./page content container -->
                                
        </div>
        <!-- ./page content -->                                               
    </div>  
    <!-- ./page container -->
    
    <!-- page footer -->    
    <div class="dev-page-footer dev-page-footer-fixed"> <!-- dev-page-footer-closed dev-page-footer-fixed -->
    </div>
    <!-- ./page footer -->
    
</div>
<!-- ./page wrapper -->