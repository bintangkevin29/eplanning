<?php
$request = Yii::$app->request;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefStandardSatuan; 
use common\models\RefJalan; 
use common\models\RefRPJMD;

//use common\models\RefKamusUsulan;
use yii\helpers\Json;
use kartik\select2\Select2;

use common\models\RefKamusUsulanBilbar;
use common\models\RefKamusUsulanBilhil;
use common\models\RefKamusUsulanBilhul;
use common\models\RefKamusUsulanPanhil;
use common\models\RefKamusUsulanPanhul;
use common\models\RefKamusUsulanPanteng;
use common\models\RefKamusUsulanPngkatan;
use common\models\RefKamusUsulanRansel;
use common\models\RefKamusUsulanRanut;
//$user=Yii::$app->levelcomponent->getKelompok();


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\Lingkungan */
/* @var $form yii\widgets\ActiveForm */
/*
$this->registerJsFile(
    '@web/js/sistem/jquery.number.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/jquery.priceformat.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
*/
$this->registerJsFile(
    '@web/js/sistem/jquery.number.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/lingkungan_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->title = 'Tambah Usulan Musrenbang Desa/Kelurahan';
$this->params['subtitle'] = 'Usulan';

//$this->params['breadcrumbs'][] ='';
$this->params['breadcrumbs'][] = ['label' => 'Usulan', 'url' => ['lingkungan/usulan']];
$this->params['breadcrumbs'][] = $this->title;

//$pesan = $request->get('pesan');
$PC_Kelompok = Yii::$app->levelcomponent->getKelompok();
$PC_InfoUser = Yii::$app->levelcomponent->getProfile();
$PC_NamaLingkungan =  Yii::$app->levelcomponent->getNamaLingkungan();
$nama_user='';
$nama_lingkungan='';
$Nm_Kec='';
$Nm_Kec2 = '';
$Nm_Kel='';

?>
<div class="alert alert-info">
  <strong>Silahkan menambahkan Usulan Dusun/Lingkungan jika ada usulan dusun/lingkungan yang belum diakomodir.
  </strong><br>
    <i>Setelah Anda menambah Usulan Dusun/Lingkungan, Silahkan periksa usulan dusun/lingkungan yang baru ditambahkan di Menu "Rekapitulasi Usulan Dusun/Lingkungan".</i>          
        </div>
<div class="row">
    <div class="col-md-12">
        <div class="box-widget widget-module">
            <div class="widget-container">
                <div class=" widget-block">
                    <?php 
                        //'Tahun', 'Kd_Prov', 'Kd_Kab', 'Kd_Kec', 'Kd_Kel', 'Kd_Urut_Kel', 
                        //'Kd_Lingkungan', 'Kd_Jalan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 
                        //'Kd_Keg', 'Nm_Permasalahan', 'Kd_Klasifikasi', 'Jenis_Usulan', 
                        //'Jumlah', 'Kd_Satuan', 'Harga_Satuan', 'Harga_Total', 'Kd_Sasaran', 'Tanggal' 
                    ?>
                    <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>

                   
                    <?= $form->field($model, 'Kd_Ta_Forum_Lingkungan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>

                    

                    <?= $form->field($model, 'Kd_Urusan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Bidang', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Prog', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Keg', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Klasifikasi', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Sasaran', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Status_Usulan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Status_Pembahasan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>

                    <div class="form-group required">
                        <label class="control-label col-sm-3" for="total"></label>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-warning" title="Tambahkan Jalan Jika Belum ada" data-toggle="modal" data-target="#modal_jalan">Tambah Jalan</button>
                            *) Tambahkan Jalan Bila Tidak ditemukan
                        </div>
                    </div>

                    <?= $form->field($model, 'Kd_Lingkungan')->dropDownList($ZULRefLingkungan,
                            ['id' => 'link',
                            'prompt' => 'Pilih Lingkungan',
                            'onchange' => '(function (){'
                                . ' var value = $("#link").val();
                                    $.ajax({
                                    url: "' . Yii::$app->urlManager->createUrl('ajax/zul-jalan') . '",
                                    data: {value: value},
                                    type: "GET",
                                    success: function(data) {
                                    $("#jalan").html(data);
                                    }
    });                         })()'])->label('Lingkungan'); ?>

                    <?php
                    //$dl_jalan = Yii::$app->levelcomponent->getKelompok();
                    echo $form->field($model, 'Kd_Jalan')->dropdownList([]
                                   , ['id'=>'jalan','prompt' => 'Pilih Jalan', 'class' => 'form-control select2-allow-clear'])->label("Jalan");
                    ?>

                    <?= $form->field($model, 'Nm_Permasalahan')->textarea(['maxlength' => true])->label("Nama Permasalahan"); ?>

                     <?php
                        if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Utara Coba'){
                            $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                          //$Nm_Kec2 = "Berhasil Kecamatan 1";
                    ?>
                                        
                                        <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                            'data'=>$Iwankamus1,
                                            'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus1'],
                                            'pluginOptions' => ['select2-allow-clear'=>true],

                                        ])->label('Kamus Usulan'); ?>
                    <?php
                      
                    }?>
                    <?php

                if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Selatan Coba'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 2";
                ?>
                                
                                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus2,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus2'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php

                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Barat Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 3";
                ?>
                    
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus3,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus3'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hilir Coba'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 4";
                ?>
                    
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus4,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus4'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hulu Coba'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 5";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus5,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus5'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Pangkatan Coba'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 6";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus6,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus6'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Tengah Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 7";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus7,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus7'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hilir Coba'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 8";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus8,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus8'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hulu Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 9";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Kecamatan Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 10";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9, 
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

                    <?= $form->field($model, 'Jenis_Usulan')->textInput(['maxlength' => true, 'id' => 'coba', 'readonly' => true]) ?>

                    <?= $form->field($model, 'Kd_Pem')->dropdownList(
                            $NASbidangpem, ['prompt' => 'Pilih Bidang', 'class' => 'form-control', 'id' => 'bidpem', 'readonly' => true]) 
                    ?>

                     <?= 
                        $form->field($model, 'Kd_Prioritas_Pembangunan_Daerah')->dropdownList(
                           $NASrpjmd,
                            ['prompt'=>'Pilih Prioritas', 'class' => 'form-control select2-allow-clear','id' => 'priopem','readonly' => true])->label("Prioritas Pembangunan Daerah");
                    ?>

                    <?= $form->field($model, 'Jumlah')->textInput(['maxlength' => true, 'class' => 'form-control hitung', 'id' => 'jumlah', 'autocomplete' => 'off']) ?>

                     <?= $form->field($model, 'Kd_Satuan')->dropdownList(
                            $NASsatuan, ['prompt' => 'Pilih Satuan', 'class' => 'form-control', 'id' => 'satuan', 'readonly' => true]) 
                    ?>

                    <?= $form->field($model, 'Harga_Satuan')->textInput(['maxlength' => true, 'class' => 'form-control nomor hitung', 'id' => 'harga', 'autocomplete' => 'off', 'readonly' => true]) ?>

 
                    <?= $form->field($model, 'Harga_Total')->textInput(['maxlength' => true, 'class' => 'form-control nomor', 'id' => 'total', 'readonly' => true]) ?>
                    ?>



                    <?php $form->field($model, 'Detail_Lokasi')->textarea(['maxlength' => true])  ?>
                   
                    <!--
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <?= Html::submitButton('Tambah', ['class' =>  'btn btn-primary']) ?>
                    </div>
                    -->
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                    <?php
                        /*
                        'Tahun' => 'Tahun',
                        'Kd_Ta_Forum_Lingkungan' => 'Kd  Ta  Forum  Lingkungan',
                        'Kd_Prov' => 'Kd  Prov',
                        'Kd_Kab' => 'Kd  Kab',
                        'Kd_Kec' => 'Kd  Kec',
                        'Kd_Kel' => 'Kd  Kel',
                        'Kd_Urut_Kel' => 'Kd  Urut  Kel',
                        'Kd_Lingkungan' => 'Kd  Lingkungan',
                        'Kd_Jalan' => 'Jalan',
                        'Kd_Urusan' => 'Kd  Urusan',
                        'Kd_Bidang' => 'Kd  Bidang',
                        'Kd_Prog' => 'Kd  Prog',
                        'Kd_Keg' => 'Kd  Keg',
                        'Nm_Permasalahan' => ' Permasalahan',
                        'Kd_Klasifikasi' => 'Kd  Klasifikasi',
                        'Jenis_Usulan' => 'Jenis  Usulan',
                        'Jumlah' => 'Jumlah',
                        'Kd_Satuan' => 'Satuan',
                        'Harga_Satuan' => 'Harga  Satuan',
                        'Harga_Total' => 'Harga  Total',
                        'Kd_Sasaran' => 'Kd  Sasaran',
                        */
                    ?>
                </div>
            </div>
        </div>
       
    </div>

</div>


<!-- modal musrenbang kelurahan -->
<div class="modal fade" id="modal_jalan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'action' => ['ta-musrenbang-kelurahan/tambah-jalan'],
                        ]) 
          ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Jalan</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="kd_usulan" id="kd_usulan_input">
                <div class="form-group">
                  <label >Lingkungan</label>
                  <select class="form-control" name="Kd_Lingkungan">
                      <?php
                        foreach ($lingkungan as $key => $val) :
                        ?>
                            <option value='<?= $val->Kd_Lingkungan ?>'><?= $val->Nm_Lingkungan ?></option>
                        <?php
                        endforeach;
                      ?>
                  </select>
                </div>
                <div class="form-group">
                  <label >Nama Jalan</label>
                  <input type="text" name="Nama_Jalan" class="form-control" placeholder="Nama Jalan">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tutup</button>
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
            </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!-- /.modal form -->
<?php
    $kamususulan1 = <<< JS
    $('#kdKamus1').change(function(){
         var idKamus1 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus1',{ idKamus1 : idKamus1 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            
             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

             
         });
    });
JS;
$this->registerJs($kamususulan1);
?>


<?php
    $kamususulan2 = <<< JS
    $('#kdKamus2').change(function(){
         var idKamus2 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus2',{ idKamus2 : idKamus2 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');  
                $('#jumlah').attr('value',0);          
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan2);
?>


<?php
    $kamususulan3 = <<< JS
    $('#kdKamus3').change(function(){
         var idKamus3 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus3',{ idKamus3 : idKamus3 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan3);
?>


<?php
    $kamususulan4 = <<< JS
    $('#kdKamus4').change(function(){
         var idKamus4 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus4',{ idKamus4 : idKamus4 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr(0);         
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan4);
?>


<?php
    $kamususulan5 = <<< JS
    $('#kdKamus5').change(function(){
         var idKamus5 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus5',{ idKamus5 : idKamus5 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan5);
?>


<?php
    $kamususulan6 = <<< JS
    $('#kdKamus6').change(function(){
         var idKamus6 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus6',{ idKamus6 : idKamus6 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan6);
?>


<?php
    $kamususulan7 = <<< JS
    $('#kdKamus7').change(function(){
         var idKamus7 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus7',{ idKamus7 : idKamus7 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan7);
?>


<?php
    $kamususulan8 = <<< JS
    $('#kdKamus8').change(function(){
         var idKamus8 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus8',{ idKamus8 : idKamus8 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan8);
?>


<?php
    $kamususulan9 = <<< JS
    $('#kdKamus9').change(function(){
         var idKamus9 = $(this).val();
         
         $.get('index.php?r=ta-musrenbang-kelurahan/kamus9',{ idKamus9 : idKamus9 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;

             $.get('index.php?r=ta-musrenbang-kelurahan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');    
                $('#jumlah').attr('value',0);        
            });

            $.get('index.php?r=ta-musrenbang-kelurahan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

         });
    });
JS;
$this->registerJs($kamususulan9);
?>
