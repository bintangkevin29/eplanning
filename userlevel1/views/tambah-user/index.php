<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
             'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',
		
            ['class' => 'yii\grid\ActionColumn',
			'template' => '{edit1} {edit2} {edit3} {edit4} {edit5}',
			
			'buttons' => [
				'edit1' => function ($url, $model){
					return Html::a('<span class="glyphicon glyphicon-eye-open"></span>&nbsp',['isi-asal', 'id' => $model->id]);
				},
				'edit2' => function ($url, $model){
					return Html::a(' <span class="glyphicon glyphicon-pencil"> </span>&nbsp',['update', 'id' => $model->id]);
				},
				'edit3' => function ($url, $model){
					return Html::a(' <span class="glyphicon glyphicon-user"> </span>&nbsp',['/ta-profile/create', 'id' => $model->id]);
				},
				'edit4' => function ($url, $model){
					return Html::a(' <span class="glyphicon glyphicon-fire"> </span>&nbsp',['/ta-user-aplikasi/index', 'id' => $model->id]);
				},
				'edit5' => function ($url, $model){
					return Html::a(' <span class="glyphicon glyphicon-trash"> </span>&nbsp',['/tambah-user/delete', 'id' => $model->id],
					['data' => [
                		'confirm' => "Anda yakin akan menghapus data ini?",
                		'method' => 'post',
            			],
            		]);
				}
				],
			],
        ],
		
    ]); ?>
    <?php Pjax::end(); ?>
</div>
