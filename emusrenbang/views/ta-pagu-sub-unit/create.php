<?php

use yii\helpers\Html;
use common\models\RefUrusan;
use common\models\RefBidang;
use common\models\RefUnit;
use common\models\RefSubUnit;
/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnit */

$this->title = 'Tambah Pagu Sub Unit';
$this->params['breadcrumbs'][] = ['label' => 'Pagu Sub Unit', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$bidang=array();
$unit=array();
$subunit=array();
?>
<div class="ta-pagu-sub-unit-create">

    <?= $this->render('_form', [
        'model' => $model,
        'bidang' => $bidang,
        'unit' => $unit,
        'subunit' => $subunit,
    ]) ?>

</div>
