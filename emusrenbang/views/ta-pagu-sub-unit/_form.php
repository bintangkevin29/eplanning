<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Referensi;

$referensi=new Referensi();
/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnit */
/* @var $form yii\widgets\ActiveForm */
$urusan=$referensi->getUrusan();
$js="
$('#kdurusan').change(function(){
    kdurusan=$('#kdurusan').val();
    $.post('index.php?r=ajax/getbidang&kdurusan='+kdurusan, function(data, success){
        $('#kdbidang').html(data);
    });
});
$('#kdbidang').change(function(){
     kdurusan=$('#kdurusan').val();
     kdbidang=$('#kdbidang').val();
     $.post('index.php?r=ajax/getunit&kdurusan='+kdurusan+'&kdbidang='+kdbidang, function(data, success){
        $('#kdunit').html(data);
    });
    // $.post('index.php?r=ajax/getidunit&kdurusan='+kdurusan+'&kdbidang='+kdbidang, function(data, success){
    //     $('#kdunit').html(data);
    // });
});

$('#kdunit').change(function(){
    kdurusan=$('#kdurusan').val();
    kdbidang=$('#kdbidang').val();
    kdunit=$('#kdunit').val();
    $.post('index.php?r=ajax/getsubunit&kdurusan='+kdurusan+'&kdbidang='+kdbidang+'&kdunit='+kdunit, function(data, success){
        $('#kdsub').html(data);
    });
});
";

$this->registerJs($js, 4, "My");

?>

<div class="ta-pagu-sub-unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Kd_Urusan')->dropDownList($urusan, ['prompt' =>'Pilih Urusan', 'id' => 'kdurusan']) ?>

    <?= $form->field($model, 'Kd_Bidang')->dropDownList($bidang, ['Prompt' => 'Pilih Sektor', 'id'=>  'kdbidang']) ?>

    <?= $form->field($model, 'Kd_Unit')->dropDownList($unit, ['Prompt' => 'Pilih Unit', 'id'=>  'kdunit']) ?>

    <?= $form->field($model, 'Kd_Sub')->dropDownList($subunit, ['Prompt' => 'Pilih Sub Unit', 'id'=>  'kdsub']) ?>

    <?= $form->field($model, 'pagu')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
