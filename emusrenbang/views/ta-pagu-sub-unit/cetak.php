
				<table class="table table-bordered" border="1">
					<thead>
						<tr>
							<th class="vcenter text-center">No</th>
							<th class="vcenter text-center">Unit</th>
							<th class="vcenter text-center">Sub Unit</th>
							<th class="vcenter text-center">Pagu</th>
							<th class="vcenter text-center">Pagu Serapan</th>
						</tr>
					</thead>
					<tbody id="isi-wrap">
					<?php
						$no=1;
						foreach ($model as $value) : 
						?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $value->unit->Nm_Unit; ?></td>
							<td><?= $value->sub_unit->Nm_Sub_Unit ?></td>
							<td style="text-align:right;"><?php echo number_format($value->pagu, 0, '.', '.')?></td>
							<td style="text-align:right;"><?php echo number_format($value->getTaBelanjaRincSubs()->sum('Total'), 0, '.', '.')?></td>
						</tr>
						<?php 
						endforeach; ?>
					</tbody>
				</table>