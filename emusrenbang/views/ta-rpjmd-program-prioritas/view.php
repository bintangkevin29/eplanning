<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaRpjmdProgramPrioritas */
?>
<div class="ta-rpjmd-program-prioritas-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            [
                'label' => 'Misi',
                'value' => '('.$model->taRpjmdMisi->No_Misi.') '.$model->taRpjmdSasaran->taRpjmdTujuan->taRpjmdMisi->Misi,
            ],
            [
                'label' => 'Tujuan',
                'value' => '('.$model->No_Tujuan.') '.$model->taRpjmdSasaran->taRpjmdTujuan->Tujuan,
            ],
            [
                'label' => 'Sasaran',
                'value' => '('.$model->No_Sasaran.') '.$model->taRpjmdSasaran->Sasaran,
            ],
            [
                'label' => 'Prioritas Pembangunan Daerah',
                'value' => $model->taRpjmdPrioritasPembangunanDaerah->Prioritas_Pembangunan_Daerah,
            ],
            [
                'label' => 'Program',
                'value' => '('.$model->No_Prioritas.') '.$model->taRpjmdPrioritasPembangunanDaerah->Prioritas_Pembangunan_Daerah,
            ],
            [
                'label' => 'Program',
                'value' => '('.$model->Kd_Prog.') '.$model->refKamusProgram->Nm_Program,
            ],
        ],
    ]) ?>

</div>
