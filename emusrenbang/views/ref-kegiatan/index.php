<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RefKegiatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kegiatan';
$this->params['breadcrumbs'][] = "Program Kegiatan";
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <?php
                if (Helper::checkRoute('create')) {
                    echo Html::a('Tambah Kegiatan', ['create'], ['class' => 'btn btn-success pull-right']);
                }
                ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <table class="table-hover">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'header' => 'No.'
                        ],
                        [
                            'format' => 'text',
                            'label' => "Urusan",
                            'value' => function($model) {
                                return $model->Kd_Urusan . ":" . $model->urusan->Nm_Urusan;
                            }
                        ],
                        [
                            'format' => 'text',
                            'label' => "Bidang",
                            'value' => function($model) {
                                return $model->Kd_Bidang . ":" . $model->bidang->Nm_Bidang;
                            }
                        ],
                        [
                            'format' => 'text',
                            'label' => "Program",
                            'value' => function($model) {
                                return $model->Kd_Prog . ":" . $model->program->Ket_Program;
                            }
                        ],
                        [
                            'attribute' => 'Ket_Kegiatan',
                            'format' => 'text',
                            'value' => function($model) {
                                return $model->Kd_Keg . ":" . $model->Ket_Kegiatan;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn('{view}{update}{delete}'),
                        ],
                    ],
                ]);
                ?>
                </table>
            </div>
        </div>            
    </div>
</div>