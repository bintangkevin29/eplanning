<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefUrusan;
use common\models\RefBidang;
use common\models\RefProgram;

/* @var $this yii\web\View */
/* @var $model app\models\RefKegiatan */
/* @var $form yii\widgets\ActiveForm */
$js = '$("#urusan").change(function(){
    $.ajax({
    url: "index.php?r=ref-kegiatan/get-bidang",
    type: "GET",
    data: {Kd_Urusan: $("#urusan").val()},
    success: function(data){
        $("#bidang").html(data);
    },
    error: function(xhr, stat, err){
        alert(xhr.responseText);
    }
});});
    $("#bidang").change(function(){
    $.ajax({
    url: "index.php?r=ref-kegiatan/get-program",
    type: "GET",
    data: {Kd_Urusan: $("#urusan").val(), Kd_Bidang: $("#bidang").val()},
    success: function(data){
        $("#program").html(data);
    },
    error: function(xhr, stat, err){
        alert(xhr.responseText);
    }
});});
    $("#program").change(function(){
    $.ajax({
    url: "index.php?r=ref-kegiatan/get-kode",
    type: "GET",
    data: {Kd_Urusan: $("#urusan").val(), Kd_Bidang: $("#bidang").val(), Kd_Prog: $("#program").val()},
    success: function(data){
        $("#kegiatan").val(data);
    },
    error: function(xhr, stat, err){
        alert(xhr.responseText);
    }
});});

$(".selects").select2({
  allowClear: true
});
';
$this->registerJs($js);

$this->registerCssFile(
        '@web/plugins/select2/select2.css'
);

$this->registerCssFile(
        '@web/plugins/select2/select2-bootstrap.css'
);

$this->registerJsFile(
        '@web/plugins/select2/select2.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="ref-kegiatan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Kd_Urusan')->dropdownList (
        ArrayHelper::map(RefUrusan::find()->all(),'Kd_Urusan','Nm_Urusan'),
        ['prompt' => 'Pilih Urusan', 'id' => 'urusan']
    ) ?>

    <?= $form->field($model, 'Kd_Bidang')->dropdownList (
        ArrayHelper::map(RefBidang::find()->all(),'Kd_Bidang','Nm_Bidang'),
        ['prompt' => 'Pilih Bidang', 'id' => 'bidang']
    ) ?>

    <?= $form->field($model, 'Kd_Prog')->dropdownList (
        ArrayHelper::map(RefProgram::find()->all(),'Kd_Prog','Ket_Program'),
        ['prompt' => 'Pilih Program', 'id' => 'program', 'class'=>'form-control selects' ]
    ) ?>

    <?= $form->field($model, 'Kd_Keg')->textInput(['id' => 'kegiatan', 'readonly'=>true]) ?>

    <?= $form->field($model, 'Ket_Kegiatan')->textInput(['maxlength' => true, 'value' => $us]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
$(".selects").select2({
  allowClear: true
});
</script>
