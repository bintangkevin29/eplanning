<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Referensi;

$referensi=new Referensi();
/* @var $this yii\web\View */
/* @var $model app\models\TaPaguKegiatan */
/* @var $form yii\widgets\ActiveForm */

$urusan=$referensi->getUrusan();
$js="
$('#kdurusan').change(function(){
    id=$('#kdurusan').val();
    $.post('index.php?r=ref-program/listbidang&urusan='+id, function(data, status){
        $('#kdbidang').html(data);
       // alert(data);
    })
});

$('#kdbidang').change(function(){
    id=$('#kdbidang').val();
    urusan=$('#kdurusan').val();
    $.post('index.php?r=ref-program/listprogram&urusan='+urusan+'&bidang='+id, function(data, status){
        $('#kdprog').html(data);
    })

    $.post('index.php?r=ref-program/listunit&urusan='+urusan+'&bidang='+id, function(data, status){
        $('#kdunit').html(data);
    })
});

$('#kdunit').change(function(){
    kdurusan=$('#kdurusan').val();
    kdbidang=$('#kdbidang').val();
    kdunit=$('#kdunit').val();
    $.post('index.php?r=ajax/getsubunit&kdurusan='+kdurusan+'&kdbidang='+kdbidang+'&kdunit='+kdunit, function(data, success){
        $('#kdsub').html(data);
    });
});

$('#kdprog').change(function(){
    kdurusan=$('#kdurusan').val();
    kdbidang=$('#kdbidang').val();
    kdprog=$('#kdprog').val();
    //alert(kdurusan+kdbidang+kdprog);
    
    $.post('index.php?r=ta-pagu-kegiatan/getkegiatan&kdurusan='+kdurusan+'&kdbidang='+kdbidang+'&kdprog='+kdprog, function(data, success){
        //alert(data);
        $('#kdkeg').html(data);
    });
});

";
$this->registerJs($js, 4, 'urusan');
$model->pagu=(int) $model->pagu;
?>

<div class="box box-success">
    <div class="box-body">
        <div class="ta-pagu-kegiatan-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'Kd_Urusan')->dropDownList($urusan,['prompt'=>'Pilih Urusan', 'id'=>'kdurusan'])->label('Urusan') ?>

            <?= $form->field($model, 'Kd_Bidang')->dropDownList($bidang, ['prompt'=>'Pilih Sektor', 'id'=>'kdbidang'])->label('Sektor') ?>

             <?= $form->field($model, 'Kd_Unit')->dropDownList($unit, ['prompt'=>'Pilih Unit', 'id'=>'kdunit'])->label('Unit'); ?>

            <?= $form->field($model, 'Kd_Sub')->dropDownList($subunit, ['prompt'=>'Pilih Sub Unit', 'id'=>'kdsub'])->label('Sub Unit'); ?>

            <?= $form->field($model, 'Kd_Prog')->dropDownList($prog, ['prompt'=>'Pilih Program', 'id'=>'kdprog'])->label('Program'); ?>


            <?= $form->field($model, 'Kd_Keg')->dropDownList($kegiatan, ['prompt'=>'Pilih Kegiatan', 'id'=>'kdkeg'])->label('Kegiatan'); ?>

            <?= $form->field($model, 'pagu')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
