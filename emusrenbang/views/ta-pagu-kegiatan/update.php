<?php

use yii\helpers\Html;
use app\models\Referensi;
$ref=new Referensi();

/* @var $this yii\web\View */
/* @var $model app\models\TaPaguKegiatan */

$this->title = 'Ubah Ta Pagu Kegiatan: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ta Pagu Kegiatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub, 'Kd_Prog' => $model->Kd_Prog]];
$this->params['breadcrumbs'][] = 'Ubah';

$prog=$ref->getProgramBidangUrusan($model->Kd_Urusan, $model->Kd_Bidang);
$bidang=$ref->getBidangUrusan($model->Kd_Urusan);
$unit=$ref->getUnitBidangUrusan($model->Kd_Urusan, $model->Kd_Bidang);
$subunit = $ref->getSubUnitBidangUrusan($model->Kd_Urusan, $model->Kd_Bidang, $model->Kd_Unit);
$kegiatan = $ref->getKegiatanProgramBidangUrusan($model->Kd_Urusan, $model->Kd_Bidang, $model->Kd_Prog);
?>
<div class="ta-pagu-kegiatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'prog'=>$prog,
        'bidang'=>$bidang,
        'unit'=>$unit,
        'subunit' => $subunit,
        'kegiatan' => $kegiatan,
    ]) ?>

</div>
