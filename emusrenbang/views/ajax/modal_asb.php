<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<input type="text" id="cari_asb" placeholder="Cari" class="form-control">
<table class="table table-bordered">
	<thead>
		<tr>
			<th>Uraian</th>
			<th>Satuan</th>
			<th>Harga</th>
			<th>Pilihan</th>
		</tr>
	</thead>
	<tbody id="body_tabel_asb">
	</tbody>
</table>

<script type="text/javascript">
	$("#cari_asb").keypress(function(e) {
    if(e.which == 13) {
    	var cari = $(this).val();
    	$("#body_tabel_asb").html('Mencari...');
    	$.ajax({ 
		    type: "GET",
		    url:'index.php?r=ajax/get-asb',
		    data:{cari:cari},
		    success: function(isi){
		      $("#body_tabel_asb").html(isi);
		    },
		    error: function(){
		      alert("failure");
		    }
		  });
    }
	});
</script>