<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<input type="text" id="cari" placeholder="Cari" class="form-control">
<table class="table table-bordered">
	<thead>
		<tr>
			<th>Uraian</th>
			<th>Satuan</th>
			<th>Spesifikasi</th>
			<th>Harga</th>
			<th>Pilihan</th>
		</tr>
	</thead>
	<tbody id="body_tabel_ssh">
	</tbody>
</table>

<script type="text/javascript">
	$("#cari").keypress(function(e) {
    if(e.which == 13) {
    	var cari = $(this).val();
    	$("#body_tabel_ssh").html('Mencari...');
    	$.ajax({ 
		    type: "GET",
		    url:'index.php?r=ajax/get-ssh',
		    data:{cari:cari},
		    success: function(isi){
		      $("#body_tabel_ssh").html(isi);
		    },
		    error: function(){
		      alert("failure");
		    }
		  });
    }
	});
</script>