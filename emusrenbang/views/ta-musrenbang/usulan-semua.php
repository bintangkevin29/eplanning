<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model eperencanaan\models\TaMusrenbang */

$this->title = 'Usulan Kecamatan';
$this->params['breadcrumbs'][] = ['label' => 'Ta Musrenbangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="ta-musrenbang-view col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= $this->title; ?>
                </h3>
                <div class="control-wrap">
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                                          'id' => 'search-usulan',
                                'action' => ['ta-musrenbang/cetak-usulan-kecamatan'],
                                'options' => ['target' => '_blank']
                    ]) ?>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <br>
                            <?= Html::submitButton('&nbsp;Cetak&nbsp;', ['id' => 'cari-submit', 'class' => 'btn btn-primary btn-lg']); ?>
                        </div>
                    </div>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table">
                      <?=
                          GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          'columns' => [
                              ['class' => 'yii\grid\SerialColumn'],

                              [
                                'attribute' => 'Kd_Kec',
                                'value' => 'kecamatan.Nm_Kec',
                                'filter'=> $data_kecamatan,
                              ],
                              [
                                'attribute' => 'Kd_Kel',
                                'value' => 'kelurahan.Nm_Kel',
                                'filter'=> $data_kelurahan,
                              ],
                              [
                                'attribute' => 'Kd_Lingkungan',
                                'value' => 'lingkungan.Nm_Lingkungan',
                                'filter'=> $data_lingkungan,
                                'contentOptions'=>['style'=>'width: 200px;']
                              ],
                              [
                                'attribute' => 'Kd_Pem',
                                'value' => 'bidangPembangunan.Bidang_Pembangunan',
                                'filter'=> $data_bidpem,
                              ],
                              [
                                'attribute' => 'Kd_Prioritas_Pembangunan_Daerah',
                                'value' => 'rpjmd.Nm_Prioritas_Pembangunan_Kota',
                                'filter'=> $data_rpjmd,
                              ],
                              'Nm_Permasalahan:ntext',
                              'Jenis_Usulan:ntext',
                              'Detail_Lokasi:ntext',
                              'Skor',
                              [
                                  'attribute'=>'Status_Prioritas',
                                  'filter'=>[""=>"Semua", "1"=>"Prioritas", '0'=>"Cadangan"],
                                  'value' => function ($model) {
                                      if($model->Status_Prioritas)
                                          return 'Prioritas';
                                      else
                                          return 'Cadangan';
                                  }
                              ],
                              [
                                  'class' => 'yii\grid\ActionColumn',
                                  'buttons' => [
                                      'tambahKegiatan' => function ($a, $model) {
                                          return Html::a(
                                              '<span class="glyphicon glyphicon-plus"></span>',
                                              ['ref-kegiatan/create/', 'us' => $model['Jenis_Usulan']],
                                              [
                                                  'title' => 'Tambah Kegiatan',
                                                  'target' => '_blank'
                                              ]
                                          );
                                      },
                                  ],
                                  'template' => "{view} {tambahKegiatan}",
                              ],
                          ],
                          ]);
                      ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
