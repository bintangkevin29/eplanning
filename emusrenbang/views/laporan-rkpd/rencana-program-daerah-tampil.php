    <div class="col-md-1"></div><div class="col-md-10" align="center" style="padding-bottom:10px"><h3>Rencana Program dan Kegiatan Prioritas Daerah Tahun <?= date('Y') ?><br>Kabupaten Labuhanbatu</h3></div><div class="col-md-1"></div>
        <div class="col-xs-12 table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">No</p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Urusan/Bidang Urusan Pemerintahan Daerah Dan Program/ Kegiatan </p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Prioritas Daerah </p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Sasaran Daerah </p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Lokasi </p></th>
                        <th colspan="6" style="vertical-align: middle;"><p class="text-center">Indikator Kinerja </p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Pagu Indikatif </p></th>
                        <th rowspan="3" style="vertical-align: middle;"><p class="text-center">Prakiraan Maju </p></th>
                        <th colspan="2" style="vertical-align: middle;"><p class="text-center">Keterangan </p></th>
                    </tr>
                    <tr>
                        <th colspan="2" style="vertical-align: middle;"><p class="text-center">Hasil Program </p></th>
                        <th colspan="2" style="vertical-align: middle;"><p class="text-center">Keluaran Kegiatan </p></th>
                        <th colspan="2" style="vertical-align: middle;"><p class="text-center">Hasil Kegiatan </p></th>
                        <th><p class="text-center">SKPD </p></th>
                        <th><p class="text-center">Jenis Keg </p></th>
                    </tr>
                        <th><p class="text-center">Tolok Ukur </p></th>
                        <th><p class="text-center">Target </p></th>
                        <th><p class="text-center">Tolok Ukur </p></th>
                        <th><p class="text-center">Target </p></th>
                        <th><p class="text-center">Tolok Ukur </p></th>
                        <th><p class="text-center">Target </p></th>
                        <th><p class="text-center">1/2/3 </p></th>
                        <th><p class="text-center">1/2/3 </p></th>
                    <tr>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($TaSubUnit as $value1):
                    ?>  
                    <tr>
                        <td><p style="font-size:11px;"><?= $value1->Kd_Urusan ?></p></td>
                        <td><p style="font-size:11px;"><?= $value1->urusan->Nm_Urusan ?></p></td>
                        <?php for($x=0;$x<13;$x++):?>
                            <td></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <td><p style="font-size:11px;"><?= $value1->Kd_Urusan.".".$value1->Kd_Bidang ?></p></td>
                        <td><p style="font-size:11px;"><?= $value1->kdBidang->Nm_Bidang ?></p></td>
                        <?php for($x=0;$x<13;$x++):?>
                            <td></td>
                        <?php endfor; ?>
                    </tr>
                    <?php foreach ($value1->taPrograms as $value2): ?>
                        <tr>
                            <td><p style="font-size:11px;"><?= $value1->Kd_Urusan.".".$value1->Kd_Bidang.".".$value2->Kd_Prog ?></p></td>
                            <td><p style="font-size:11px;"><?= $value2->Ket_Prog ?></p></td>
                            <?php for($x=0;$x<13;$x++):?><td></td><?php endfor; ?>
                        </tr>
                        <?php foreach ($value2->taKegiatans as $value3): ?>
                            <tr>
                                <td><p style="font-size:11px;"><?=  $value1->Kd_Urusan.".".$value1->Kd_Bidang.".".$value2->Kd_Prog.".".$value3->Kd_Keg   ?></p></td>
                                <td><p style="font-size:11px;"><?= $value3->Ket_Kegiatan ?></p></td>
                                <td></td>
                                <td></td>
                                <td><?= $value3->Lokasi ?></td>
                                <?php foreach ($value3->taIndikators as $value4): ?>
                                    <?php if($value4->Kd_Indikator!=7): ?>
                                        <td><p style="font-size:11px;"><?= $value4->Tolak_Ukur ?></p></td>
                                        <td><p style="font-size:11px;"><?= number_format($value4->Target_Angka,0,'.','.') ?> <?= $value4->Target_Uraian ?></td></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td><p style="font-size:11px;"><?= number_format($value3->pagu->pagu,0,'.','.') ?></p></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>