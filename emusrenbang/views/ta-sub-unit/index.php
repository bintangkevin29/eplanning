<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaSubUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Umum Unit Organisasi';
$this->params['breadcrumbs'][] = "Data Umum";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-sub-unit-index">
    <div class="box box-success">
        <div class="box-header">
            <p>
                <?php 
                    if (Helper::checkRoute('create')) {
                        echo Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success pull-right']);
                    }
                ?>
            </p>
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php
                if(Yii::$app->user->identity->id_level!==1) { ?>

                <table class="table table-hover table-bordered">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No.'
                            ],
                            [
                                'attribute' =>'Tahun',
                                'format' => 'text',
                                'options' => ['class' => 'col-md-1'],
                                'value' => function($model){ return $model->Tahun; }
                            ],
                             'Nm_Pimpinan',
                             'Nip_Pimpinan',
                             'Jbt_Pimpinan',
                             'Alamat',
                             'Ur_Visi',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['class' => 'col-md-1']
                            ],
                        ],
                    ]); ?> 
                </table>
                     
                <?php }
                
                else { ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'Tahun',
                            'Kd_Urusan',
                            'Kd_Bidang',
                            'Kd_Unit',
                            'Kd_Sub',
                             'Nm_Pimpinan',
                             'Nip_Pimpinan',
                             'Jbt_Pimpinan',
                             'Alamat',
                             'Ur_Visi',

                            [
                                'class' => 'yii\grid\ActionColumn', 
                                'template' => Helper::filterActionColumn('{view}{update}{delete}'),
                            ],
                        ],
                    ]); ?>
                <?php }
            ?>
        </div>
    </div>
</div>