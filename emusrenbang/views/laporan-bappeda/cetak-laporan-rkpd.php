
     <div class="box-header with-border">
        <div class="col-md-1"></div><div class="col-md-10" style="text-align:center;"><h3>Rencana Program dan Kegiatan Prioritas Daerah Tahun <?= date('Y') ?> <br>Provinsi/Kabupaten/Kota........*)</h3></div><div class="col-md-1"></div>
        <br>
        <div class="col-xs-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Nomor </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Urusan/ Bidang Urusan/ Pemerintahan Daerah dan Program/ Kegiatan </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Prioritas Daerah </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Sasaran Daerah </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Lokasi </th>
                        <th colspan="6" style="text-align:center;vertical-align:middle;">Indikator Kerja </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Pagu Indikatif </th>
                        <th rowspan="3" style="text-align:center;vertical-align:middle;">Prakiraan Maju </th>
                        <th colspan="3" style="text-align:center;vertical-align:middle;">Keterangan </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="text-align:center;vertical-align:middle;">Hasil Program </th>
                        <th colspan="2" style="text-align:center;vertical-align:middle;">Keluaran Kegiatan </th>
                        <th colspan="2" style="text-align:center;vertical-align:middle;">Hasil Kegiatan </th>
                        <th style="text-align:center;vertical-align:middle;">SKPD </th>
                        <th style="text-align:center;vertical-align:middle;">Jenis Keg </th>
                    </tr>
                    <tr>
                        <th style="text-align:center;vertical-align:middle;">Tolok Ukur </th>
                        <th style="text-align:center;vertical-align:middle;">Target </th>
                        <th style="text-align:center;vertical-align:middle;">Tolok Ukur </th>
                        <th style="text-align:center;vertical-align:middle;">Target </th>
                        <th style="text-align:center;vertical-align:middle;">Tolok Ukur </th>
                        <th style="text-align:center;vertical-align:middle;">Target </th>
                        <th style="text-align:center;vertical-align:middle;">1/2/3 </th>
                        <th style="text-align:center;vertical-align:middle;">1/2/3 </th>
                    </tr>

                    <tr>
                    <?php for($i=1;$i<=15;$i++): ?>
                        <td style="text-align:center;vertical-align:middle;">(<?= $i ?>)</td>
                    <?php endfor; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->urusan->Kd_Urusan ?></td>
                        <td><?= $data->urusan->Nm_Urusan ?></td>
                        <?php for($i=1;$i<=13;$i++): ?> <td></td> <?php endfor; ?>
                    </tr>
                    <tr>
                        <td><p><?= $data->urusan->Kd_Urusan.'.'.$data->kdBidang->Kd_Bidang ?></p></td>
                        <td><p style="padding-left:10px;"><?= $data->kdBidang->Nm_Bidang ?></p></td>
                        <?php for($i=1;$i<=13;$i++): ?> <td></td> <?php endfor; ?>
                    </tr>
                    <?php foreach ($data->taPrograms as $key1 => $value1): ?>
                        <tr>
                            <td><p><?= $data->urusan->Kd_Urusan.'.'.$data->kdBidang->Kd_Bidang.'.'.$value1->Kd_Prog ?></p></td>
                            <td><p style="padding-left:20px;"><?= $value1->Ket_Prog ?></p></td>
                            <?php for($i=1;$i<=13;$i++): ?> <td></td> <?php endfor; ?>
                        </tr>
                        <?php foreach ($value1->kegiatans as $key2 => $value2): ?>
                            <tr>
                                <td><p><?= $data->urusan->Kd_Urusan.'.'.$data->kdBidang->Kd_Bidang.'.'.$value1->Kd_Prog.'.'.$value2->Kd_Keg ?></p></td>
                                <td><p style="padding-left:30px;"><?= $value2->Ket_Kegiatan ?></p></td>
                                <td></td>
                                <td></td>
                                <td><?= $value2->Lokasi ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="text-align:right;"><?= number_format($value2->getBelanjaRincSubs()->sum('Total'),0,'.','.') ?></td>
                                <?php for($i=1;$i<=11;$i++): ?> <td></td> <?php endfor; ?>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>