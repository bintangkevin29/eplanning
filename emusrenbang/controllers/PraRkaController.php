<?php

namespace emusrenbang\controllers;
use Yii;
use common\models\TaPaguSubUnit;
use common\models\TaSubUnit;
use common\models\TaProgram;
use common\models\search\TaProgramSearch;
use common\models\TaPaguProgram;
use common\models\TaKegiatan;
use common\models\TaKegiatanSearch;
use emusrenbang\models\TaPaguKegiatan;
use common\models\RefProgram;
use common\models\RefKegiatan;
use common\models\RefIndikator;
use common\models\Satuan;
use yii\helpers\ArrayHelper;
use emusrenbang\models\TaIndikator;
use emusrenbang\models\TaBelanja;
use common\models\RefRek1;
use common\models\RefRek2;
use common\models\RefRek3;
use common\models\RefRek4;
use common\models\RefRek5;
use common\models\RefApPub;
use common\models\RefSumberDana;
use emusrenbang\models\TaBelanjaRinc;
use emusrenbang\models\TaBelanjaRincSub;
use common\models\RefStandardSatuan;
use common\models\RefSsh;

//========riwayat=========//
use emusrenbang\models\TaKegiatanRiwayat;
use emusrenbang\models\TaBelanjaRiwayat;
use emusrenbang\models\TaBelanjaRincRiwayat;
use emusrenbang\models\TaBelanjaRincSubRiwayat;



class PraRkaController extends \yii\web\Controller
{

	public function actionIndex()
	{
		return $this->render('index');
	}
	public function actionTambahKamus($Kd_Urusan, $Kd_Bidang, $Kd_Prog)
	{
		$Posisi_ref = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Prog' => $Kd_Prog,
		];

		$model = new RefKegiatan();

		$max_kd=RefKegiatan::find()
		->where($Posisi_ref)
		->max('Kd_Keg');
		$Kd_Keg = $max_kd + 1;

        //$model2 = new TaPaguKegiatan();
		return $this->renderajax('tambah_kamus', [
			'model' => $model,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Prog' => $Kd_Prog,
		]);
	}

	public function actionTambahKamusProses()
	{
		$request = Yii::$app->request;
		$model = new RefKegiatan();
		if($model->load($request->post())){
			if($model->save(false))
				echo "Tambah Kamus Berhasil";
		}

	}

    //====================//
	public function actionBelanjaRincSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
	{
		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
		];
		$data = TaBelanjaRinc::findOne($PosisiKegiatan);

		$data_belanja=TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->all();

		return $this->render('belanja_rinc_sub', [
			'data' => $data,
			'data_belanja' => $data_belanja,
		]);
	}

	public function actionTambahRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
	{
		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
		];

		$model = new TaBelanjaRincSub();

		$max_id=TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->max('No_ID');
		$No_ID = $max_id + 1;


		$Ref_Usulan_Rincian = 4;
		$Standard_Satuan=ArrayHelper::map(RefStandardSatuan::find()->orderby('Uraian')->all(),'Uraian','Uraian');

		return $this->renderajax('tambah-rincian-obyek', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'model' => $model,
			'No_Rinc' => $No_Rinc,
			'No_ID' => $No_ID,
			'Standard_Satuan' => $Standard_Satuan,
			'Ref_Usulan_Rincian' => $Ref_Usulan_Rincian,
		]);
	}

	public function actionTambahRincianObyekProses()
	{
		$request = Yii::$app->request;
		$model = new TaBelanjaRincSub();
		if($model->load($request->post())){
        //$model->Ket_Kegiatan;

			$PosisiKegiatan = [
				'Kd_Urusan' => $model->Kd_Urusan,
				'Kd_Bidang' => $model->Kd_Bidang,
				'Kd_Unit' => $model->Kd_Unit,
				'Kd_Sub' => $model->Kd_Sub,
				'Kd_Prog' => $model->Kd_Prog,
				'Kd_Keg' => $model->Kd_Keg,
				'Kd_Rek_1' => $model->Kd_Rek_1,
				'Kd_Rek_2' => $model->Kd_Rek_2,
				'Kd_Rek_3' => $model->Kd_Rek_3,
				'Kd_Rek_4' => $model->Kd_Rek_4,
				'Kd_Rek_5' => $model->Kd_Rek_5,
				'No_Rinc' => $model->No_Rinc,
			];
			$data = TaBelanjaRinc::findOne($PosisiKegiatan);

			$pagu_skpd = $data->taPaguSubUnit->pagu;
			$pemakaian_skpd = $data->getJlhBelanjaRincSubUnits()->sum('Total');
			$total_biaya = $model->Total;
			$total_pakai = $pemakaian_skpd + $total_biaya;

			if ($total_pakai > $pagu_skpd) {
				echo "Pagu Tidak Mencukupi";
			}
			else{
				$connection = \Yii::$app->db;
				$transaction = $connection->beginTransaction();
				try {
					$model_riwayat = new TaBelanjaRincSubRiwayat();
					$model_riwayat->attributes = $model->attributes;
					$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
					$model_riwayat->Keterangan_Riwayat="Tambah";
					$model_riwayat->save();

					$model->save(false);

					echo "Tambah Rincian Obyek Kegiatan Berhasil";
					$transaction->commit();
				} catch (\Exception $e) {
					$transaction->rollBack();
					throw $e;
				} catch (\Throwable $e) {
					$transaction->rollBack();
					throw $e;
				}
			}
		}
	}

	public function actionHapusRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
			'No_ID' => $No_ID,
		];
		$model = TaBelanjaRincSub::findOne($Posisi);

      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$model_riwayat = new TaBelanjaRincSubRiwayat();
			$model_riwayat->attributes = $model->attributes;
			$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
			$model_riwayat->Keterangan_Riwayat="Hapus";
			$model_riwayat->save();

			$model->delete(false);

			echo "Hapus Berhasil";
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		} catch (\Throwable $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	public function actionUbahRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
			'No_ID' => $No_ID,
		];
		$model = TaBelanjaRincSub::findOne($Posisi);
		$No_ID = $model->No_ID;
		$Ref_Usulan_Rincian = $model->Ref_Usulan_Rincian;
		$Standard_Satuan=ArrayHelper::map(RefStandardSatuan::find()->orderby('Uraian')->all(),'Uraian','Uraian');

		return $this->renderajax('tambah-rincian-obyek', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'model' => $model,
			'No_Rinc' => $No_Rinc,
			'No_ID' => $No_ID,
			'Standard_Satuan' => $Standard_Satuan,
			'Ref_Usulan_Rincian' => $Ref_Usulan_Rincian,
			'ubah' => 1,
		]);
	}

	public function actionUbahRincianObyekProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
			'No_ID' => $No_ID,
		];
		$model = TaBelanjaRincSub::findOne($Posisi);

		$total_pra = $model->Total;
		$request = Yii::$app->request;
		if($model->load($request->post())){
			$PosisiKegiatan = [
				'Kd_Urusan' => $model->Kd_Urusan,
				'Kd_Bidang' => $model->Kd_Bidang,
				'Kd_Unit' => $model->Kd_Unit,
				'Kd_Sub' => $model->Kd_Sub,
				'Kd_Prog' => $model->Kd_Prog,
				'Kd_Keg' => $model->Kd_Keg,
				'Kd_Rek_1' => $model->Kd_Rek_1,
				'Kd_Rek_2' => $model->Kd_Rek_2,
				'Kd_Rek_3' => $model->Kd_Rek_3,
				'Kd_Rek_4' => $model->Kd_Rek_4,
				'Kd_Rek_5' => $model->Kd_Rek_5,
				'No_Rinc' => $model->No_Rinc,
			];
			$data = TaBelanjaRinc::findOne($PosisiKegiatan);

			$pagu_skpd = $data->taPaguSubUnit->pagu;
			$pemakaian_skpd = $data->getJlhBelanjaRincSubUnits()->sum('Total')-$total_pra;
			$total_biaya = $model->Total;
			$total_pakai = $pemakaian_skpd + $total_biaya;

			if ($total_pakai > $pagu_skpd) {
				echo "Pagu Tidak Mencukupi";
			}
			else{


				$connection = \Yii::$app->db;
				$transaction = $connection->beginTransaction();
				try {
					$model_riwayat = new TaBelanjaRincSubRiwayat();
					$model_riwayat->attributes = $model->attributes;
					$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
					$model_riwayat->Keterangan_Riwayat="Ubah";
					$model_riwayat->save();

					$model->save(false);

					echo "Ubah Rincian Obyek Kegiatan Berhasil";
					$transaction->commit();
				} catch (\Exception $e) {
					$transaction->rollBack();
					throw $e;
				} catch (\Throwable $e) {
					$transaction->rollBack();
					throw $e;
				}
			}
		}
	}


    //=====================//
	public function actionBelanjaRinc($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
	{
		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
		];
		$data = TaBelanja::findOne($PosisiKegiatan);

		$data_belanja = TaBelanjaRinc::find()->where($PosisiKegiatan)->all();
		$jlh_pagu_terpakai = TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->sum('Total');
		$jlh_objek_rincian = TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->count();

		return $this->render('belanja-rinc', [
			'data' => $data,
			'data_belanja' => $data_belanja,
			'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
			'jlh_objek_rincian' => $jlh_objek_rincian,
                //'data_belanja' => $data_belanja,
		]);
	}

	public function actionTambahRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
	{
		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
		];

		$model = new TaBelanjaRinc();

		$max_id=TaBelanjaRinc::find()
		->where($PosisiKegiatan)
		->max('No_Rinc');
		$No_Rinc = $max_id + 1;

		return $this->renderajax('tambah-rincian-sub', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'model' => $model,
			'No_Rinc' => $No_Rinc,
		]);
	}

	public function actionTambahRincianSubProses()
	{
		$request = Yii::$app->request;
		$model = new TaBelanjaRinc();
		if($model->load($request->post())){
          //$model->Ket_Kegiatan;
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$model_riwayat = new TaBelanjaRincRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Tambah";
				$model_riwayat->save();

				$model->save(false);

				echo "Masukkan Rincian Sub Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

	public function actionHapusRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
		];
		$model = TaBelanjaRinc::findOne($Posisi);

      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$model_riwayat = new TaBelanjaRincRiwayat();
			$model_riwayat->attributes = $model->attributes;
			$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
			$model_riwayat->Keterangan_Riwayat="Hapus";
			$model_riwayat->save();

			$model->delete(false);

			echo "Hapus Berhasil";
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		} catch (\Throwable $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	public function actionUbahRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
		];
		$model = TaBelanjaRinc::findOne($Posisi);

		return $this->renderajax('tambah-rincian-sub', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
			'model' => $model,
			'ubah' => 1,
		]);
	}

	public function actionUbahRincianSubProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'No_Rinc' => $No_Rinc,
		];
		$model = TaBelanjaRinc::findOne($Posisi);
		$request = Yii::$app->request;
		if($model->load($request->post())){
          //$model->Ket_Kegiatan;
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$model_riwayat = new TaBelanjaRincRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Ubah";
				$model_riwayat->save();
				$model->save(false);

				echo "Ubah Rincian Sub Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

    //====================//

	public function actionBelanja($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{
		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
		];
		$data = TaKegiatan::findOne($PosisiKegiatan);

		$data_belanja = TaBelanja::find()->where($PosisiKegiatan)->all();
		$jlh_pagu_terpakai = TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->sum('Total');

		return $this->render('belanja', [
			'data' => $data,
			'data_belanja' => $data_belanja,
			'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
		]);
	}

	public function actionTambahRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{

		$PosisiKegiatan = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
		];

		$model = new TaBelanja();

		$Data_Rek_1 = RefRek1::findOne(['Kd_Rek_1'=>5]);
		$Data_Rek_2 = RefRek2::findOne(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2]);
		$Data_Rek_3 = ArrayHelper::map(RefRek3::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2])->all(),
			'Kd_Rek_3',
			function($model, $defaultValue) {
				return $model->Kd_Rek_3.". ".$model->Nm_Rek_3;
			}
		);
		$Data_Rek_4 = [];
		$Data_Rek_5 = [];


		$Kd_Rek_1 = $Data_Rek_1->Kd_Rek_1;
		$Nm_Rek_1 = $Data_Rek_1->Nm_Rek_1;
		$Kd_Rek_2 = $Data_Rek_2->Kd_Rek_2;
		$Nm_Rek_2 = $Data_Rek_2->Nm_Rek_2;
        //$model->addRule(['Nm_Rek_1'], 'string', ['max' => 128]);



		$RefApPub = ArrayHelper::map(RefApPub::find()->all(), 'Kd_Ap_Pub', 'Nm_Ap_Pub');
		$RefSumberDana = ArrayHelper::map(RefSumberDana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

		return $this->renderajax('tambah-rincian', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'model' => $model,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Nm_Rek_1' => $Nm_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Nm_Rek_2' => $Nm_Rek_2,
			'Data_Rek_2' => $Data_Rek_2,
			'Data_Rek_3' => $Data_Rek_3,
			'Data_Rek_4' => $Data_Rek_4,
			'Data_Rek_5' => $Data_Rek_5,
			'RefApPub' => $RefApPub,
			'RefSumberDana' => $RefSumberDana
		]);
	}

	public function actionTambahRincianProses()
	{
		$request = Yii::$app->request;
		$model = new TaBelanja();
		if($model->load($request->post())){
          //$model->Ket_Kegiatan;
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$model_riwayat = new TaBelanjaRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Tambah";
				$model_riwayat->save();

				$model->save(false);

				echo "Masukkan Rincian Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

	public function actionHapusRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5
		];
		$model = TaBelanja::findOne($Posisi);

      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$model_riwayat = new TaBelanjaRiwayat();
			$model_riwayat->attributes = $model->attributes;
			$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
			$model_riwayat->Keterangan_Riwayat="Hapus";
			$model_riwayat->save();

			$model->delete(false);

			echo "Hapus Berhasil";
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		} catch (\Throwable $e) {
			$transaction->rollBack();
			throw $e;
		}

	}

	public function actionUbahRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
	{
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5
		];

		$model = TaBelanja::findOne($Posisi);

		$Data_Rek_1 = RefRek1::findOne(['Kd_Rek_1'=>5]);
		$Data_Rek_2 = RefRek2::findOne(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2]);
		$Data_Rek_3 = ArrayHelper::map(RefRek3::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2])->all(), 'Kd_Rek_3','Nm_Rek_3');
		$Data_Rek_4 = ArrayHelper::map(RefRek4::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2, 'Kd_Rek_3'=>$Kd_Rek_3])->all(), 'Kd_Rek_4','Nm_Rek_4');
		$Data_Rek_5 = ArrayHelper::map(RefRek5::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2, 'Kd_Rek_3'=>$Kd_Rek_3, 'Kd_Rek_4'=>$Kd_Rek_4])->all(), 'Kd_Rek_5','Nm_Rek_5');


		$Kd_Rek_1 = $Data_Rek_1->Kd_Rek_1;
		$Nm_Rek_1 = $Data_Rek_1->Nm_Rek_1;
		$Kd_Rek_2 = $Data_Rek_2->Kd_Rek_2;
		$Nm_Rek_2 = $Data_Rek_2->Nm_Rek_2;
        //$model->addRule(['Nm_Rek_1'], 'string', ['max' => 128]);


		$RefApPub = ArrayHelper::map(RefApPub::find()->all(), 'Kd_Ap_Pub', 'Nm_Ap_Pub');
		$RefSumberDana = ArrayHelper::map(RefSumberDana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

		return $this->renderajax('tambah-rincian', [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'model' => $model,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Nm_Rek_1' => $Nm_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Nm_Rek_2' => $Nm_Rek_2,
			'Data_Rek_2' => $Data_Rek_2,
			'Data_Rek_3' => $Data_Rek_3,
			'Data_Rek_4' => $Data_Rek_4,
			'Data_Rek_5' => $Data_Rek_5,
			'RefApPub' => $RefApPub,
			'RefSumberDana' => $RefSumberDana,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5,
			'ubah' => 1,
		]);
	}

	public function actionUbahRincianProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
	{
		$request = Yii::$app->request;
		$Posisi = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Kd_Rek_1' => $Kd_Rek_1,
			'Kd_Rek_2' => $Kd_Rek_2,
			'Kd_Rek_3' => $Kd_Rek_3,
			'Kd_Rek_4' => $Kd_Rek_4,
			'Kd_Rek_5' => $Kd_Rek_5
		];

		$model = TaBelanja::findOne($Posisi);
      //$model = new TaBelanja();
		if($model->load($request->post())){
          //$model->Ket_Kegiatan;
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$model_riwayat = new TaBelanjaRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Ubah";
				$model_riwayat->save();

				$model->save(false);

				echo "Ubah Rincian Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

    //======================//

	public function actionKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog)
	{
		$PosisiKegiatan = [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
		];

		$searchModel = new TaKegiatanSearch();
		$dataProvider = $searchModel->searchKegiatan($PosisiKegiatan);
		$modelUnit = TaProgram::findOne($PosisiKegiatan);

		$jlh_pagu_terpakai = TaBelanjaRincSub::find()
		->where($PosisiKegiatan)
		->sum('Total');

		return $this->render('kegiatan', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'data' => $modelUnit,
			'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
		]);
	}

	public function actionTambahKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog)
	{
		$PosisiKegiatan = [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
		];

		$Posisi_ref = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Prog' => $Kd_Prog,
		];

		$model = new TaKegiatan();

		$max_id=TaKegiatan::find()
		->where($PosisiKegiatan)
		->max('ID_Prog');
		$ID_Prog = $max_id + 1;

		$max_kd=TaKegiatan::find()
		->where($PosisiKegiatan)
		->max('Kd_Keg');
		$Kd_Keg = $max_kd + 1;

        //============//
		$indikator=RefIndikator::find()
		->where(['In', 'Kd_Indikator', [2,3,4,7]])
		->all();

        //============//
        //$satuan=satuan::find()->all();
		$satuan =  ArrayHelper::map(Satuan::find()->all(),'id','satuan');

        //============//
		$sumber_dana = ArrayHelper::map(RefSumberdana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        //============//
        //$ref_kegiatan =  ArrayHelper::map(RefKegiatan::find()->where($Posisi_ref)->all(),'Ket_Kegiatan','Ket_Kegiatan');
		$ref_kegiatan = [];
		$dat_kegiatan = RefKegiatan::find()->where($Posisi_ref)->all();
		foreach ($dat_kegiatan as $key => $value) {
			$ref_kegiatan[$value->Kd_Keg."|".$value->Ket_Kegiatan] = $value->Ket_Kegiatan;
		}


        //$model2 = new TaPaguKegiatan();
		return $this->renderajax('tambah-kegiatan', [
			'model' => $model,
			'ID_Prog' => $ID_Prog,
			'Kd_Keg' => $Kd_Keg,
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'indikator' => $indikator,
			'satuan' => $satuan,
			'sumber_dana' => $sumber_dana,
			'ref_kegiatan' => $ref_kegiatan,
                //'models' => $model2,
		]);
	}

	public function actionTambahKegiatanProses()
	{
		$request = Yii::$app->request;
		$model = new TaKegiatan();
		if($model->load($request->post())){
          //$model->Ket_Kegiatan;
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$waktu_pelaksanaan_nilai = $request->post('waktu_pelaksanaan_nilai');
				$waktu_pelaksanaan_satuan = $request->post('waktu_pelaksanaan_satuan');
				$model->Waktu_Pelaksanaan = $waktu_pelaksanaan_nilai." ".$waktu_pelaksanaan_satuan;
				$model->Status = 0;
				$dat_kegiatan = explode("|", $model->Ket_Kegiatan);
              // $model->Kd_Keg = $dat_kegiatan[0];
				$model->Ket_Kegiatan = $dat_kegiatan[1];
				$model->save(false);

              //simpan ke riwayat
				$model_riwayat = new TaKegiatanRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Tambah";
				$model_riwayat->save();

				$model_pagu = new TaPaguKegiatan();
				$model_pagu->Tahun=$model->Tahun;
				$model_pagu->Kd_Keg=$model->Kd_Keg;
				$model_pagu->Kd_Urusan=$model->Kd_Urusan;
				$model_pagu->Kd_Bidang=$model->Kd_Bidang;
				$model_pagu->Kd_Unit=$model->Kd_Unit;
				$model_pagu->Kd_Sub=$model->Kd_Sub;
				$model_pagu->Kd_Prog=$model->Kd_Prog;
				$model_pagu->pagu=$model->Pagu_Anggaran;

				$model_pagu->save(false);

				$tolak_ukur = $request->post('tolak_ukur');
				$target = $request->post('target');
				$target_satuan = $request->post('target_satuan');

              // $max_indi=TaIndikator::find()
              //           ->where([
              //               'Kd_Urusan' => $model->Kd_Urusan,
              //               'Kd_Bidang' => $model->Kd_Bidang,
              //               'Kd_Unit' => $model->Kd_Unit,
              //               'Kd_Sub' => $model->Kd_Sub,
              //               'Kd_Prog' => $model->Kd_Prog,
              //               'Kd_Keg' => $model->Kd_Keg,
              //             ])
              //           ->max('Kd_Indikator');
              // $Kd_Indikator = $max_indi + 1;

				foreach ($tolak_ukur as $key => $toluk) {
					$model_indikator = new TaIndikator();

					$model_indikator->Tahun = $model->Tahun;
					$model_indikator->Kd_Urusan = $model->Kd_Urusan;
					$model_indikator->Kd_Bidang = $model->Kd_Bidang;
					$model_indikator->Kd_Unit = $model->Kd_Unit;
					$model_indikator->Kd_Sub = $model->Kd_Sub;
					$model_indikator->Kd_Prog = $model->Kd_Prog;
					$model_indikator->ID_Prog = $model->ID_Prog;
					$model_indikator->Kd_Keg = $model->Kd_Keg;
					$model_indikator->Kd_Indikator = $key;
					$model_indikator->No_ID = 0;
					$model_indikator->Tolak_Ukur= $toluk;
					$model_indikator->Target_Angka=str_replace('.', '', $target[$key]);
					$model_indikator->Target_Uraian=str_replace('.', '', $target_satuan[$key]);

					$model_indikator->save(false);
                //$Kd_Indikator++;
				}

				echo "Masukkan Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

	public function actionUbahKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{
		$PosisiKegiatan = [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
		];

		$Posisi_ref = [
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Prog' => $Kd_Prog,
		];

		$model = TaKegiatan::findOne($PosisiKegiatan);

        // $max_id=TaKegiatan::find()
        //         ->where($PosisiKegiatan)
        //         ->max('ID_Prog');
        // $ID_Prog = $max_id + 1;

        // $max_kd=TaKegiatan::find()
        //         ->where($PosisiKegiatan)
        //         ->max('Kd_Keg');
        // $Kd_Keg = $max_kd + 1;

        //============//
        // $indikator=RefIndikator::find()
        //               ->where(['In', 'Kd_Indikator', [2,3,4,7]])
        //               ->all();

		$indikator_data=$model->getIndikator()->all();
		$indikator=RefIndikator::find()
		->where(['In', 'Kd_Indikator', [2,3,4,7]])
		->all();
        //============//
        //$satuan=satuan::find()->all();
		$satuan =  ArrayHelper::map(Satuan::find()->all(),'id','satuan');

        //============//
		$sumber_dana = ArrayHelper::map(RefSumberdana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        //============//
        //$ref_kegiatan =  ArrayHelper::map(RefKegiatan::find()->where($Posisi_ref)->all(),'Ket_Kegiatan','Ket_Kegiatan');
		$ref_kegiatan = [];
		$dat_kegiatan = RefKegiatan::find()->where($Posisi_ref)->all();
		foreach ($dat_kegiatan as $key => $value) {
			$ref_kegiatan[$value->Kd_Keg."|".$value->Ket_Kegiatan] = $value->Ket_Kegiatan;
		}

		if (isset($model->Waktu_Pelaksanaan)) {
			$waktu = explode(" ", $model->Waktu_Pelaksanaan);
			$waktu_pelaksanaan_nilai = $waktu[0];
			$waktu_pelaksanaan_satuan = $waktu[1];
		}
		else{
			$waktu_pelaksanaan_nilai = '';
			$waktu_pelaksanaan_satuan = '';
		}
		$a = $model->Kd_Keg;
		$b = $model->Ket_Kegiatan;
		$model->Ket_Kegiatan = $a."|".$b;

		$model_indikator = TaIndikator::findall($PosisiKegiatan);
		foreach ($model_indikator as $key => $indi) {
			$Kd_Indikator = $indi->Kd_Indikator;

			$Tolak_Ukur[$Kd_Indikator]=$indi->Tolak_Ukur;
			$Target_Angka[$Kd_Indikator]=$indi->Target_Angka;
			$Target_Uraian[$Kd_Indikator]=$indi->Target_Uraian;
		}

		if (!isset($Tolak_Ukur)) {
			$Tolak_Ukur=[];
			$Target_Angka=[];
			$Target_Uraian=[];
		}



        //$model2 = new TaPaguKegiatan();
		return $this->renderajax('tambah-kegiatan', [
			'model' => $model,
                // 'ID_Prog' => $ID_Prog,
                // 'Kd_Keg' => $Kd_Keg,
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
			'indikator' => $indikator,
			'satuan' => $satuan,
			'sumber_dana' => $sumber_dana,
			'ref_kegiatan' => $ref_kegiatan,
			'waktu_pelaksanaan_nilai' => $waktu_pelaksanaan_nilai,
			'waktu_pelaksanaan_satuan' => $waktu_pelaksanaan_satuan,
			'ubah' => 1,
			'Tolak_Ukur' => $Tolak_Ukur,
			'Target_Angka' => $Target_Angka,
			'Target_Uraian' => $Target_Uraian,
                //'models' => $model2,
		]);
	}

	public function actionUbahKegiatanProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{
		$request = Yii::$app->request;
		$Posisi = [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
		];

		$model = TaKegiatan::findone($Posisi);

		if($model->load($request->post())){
			$connection = \Yii::$app->db;
			$transaction = $connection->beginTransaction();
			try {
				$waktu_pelaksanaan_nilai = $request->post('waktu_pelaksanaan_nilai');
				$waktu_pelaksanaan_satuan = $request->post('waktu_pelaksanaan_satuan');
				$model->Waktu_Pelaksanaan = $waktu_pelaksanaan_nilai." ".$waktu_pelaksanaan_satuan;
				$model->Status = 0;
				$model->save(false);

              //simpan ke riwayat
				$model_riwayat = new TaKegiatanRiwayat();
				$model_riwayat->attributes = $model->attributes;
				$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
				$model_riwayat->Keterangan_Riwayat="Ubah";
				$model_riwayat->save();

				$model_pagu = TaPaguKegiatan::findOne($Posisi);
				$model_pagu->pagu=$model->Pagu_Anggaran;
				$model_pagu->save(false);

				$tolak_ukur = $request->post('tolak_ukur');
				$target = $request->post('target');
				$target_satuan = $request->post('target_satuan');


				TaIndikator::deleteAll($Posisi);
				foreach ($tolak_ukur as $key => $toluk) {
					$model_indikator = new TaIndikator();

					$model_indikator->Tahun = $model->Tahun;
					$model_indikator->Kd_Urusan = $model->Kd_Urusan;
					$model_indikator->Kd_Bidang = $model->Kd_Bidang;
					$model_indikator->Kd_Unit = $model->Kd_Unit;
					$model_indikator->Kd_Sub = $model->Kd_Sub;
					$model_indikator->Kd_Prog = $model->Kd_Prog;
					$model_indikator->ID_Prog = $model->ID_Prog;
					$model_indikator->Kd_Keg = $model->Kd_Keg;
					$model_indikator->Kd_Indikator = $key;
					$model_indikator->No_ID = 0;
					$model_indikator->Tolak_Ukur=$nilai = str_replace('.', '', $toluk);
					$model_indikator->Target_Angka=str_replace('.', '', $target[$key]);
					$model_indikator->Target_Uraian=str_replace('.', '', $target_satuan[$key]);

					$model_indikator->save(false);
                //$Kd_Indikator++;
				}

				echo "Ubah Kegiatan Berhasil";
				$transaction->commit();
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			} catch (\Throwable $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
	}

	public function actionViewKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{
		return $this->render('view-kegiatan');
	}

	public function actionHapusKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
	{
		$Posisi = [
			'Tahun' => $Tahun,
			'Kd_Urusan' => $Kd_Urusan,
			'Kd_Bidang' => $Kd_Bidang,
			'Kd_Unit' => $Kd_Unit,
			'Kd_Sub' => $Kd_Sub,
			'Kd_Prog' => $Kd_Prog,
			'Kd_Keg' => $Kd_Keg,
		];
		$model = TaKegiatan::findOne($Posisi);
      //$model2 = TaPaguKegiatan::findOne($Posisi);
      //$model3 = TaIndikator::findOne($Posisi);

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$model_riwayat = new TaKegiatanRiwayat();
			$model_riwayat->attributes = $model->attributes;
			$model_riwayat->Tanggal_Riwayat=date('Y-m-d');
			$model_riwayat->Keterangan_Riwayat="Hapus";
			$model_riwayat->save();

			TaIndikator::deleteall($Posisi);
			$model->delete(false);

			echo "Hapus Berhasil";
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		} catch (\Throwable $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

    //===========================//
	public function actionProgram()
	{

		$unit = Yii::$app->levelcomponent->getUnit();
		$PosisiUnit = Yii::$app->levelcomponent->PosisiUnit();

		$searchModel = new TaProgramSearch();
		$dataProvider = $searchModel->searchProgram2(Yii::$app->request->queryParams);
		$modelUnit = TaSubUnit::find()->where($PosisiUnit)->all();
		$data = TaSubUnit::findone($PosisiUnit);

		$tabelanjanrincsub = TaBelanjaRincSub::find()
		->where($PosisiUnit)
		->sum('Total');

		$modelLevel = $PosisiUnit;

		return $this->render('program', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'modelLevel' => $modelLevel,
			'modelUnit' => $modelUnit,
			'data' => $data,
			'tabelanjanrincsub' => $tabelanjanrincsub,
		]);
	}
	public function actionSsh6()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }
    public function actionSsh5()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh5')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }
    public function actionSsh4()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh4')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }
    public function actionSsh3()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh3')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }

    public function actionSsh2()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh2')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }

    public function actionSsh1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh1')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }

    public function actionUnit()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Unit')
                    ->all(); 
			 echo '<pre>' . var_export($ssh, true) . '</pre>'; 
        /* return $this->render('tarikdata', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
        */
    }

    public function actionRefunit()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Unit')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Nm_Unit" => $data['Nm_Unit']);

            	$i++;
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }



    public function actionRefurusan1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Urusan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Nm_Urusan" => $data['Nm_Urusan'],
            		"tahun" => '2018');

            	$i++;
            }
            $json = array(
            	'item' => $item);

            header('Content-type: text/xml');
            echo "<response>";
            foreach($item as $tes)
            {
            	echo "<item>";
            		foreach ($tes as $tis => $tos) {
            			echo '<'.$tis.'>'.htmlentities($tos).'</'.$tis.'>';
            		}
            	echo "</item>";
            }
            echo "</response>";
    }

    public function actionRefurusan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Urusan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Nm_Urusan" => $data['Nm_Urusan']);

            	$i++;
            }

            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionRefbidang()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Bidang')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Nm_Bidang" => $data['Nm_Bidang'],
            		"Kd_Fungsi" => $data['Kd_Fungsi']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionRefsubunit()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Sub_Unit')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"Nm_Sub_Unit" => $data['Nm_Sub_Unit']);
				$i++;
            }
            $json = array(
            	'item' => $item);

            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronrefprogram()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Program')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Prog" => $data['Kd_Prog'],
            		"Ket_Program" => $data['Ket_Program']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_kegiatan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Kegiatan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Prog" => $data['Kd_Prog'],
            		"Kd_Keg" => $data['Kd_Keg'],
            		"Ket_Kegiatan" => $data['Ket_Kegiatan']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_fungsi()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Fungsi')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Fungsi" => $data['Kd_Fungsi'],
            		"Nm_Fungsi" => $data['Nm_Fungsi']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_peraturan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Peraturan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Tahapan" => $data['Kd_Tahapan'],
            		"Kd_Peraturan" => $data['Kd_Peraturan'],
            		"No_ID" => $data['No_ID'],
            		"No_Peraturan" => $data['No_Peraturan'],
            		"Tgl_Peraturan" => $data['Tgl_Peraturan'],
            		"Uraian" => $data['Uraian']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_standard_satuan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Standard_Satuan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Satuan" => $data['Kd_Satuan'],
            		"Uraian" => $data['Uraian']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronsatuan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('satuan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"id" => $data['id'],
            		"satuan" => $data['satuan']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_pagu_indikatif()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Pagu_Sub_Unit')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"pagu" => $data['pagu']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_tupok()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Tupok')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"No_Tupok" => $data['No_Tupok'],
            		"Ur_Tupok" => $data['Ur_Tupok']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Kd_Ssh2" => $data['Kd_Ssh2'],
            		"Kd_Ssh3" => $data['Kd_Ssh3'],
            		"Kd_Ssh4" => $data['Kd_Ssh4'],
            		"Kd_Ssh5" => $data['Kd_Ssh5'],
            		"Kd_Ssh6" => $data['Kd_Ssh6'],
            		"Nama_Barang" => $data['Nama_Barang'],
            		"Kd_Satuan" => $data['Kd_Satuan'],
            		"Harga_Satuan" => $data['Harga_Satuan']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh1')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Nm_Ssh1" => $data['Nm_Ssh1']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh2()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh2')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Kd_Ssh2" => $data['Kd_Ssh2'],
            		"Nm_Ssh2" => $data['Nm_Ssh2']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh3()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh3')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Kd_Ssh2" => $data['Kd_Ssh2'],
            		"Kd_Ssh3" => $data['Kd_Ssh3'],
            		"Nm_Ssh3" => $data['Nm_Ssh3']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh4()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh4')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Kd_Ssh2" => $data['Kd_Ssh2'],
            		"Kd_Ssh3" => $data['Kd_Ssh3'],
            		"Kd_Ssh4" => $data['Kd_Ssh4'],
            		"Nm_Ssh4" => $data['Nm_Ssh4']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_ssh5()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Ssh5')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Ssh1" => $data['Kd_Ssh1'],
            		"Kd_Ssh2" => $data['Kd_Ssh2'],
            		"Kd_Ssh3" => $data['Kd_Ssh3'],
            		"Kd_Ssh4" => $data['Kd_Ssh4'],
            		"Kd_Ssh5" => $data['Kd_Ssh5'],
            		"Nm_Ssh5" => $data['Nm_Ssh5']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_asb()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Asb')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Asb1" => $data['Kd_Asb1'],
            		"Kd_Asb2" => $data['Kd_Asb2'],
            		"Kd_Asb3" => $data['Kd_Asb3'],
            		"Kd_Asb4" => $data['Kd_Asb4'],
            		"Kd_Asb5" => $data['Kd_Asb5'],
            		"Jenis_Pekerjaan" => $data['Jenis_Pekerjaan'],
            		"Kd_Satuan" => $data['Kd_Satuan'],
            		"Harga" => $data['Harga'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_asb1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Asb1')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Asb1" => $data['Kd_Asb1'],
            		"Nm_Asb1" => $data['Nm_Asb1']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_asb2()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Asb2')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Asb1" => $data['Kd_Asb1'],
            		"Kd_Asb2" => $data['Kd_Asb2'],
            		"Nm_Asb2" => $data['Nm_Asb2']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_asb3()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Asb3')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Asb1" => $data['Kd_Asb1'],
            		"Kd_Asb2" => $data['Kd_Asb2'],
            		"Kd_Asb3" => $data['Kd_Asb3'],
            		"Nm_Asb3" => $data['Nm_Asb3']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_asb4()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Asb4')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Asb1" => $data['Kd_Asb1'],
            		"Kd_Asb2" => $data['Kd_Asb2'],
            		"Kd_Asb3" => $data['Kd_Asb3'],
            		"Kd_Asb4" => $data['Kd_Asb4'],
            		"Nm_Asb4" => $data['Nm_Asb4']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_hspk()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Hspk')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Hspk1" => $data['Kd_Hspk1'],
            		"Kd_Hspk2" => $data['Kd_Hspk2'],
            		"Kd_Hspk3" => $data['Kd_Hspk3'],
            		"Kd_Hspk4" => $data['Kd_Hspk4'],
            		"Uraian_Kegiatan" => $data['Uraian_Kegiatan'],
            		"Kd_Satuan" => $data['Kd_Satuan'],
            		"Harga" => $data['Harga'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_hspk1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Hspk1')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Hspk1" => $data['Kd_Hspk1'],
            		"Nm_Hspk1" => $data['Nm_Hspk1'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_hspk2()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Hspk2')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Hspk1" => $data['Kd_Hspk1'],
            		"Kd_Hspk2" => $data['Kd_Hspk2'],
            		"Nm_Hspk2" => $data['Nm_Hspk2'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_hspk3()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Hspk3')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Hspk1" => $data['Kd_Hspk1'],
            		"Kd_Hspk2" => $data['Kd_Hspk2'],
            		"Kd_Hspk3" => $data['Kd_Hspk3'],
            		"Nm_Hspk3" => $data['Nm_Hspk3'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_rek1()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Rek_1')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Rek_1" => $data['Kd_Rek_1'],
            		"Nm_Rek_1" => $data['Nm_Rek_1']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_rek2()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Rek_2')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Rek_1" => $data['Kd_Rek_1'],
            		"Kd_Rek_2" => $data['Kd_Rek_2'],
            		"Nm_Rek_2" => $data['Nm_Rek_2']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_rek3()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Rek_3')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Rek_1" => $data['Kd_Rek_1'],
            		"Kd_Rek_2" => $data['Kd_Rek_2'],
            		"Kd_Rek_3" => $data['Kd_Rek_3'],
            		"Nm_Rek_3" => $data['Nm_Rek_3']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_rek4()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Rek_4')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Rek_1" => $data['Kd_Rek_1'],
            		"Kd_Rek_2" => $data['Kd_Rek_2'],
            		"Kd_Rek_3" => $data['Kd_Rek_3'],
            		"Kd_Rek_4" => $data['Kd_Rek_4'],
            		"Nm_Rek_4" => $data['Nm_Rek_4']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronref_rek5()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ref_Rek_5')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Kd_Rek_1" => $data['Kd_Rek_1'],
            		"Kd_Rek_2" => $data['Kd_Rek_2'],
            		"Kd_Rek_3" => $data['Kd_Rek_3'],
            		"Kd_Rek_4" => $data['Kd_Rek_4'],
            		"Kd_Rek_5" => $data['Kd_Rek_5'],
            		"Peraturan" => $data['Peraturan']
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_misi()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Misi')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"No_Misi" => $data['No_Misi'],
            		"Ur_Misi" => $data['Ur_Misi'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_tujuan()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Tujuan')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"No_Misi" => $data['No_Misi'],
            		"No_Tujuan" => $data['No_Tujuan'],
            		"Ur_Tujuan" => $data['Ur_Tujuan'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_sasaran()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Sasaran')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"No_Misi" => $data['No_Misi'],
            		"No_Tujuan" => $data['No_Tujuan'],
            		"No_Sasaran" => $data['No_Sasaran'],
            		"Ur_Sasaran" => $data['Ur_Sasaran'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }


    public function actionSinkronta_indikator()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Indikator')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"Kd_Prog" => $data['Kd_Prog'],
            		"ID_Prog" => $data['ID_Prog'],
            		"Kd_Keg" => $data['Kd_Keg'],
            		"Kd_Indikator" => $data['Kd_Indikator'],
            		"No_ID" => $data['No_ID'],
            		"Tolak_Ukur" => $data['Tolak_Ukur'],
            		"Target_Angka" => $data['Target_Angka'],
            		"Target_Uraian" => $data['Target_Uraian']);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_fungsi()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Fungsi')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"No_Fungsi" => $data['No_Fungsi'],
            		"Ur_Fungsi" => $data['Ur_Fungsi'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_sub_unit()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Sub_Unit')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"Nm_Pimpinan" => $data['Nm_Pimpinan'],
            		"Nip_Pimpinan" => $data['Nip_Pimpinan'],
            		"Jbt_Pimpinan" => $data['Jbt_Pimpinan'],
            		"Alamat" => $data['Alamat'],
            		"Ur_Visi" => $data['Ur_Visi'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_sub_unit_jab()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Sub_Unit_Jab')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"Kd_Jab" => $data['Kd_Jab'],
            		"No_Urut" => $data['No_Urut'],
            		"Nama" => $data['Nama'],
            		"Nip" => $data['Nip'],
            		"Jabatan" => $data['Jabatan'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

    public function actionSinkronta_hasil()
	{ 
 
			$ssh = (new \yii\db\Query())
                    ->select('*')
                    ->from('Ta_Sub_Unit_Jab')
                    ->all(); 

                    $i = 1;
            foreach($ssh as $data){
            	$item[] = array(
            		"Tahun" => $data['Tahun'],
            		"Kd_Urusan" => $data['Kd_Urusan'],
            		"Kd_Bidang" => $data['Kd_Bidang'],
            		"Kd_Unit" => $data['Kd_Unit'],
            		"Kd_Sub" => $data['Kd_Sub'],
            		"Kd_Jab" => $data['Kd_Jab'],
            		"No_Urut" => $data['No_Urut'],
            		"Nama" => $data['Nama'],
            		"Nip" => $data['Nip'],
            		"Jabatan" => $data['Jabatan'],
            	);
            }
            header('Content-Type: application/json; charset=UTF-8;');

            echo json_encode($item);
    }

   



    public function actionPaguProgram()
    {

    	$unitskpd = Yii::$app->levelcomponent->getUnit();
    	$data = TaPaguSubUnit::find()
    	->where([
    		'Tahun' => date("Y"),
    		'Kd_Urusan' => $unitskpd->Kd_Urusan,
    		'Kd_Bidang' => $unitskpd->Kd_Bidang,
    		'Kd_Unit' => $unitskpd->Kd_Unit,
    		'Kd_Sub' => $unitskpd->Kd_Sub_Unit,
    	])
    	->all();

    	return $this->render('pagu-program', [
    		'data' => $data,
    	]);
    }

    public function actionRincian()
    {
    	return $this->render('rincian', ['cinta' => []]);
    }

}
