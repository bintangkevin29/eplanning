<?php

namespace emusrenbang\controllers;

use yii\helpers\Json;
use emusrenbang\models\TaHasil;
use common\models\TaProgram;
use common\models\TaKegiatan;
use emusrenbang\models\TaBelanja;
use emusrenbang\models\TaBelanjaRinc;
use emusrenbang\models\TaBelanjaRincSub;
use emusrenbang\models\TaIndikator;
use common\models\RefUrusan;
use common\models\RefBidang;
use common\models\RefUnit;
use common\models\RefSubUnit;
use common\models\TaMisi;
use common\models\TaTujuan;
use common\models\TaSasaran;
use common\models\RefFungsi;
use common\models\TaTupok;
use common\models\RefTahapan;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;


class ApiController extends ActiveController
{
    
    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }
    
    public function actionIndex()
    {
        //return $this->render('index');
    }

    public function actionTaHasil($tahapan)
    {
    	//echo 'asdf';
      $tahapan = RefTahapan::findOne(['Uraian' => $tahapan]);
      $id_tahapan = $tahapan->Kd_Tahapan;
      // echo $id_tahapan;
      // die();

     
      return new ActiveDataProvider([
            'query' => TaHasil::find()->where(['Kd_Tahapan' => $id_tahapan]),
            'pagination' => [
                    'pageSize' => 1000,
            ],
	]);

    	
    }

    public function actionTaProgram()
    {
    	//echo 'asdf';
    	$isi_data = TaProgram::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaKegiatan()
    {
    	//echo 'asdf';
    	$isi_data = TaKegiatan::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaBelanja()
    {
    	//echo 'asdf';
    	$isi_data = TaBelanja::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaBelanjaRinc()
    {
    	//echo 'asdf';
    	$isi_data = TaBelanjaRinc::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaBelanjaRincSub()
    {
    	//echo 'asdf';
    	$isi_data = TaBelanjaRincSub::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionRefUrusan()
    {
    	//echo 'asdf';
    	$isi_data = RefUrusan::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionRefBidang()
    {
    	//echo 'asdf';
    	$isi_data = RefBidang::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionRefUnit()
    {
    	//echo 'asdf';
    	$isi_data = RefUnit::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionRefSubUnit()
    {
    	//echo 'asdf';
    	$isi_data = RefSubUnit::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaMisi()
    {
    	//echo 'asdf';
    	$isi_data = TaMisi::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaTujuan()
    {
    	//echo 'asdf';
    	$isi_data = TaTujuan::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaSasaran()
    {
    	//echo 'asdf';
    	$isi_data = TaSasaran::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionRefFungsi()
    {
    	//echo 'asdf';
    	$isi_data = RefFungsi::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaTupok()
    {
    	//echo 'asdf';
    	$isi_data = TaTupok::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

    public function actionTaIndikator()
    {
    	//echo 'asdf';
    	$isi_data = TaIndikator::find()->all();

    	$data = Json::encode($isi_data);
      return $this->renderpartial('index',[
      	'data' => $data,
      ]);
    }

}
