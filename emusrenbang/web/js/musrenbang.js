

$(".lihat_file").click(function(){
  var alamat = $(this).data('url');
  //alert(alamat);
  $('#lihatFileModal').modal('show')
        .find('#isi_modal')
        .html("Loading...");

  $.ajax({ 
    type: "POST",
    url: alamat,
    data:'',
    success: function(isi){
      $('#lihatFileModal')
        .find('#isi_modal')
        .html(isi);
    },
    error: function(){
      alert("failure");
    }
  });
});
