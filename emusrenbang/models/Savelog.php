<?php

namespace emusrenbang\models;

use yii\base\Model;
use Yii;
use emusrenbang\models\Log;

class Savelog extends Model
{
	public function save($controller, $action, $message){
		$model=new Log();

		$model->username=\Yii::$app->user->identity->username;
		$model->ip=$userIP = Yii::$app->request->userIP;	
		$model->controller = $controller;
		$model->action = $action;
		$model->message = $message;
		$model->save();
	}	
}