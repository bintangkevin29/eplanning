<?php

namespace emusrenbang\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property string $username
 * @property string $controller
 * @property string $action
 * @property string $created_at
 * @property string $message
 * @property string $ip
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'controller', 'action', 'message', 'ip'], 'required'],
            [['created_at'], 'safe'],
            [['message'], 'string'],
            [['username', 'controller', 'action', 'ip'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'controller' => 'Controller',
            'action' => 'Action',
            'created_at' => 'Created At',
            'message' => 'Message',
            'ip' => 'Ip',
        ];
    }
}
