<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RefKamusUsulan;

/**
 * RefKamusUsulanSearch represents the model behind the search form about `app\models\RefKamusUsulan`.
 */
class RefKamusUsulanSearch extends RefKamusUsulan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kamus_usulan', 'harga_satuan', 'satuan', 'id_skpd', 'kd_bidang_pem'], 'integer'],
            [['nama_kamus_usulan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefKamusUsulan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kamus_usulan' => $this->id_kamus_usulan,
            'harga_satuan' => $this->harga_satuan,
        ]);

        $query->andFilterWhere(['like', 'nama_kamus_usulan', $this->nama_kamus_usulan])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'id_skpd', $this->id_skpd]);

        return $dataProvider;
    }
}
