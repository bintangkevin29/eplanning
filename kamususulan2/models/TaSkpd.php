<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ta_skpd".
 *
 * @property integer $id_skpd
 * @property string $nama_skpd
 */
class TaSkpd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ta_skpd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_skpd'], 'required'],
            [['nama_skpd'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Skpd',
            'nama_skpd' => 'Nama Skpd',
        ];
    }
}
