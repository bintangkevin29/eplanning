<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanhul */

$this->title = 'Update Kamus Usulan Panai Hulu: ' . $model->id_kamus_usulan;
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Panai Hulu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kamus_usulan, 'url' => ['view', 'id' => $model->id_kamus_usulan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-kamus-usulan-panhul-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
