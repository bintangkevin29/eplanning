<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanhul */

$this->title = $model->id_kamus_usulan;
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Panai Hulu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-panhul-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_kamus_usulan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_kamus_usulan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kamus_usulan',
            'nama_kamus_usulan',
            // 'satuan',
            // 'harga_satuan',
            // 'id_skpd',
            // 'kd_bidang_pem',
            [
                'attribute' => 'satuan',
                'value' => function($data){return $data->satuan1->Uraian;}
            ],

            //'harga_satuan',
            [
                //'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'harga_satuan',
                'value' => function ($model) { return number_format("".$model->harga_satuan, "2", ",", ".");}
                
            ],
            [
                'attribute' => 'skpd_pelaksana',
                'value' => function($data){return $data->skpd->nama_skpd;}
            ],
            //'skpd_pelaksana',
            //'kd_bidang_pem',
            [
                'attribute' => 'kd_bidang_pem',
                'value' => function($data){return $data->pembangunan->Bidang_Pembangunan;}
            ],
            'asal_usulan',
        ],
    ]) ?>

</div>
