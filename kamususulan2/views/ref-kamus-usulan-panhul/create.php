<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanhul */

$this->title = 'Tambah Kamus Usulan Panai Hulu';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Panai Hulu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-panhul-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
