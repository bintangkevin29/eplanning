<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulan */

$this->title = 'Create Ref Kamus Usulan';
$this->params['breadcrumbs'][] = ['label' => 'Ref Kamus Usulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formssh', [
        'model' => $model,
    ]) ?>

</div>
