<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\i18n\numberFormatter;

//use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RefKamusUsulanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile(
    '@web/js/sistem/kamus_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->title = 'Refensi Kamus Usulan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Kamus Usulan', ['create'], ['class' => 'btn btn-success']) ?>
       
    </p>
    <p>
        <?= Html::a('Tambah Kamus Usulan SSH', ['pilihssh'], ['class' => 'btn btn-success']) ?>
       
    </p>
   
       
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kamus_usulan',
            'nama_kamus_usulan',
            //'satuan',
            [
                'attribute' => 'satuan',
                'value' => function($data){return $data->satuan1->Uraian;}
            ],

            //'harga_satuan',
            [
                //'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'harga_satuan',
                'value' => function ($model) { return number_format("".$model->harga_satuan, "2", ",", ".");}
                
            ],

            [
                'attribute' => 'skpd_pelaksana',
                'value' => function($data){return $data->skpd->nama_skpd;}
            ],
            //'skpd_pelaksana',
            //'kd_bidang_pem',
            [
                'attribute' => 'kd_bidang_pem',
                'value' => function($data){return $data->pembangunan->Bidang_Pembangunan;}
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
