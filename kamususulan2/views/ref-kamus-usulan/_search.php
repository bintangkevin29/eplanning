<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\RefKamusUsulanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-kamus-usulan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_kamus_usulan') ?>

    <?= $form->field($model, 'nama_kamus_usulan') ?>

    <?= $form->field($model, 'satuan') ?>

    <?= $form->field($model, 'harga_satuan') ?>

    <?= $form->field($model, 'id_skpd') ?>

    <?= $form->field($model, 'kd_bidang_pem') ?>
    
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
