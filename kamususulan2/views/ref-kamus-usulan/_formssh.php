<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widget\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulan */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Tambah Kamus Usulan SSH';
$this->params['breadcrumbs'][] = ['label' => 'Ref Kamus Usulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="ref-kamus-usulan-form">
    <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-container">
                    <div class=" widget-block">


                        <div class="form-group required">
                            <label class="control-label col-sm-3" for="total" align="left"></label>
                            <div class="col-sm-12">
                                
                                    
                                
                                <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>

                        
                        
                                                
                        <?= $form->field($model, 'nama_kamus_usulan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

                        <?= $form->field($model, 'satuan')->dropDownList(
                            ArrayHelper::map(app\models\RefStandardSatuan::find()->all(),'Kd_Satuan','Uraian'),['prompt' => 'Pilih Satuan', 'class' => 'form-control select2-allow-clear']) 
                        ?>
                   
                         <?= $form->field($model, 'satuan')->dropDownList(
                            ArrayHelper::map(app\models\RefStandardSatuan::find()->all(),'Kd_Satuan','Uraian'),['prompt' => 'Pilih Satuan', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                       <?= $form->field($model, 'harga_satuan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

                        <?= $form->field($model, 'id_skpd')->dropDownList(
                            ArrayHelper::map(app\models\TaSkpd::find()->all(),'id','nama_skpd'),['prompt' => 'Pilih SKPD', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'kd_bidang_pem')->dropDownList(ArrayHelper::map(app\models\RefBidangPembangunan::find()->all(),'Kd_Pem','Bidang_Pembangunan'),['prompt' => 'Pilih Bidang', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="total"></label>
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                        
                    </div>
                 </div>
            </div>
       
         </div>

    </div>

</div>