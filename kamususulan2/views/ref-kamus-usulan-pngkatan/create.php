<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPngkatan */

$this->title = 'Tambah Kamus Usulan Pangkatan';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Pangkatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-pngkatan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
