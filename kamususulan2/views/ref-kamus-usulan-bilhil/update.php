<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanBilhil */

$this->title = 'Update Kamus Usulan Bilah Hilir: ' . $model->id_kamus_usulan;
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Bilah Hilir', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kamus_usulan, 'url' => ['view', 'id' => $model->id_kamus_usulan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-kamus-usulan-bilhil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
