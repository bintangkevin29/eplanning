<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TaSkpdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ta Skpds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-skpd-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ta Skpd', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_skpd',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
