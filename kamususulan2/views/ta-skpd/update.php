<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaSkpd */

$this->title = 'Update Ta Skpd: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ta Skpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ta-skpd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
