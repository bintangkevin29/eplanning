<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaSkpd */

$this->title = 'Create Ta Skpd';
$this->params['breadcrumbs'][] = ['label' => 'Ta Skpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ta-skpd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
