<?php
$this->registerJsFile(
    '@web/js/form_ssh_hspk.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
//print_r($data);
//echo $data[0]['Kategori_Pekerjaan_Nama'];
//die();
if ($data):
?>
  <table class="tabel-hasil">
    <thead>
      <tr>
        <th>Aksi</th>
        <th>NOMOR</th>
        <th>KATEGORI</th>
        <th>URAIAN KEGIATAN</th>
        <th>KOEF.</th>
        <th>SAT</th>
        <th>HARGA SATUAN</th>
        <th>HARGA</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $tot_harga = 0;
        foreach ($data as $key => $value) :
            $Kd1 = $value['Kd1'];
            $Kd2 = $value['Kd2'];
            $Kd3 = $value['Kd3'];
            $Kd4 = $value['Kd4'];
            $Kd5 = $value['Kd5'];
            $Kd6 = $value['Kd6'];
            $Kategori_Pekerjaan = $value['Kategori_Pekerjaan'];
            $Nama_Pekerjaan = $value['Nama_Pekerjaan'];
            $Satuan = $value['Satuan'];
            $Harga_Satuan = $value['Harga_Satuan'];
            $Koefisien = $value['Koefisien'];
            $Harga = $value['Harga'];
            $Uraian = $value['Uraian'];
            $Kd_Satuan = $value['Kd_Satuan'];
            $Asal = $value['Asal'];

            if ($Asal == 1) {
              $nomor = $Kd1.".".$Kd2.".".$Kd3.".".$Kd4.".".$Kd5.".".$Kd6;
            }
            else if($Asal == 2){
              $nomor = $Kd1.".".$Kd2.".".$Kd3.".".$Kd4;
            }
            else if($Asal == 3){
              $nomor = $Kd1.".".$Kd2.".".$Kd3.".".$Kd4.".".$Kd5;
            }
            $tot_harga += $Harga;

          ?>
            <tr><!-- kategori -->
              <td><button type="button" class="btn btn-danger btn-xs hapus_cookie" data-key=<?= $key ?>><i class="fa fa-trash"></i></button></td>
              <td><?= $nomor ?></td>
              <td><?= $Nama_Pekerjaan ?></td>
              <td><?= $Uraian ?></td>
              <td><?= $Koefisien ?></td>
              <td><?= $Satuan ?></td>
              <td><?= $Harga_Satuan ?></td>
              <td><?= $Harga ?></td>
            </tr>
          <?php
        endforeach;
      ?>
      <tr class="akhir"> <!-- toal akhir semua jumlah -->
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-right text-bold">Nilai HSPK</td>
        <td class="uang jumlah" id="jumlah_hspk"><?= $tot_harga ?></td>
      </tr>
      <!-- akhir kegiatan -->
    </tbody>
  </table>

<script type="text/javascript">
  $(".hapus_cookie").click(function(){
    var key = $(this).data('key');
    //alert('index.php?r=ajax/del-cookie&key='+key);
    $.ajax({
      type: "GET",
      url:'index.php?r=ajax/del-cookie',
      data:{key:key},
      success: function(isi){
        get_data_cookie();
      },
      error: function(){
        alert("failure");
      }
    });
  });
</script>
<?php endif; ?>