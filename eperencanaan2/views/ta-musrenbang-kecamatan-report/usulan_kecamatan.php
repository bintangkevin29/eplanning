<?php

use yii\helpers\Html;

$this->title = 'Usulan Kecamatan';

//$this->params['breadcrumbs'][] ='';
$this->params['breadcrumbs'][] = ['label' => 'Usulan', 'url' => ['#']];
$this->params['breadcrumbs'][] = $this->title;

// $this->registerJsFile(
//     '@web/js/musrenbang/usulan_prioritas.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );


?>

<div class="col-md-12">
	
	<div class="control-wrap">
      <?php $form = \yii\bootstrap\ActiveForm::begin([
      						'id' => 'search-usulan',
                  'action' => ['ta-musrenbang-kecamatan-report/usulan-kecamatan-cetak'], 
                  'options' => ['target' => '_blank']
      ]) ?>
      <div class="form-group">
          <div class="col-sm-2">
              <br>
              <?= Html::submitButton('&nbsp;Cetak&nbsp;', ['id' => 'cari-submit', 'class' => 'btn btn-primary btn-lg']); ?>
          </div>
      </div>
      <?php \yii\bootstrap\ActiveForm::end() ?>
  </div>
  
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Asal Usulan</th>
				<th>Kegiatan Prioritas</th>
				<th>Desa/Kelurahan</th>
				<th>Dusun/Lingkungan</th>
				<th>Jalan</th>
				<th>Jumlah/vol</th>
				<th>Pagu (Rp)</th>
				<th>PD Penanggung Jawab</th>
				<th>Prioritas Pembangunan</th>
			</tr>
		</thead>
		<tbody id="isi-wrap">
			<!--insfrastruktur -->
			<tr>
				<td colspan="11">
					<h3>Infrastruktur</h3>
				</td>
			</tr>
			<?php
			$jumTot [] = 0;

				$no=0;
				foreach ($data_infrastruktur as $val) :
					$id = $val->id;
					$no++;
					if ($val->Kd_Jalan)
						$jalan = $val->kdJalan->Nm_Jalan;
					else
						$jalan = '';

					if($val->Kd_Prioritas_Pembangunan_Daerah)
						$rpjmd_pilih = $val->rpjmd->Nm_Prioritas_Pembangunan_Kota;
					else
						$rpjmd_pilih = 'Non Prioritas';

					if ($val->Kd_Asal_Usulan == 1) {
						$asal_usulan = "Lingkungan";
					}
					else if ($val->Kd_Asal_Usulan == 2) {
						$asal_usulan = "Kelurahan";
					}
					else if ($val->Kd_Asal_Usulan == 3) {
						$asal_usulan = "Kecamatan";
					}
					else {
						$asal_usulan = "Tidak Ditemukan";
					}

					//$val->Status_Penerimaan_Kecamatan
					 $jumTot[] = $val->Harga_Total;
					?>
					<tr>
						<td>
							<?= $no ?>
						</td>
						<td>
							<?= $asal_usulan ?>
						</td>
						<td>
							<b>Permasalahan:</b><br/>
					    <p><?= $val->Nm_Permasalahan ?></p>
					    <b>Usulan:</b>
					    <p><?= $val->Jenis_Usulan ?></p>
					    (<?= $val->bidangPembangunan->Bidang_Pembangunan ?>)
						</td>
					  <td>
					  	<?php if($val->Kd_Kel) echo $val->kelurahan->Nm_Kel ?>
					  </td>
					  <td>
					  	<?php if($val->Kd_Lingkungan) echo $val->lingkungan->Nm_Lingkungan ?>
					  </td>
					  <td>
					  	<?php if ($val->Kd_Jalan) echo $val->kdJalan->Nm_Jalan ?> <br/>
					  	<?= $val->Detail_Lokasi ?>
					  </td>
						<td><?= $val->Jumlah.' '.$val->satuan->Uraian; ?></td>
						<td><?= number_format($val->Harga_Total,0, ',', '.') ?></td>
						
						<td align='center'>
					    	<?= $val->skpd->skpd ?>
						</td>
						<td align="center">
					    	<?= $rpjmd_pilih ?>
						</td>
					</tr>
					<?php
				endforeach;
			?>

			<tr>
                <td colspan="9" style="text-align: center"><b>Jumlah</b></td><td style="text-align: right"><b><?php echo 'Rp. '. \Yii::$app->zultanggal->ZULgetcurrency(array_sum($jumTot)); ?></b></td>
            </tr> 
			<!--sosialbudaya-->

			<tr>
				<td colspan="11">
					<h3>Sosial Budaya</h3>
				</td>
			</tr>
			<?php
			$jumTot2 [] = 0;

				$no=0;
				foreach ($data_sosbud as $val) :
					$id = $val->id;
					$no++;
					if ($val->Kd_Jalan)
						$jalan = $val->kdJalan->Nm_Jalan;
					else
						$jalan = '';

					if($val->Kd_Prioritas_Pembangunan_Daerah)
						$rpjmd_pilih = $val->rpjmd->Nm_Prioritas_Pembangunan_Kota;
					else
						$rpjmd_pilih = 'Non Prioritas';

					if ($val->Kd_Asal_Usulan == 1) {
						$asal_usulan = "Lingkungan";
					}
					else if ($val->Kd_Asal_Usulan == 2) {
						$asal_usulan = "Kelurahan";
					}
					else if ($val->Kd_Asal_Usulan == 3) {
						$asal_usulan = "Kecamatan";
					}
					else {
						$asal_usulan = "Tidak Ditemukan";
					}

					//$val->Status_Penerimaan_Kecamatan
					$jumTot2[] = $val->Harga_Total;
					?>
					<tr>
						<td>
							<?= $no ?>
						</td>
						<td>
							<?= $asal_usulan ?>
						</td>
						<td>
							<b>Permasalahan:</b><br/>
					    <p><?= $val->Nm_Permasalahan ?></p>
					    <b>Usulan:</b>
					    <p><?= $val->Jenis_Usulan ?></p>
					    (<?= $val->bidangPembangunan->Bidang_Pembangunan ?>)
						</td>
					  <td>
					  	<?php if($val->Kd_Kel) echo $val->kelurahan->Nm_Kel ?>
					  </td>
					  <td>
					  	<?php if($val->Kd_Lingkungan) echo $val->lingkungan->Nm_Lingkungan ?>
					  </td>
					  <td>
					  	<?php if ($val->Kd_Jalan) echo $val->kdJalan->Nm_Jalan ?> <br/>
					  	<?= $val->Detail_Lokasi ?>
					  </td>
						<td><?= $val->Jumlah.' '.$val->satuan->Uraian; ?></td>
						<td><?= number_format($val->Harga_Total,0, ',', '.') ?></td>
						<!-- <?php $jumTot2[] = $val->Harga_Total;?> -->
						<td align='center'>
					    	<?= $val->skpd->skpd ?>
						</td>
						<td align="center">
					    	<?= $rpjmd_pilih ?>
						</td>
					</tr>
					<?php
				endforeach;
			?>

			<tr>
                <td colspan="9" style="text-align: center"><b>Jumlah</b></td><td style="text-align: right"><b><?php echo 'Rp. '. \Yii::$app->zultanggal->ZULgetcurrency(array_sum($jumTot2)); ?></b></td>
            </tr> 

			<!--ekonomi-->
			<tr>
				<td colspan="11">
					<h3>Ekonomi</h3>
				</td>
			</tr>
			<?php
			$jumTot3 [] = 0;
				$no=0;
				foreach ($data_ekonomi as $val) :
					$id = $val->id;
					$no++;
					if ($val->Kd_Jalan)
						$jalan = $val->kdJalan->Nm_Jalan;
					else
						$jalan = '';

					if($val->Kd_Prioritas_Pembangunan_Daerah)
						$rpjmd_pilih = $val->rpjmd->Nm_Prioritas_Pembangunan_Kota;
					else
						$rpjmd_pilih = 'Non Prioritas';

					if ($val->Kd_Asal_Usulan == 1) {
						$asal_usulan = "Lingkungan";
					}
					else if ($val->Kd_Asal_Usulan == 2) {
						$asal_usulan = "Kelurahan";
					}
					else if ($val->Kd_Asal_Usulan == 3) {
						$asal_usulan = "Kecamatan";
					}
					else {
						$asal_usulan = "Tidak Ditemukan";
					}

					//$val->Status_Penerimaan_Kecamatan
					$jumTot3[] = $val->Harga_Total;
					?>
					<tr>
						<td>
							<?= $no ?>
						</td>
						<td>
							<?= $asal_usulan ?>
						</td>
						<td>
							<b>Permasalahan:</b><br/>
					    <p><?= $val->Nm_Permasalahan ?></p>
					    <b>Usulan:</b>
					    <p><?= $val->Jenis_Usulan ?></p>
					    (<?= $val->bidangPembangunan->Bidang_Pembangunan ?>)
						</td>
					  <td>
					  	<?php if($val->Kd_Kel) echo $val->kelurahan->Nm_Kel ?>
					  </td>
					  <td>
					  	<?php if($val->Kd_Lingkungan) echo $val->lingkungan->Nm_Lingkungan ?>
					  </td>
					  <td>
					  	<?php if ($val->Kd_Jalan) echo $val->kdJalan->Nm_Jalan ?> <br/>
					  	<?= $val->Detail_Lokasi ?>
					  </td>
						<td><?= $val->Jumlah.' '.$val->satuan->Uraian; ?></td>
						<td><?= number_format($val->Harga_Total,0, ',', '.') ?></td>
						<!-- <?php $jumTot3[] = $val->Harga_Total;?> -->
						<td align='center'>
					    	<?= $val->skpd->skpd ?>
						</td>
						<td align="center">
					    	<?= $rpjmd_pilih ?>
						</td>
					</tr>
					<?php
				endforeach;
			?>

			<tr>
                <td colspan="9" style="text-align: center"><b>Jumlah</b></td><td style="text-align: right"><b><?php echo 'Rp. '. \Yii::$app->zultanggal->ZULgetcurrency(array_sum($jumTot3)); ?></b></td>
            </tr>
		</tbody>
	</table>
</div>