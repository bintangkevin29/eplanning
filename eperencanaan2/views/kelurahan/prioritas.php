<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Jabatans */

$this->title = 'Tambah Dokumen';
$this->params['subtitle'] = 'Dokumen';

//$this->params['breadcrumbs'][] ='';
$this->params['breadcrumbs'][] = ['label' => 'Dokumen', 'url' => ['']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="main-container">
			<div class="container-fluid">
				<div class="page-breadcrumb">
					<div class="row">
						<div class="col-md-7">
							<div class="page-breadcrumb-wrap">
								<div class="page-breadcrumb-info">
									<h2 class="breadcrumb-titles">Prioritas Pembangunan Kabupaten Labuhanbatu </h2>
									<ul class="list-page-breadcrumb">
										<li><a href="#">Beranda</a>
										</li>
										<li class="active-page"> Prioritas Pembangunan Kabupaten Labuhanbatu</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-5">
						</div>
					</div>
				</div>
                
                
                 <div class="alert alert-info" role="alert">
										Penjelasan, pengumuman atau keterangan dapat diletakkan disini untuk memberikan informasi lengkap ke  user 
				</div>
				<div class="row">
					<div class="col-md-12">
                        	<div class="box-widget widget-module">
							<div class="widget-container">
								<div class=" widget-block">
                                    
                                     <div class="section-header">
                                         <h2>Prioritas Pembangunan Kabupaten Labuhanbatu  </h2> 
                                            </div>
                                   
                                    1. Peningkatan ketersediaan dan kualitas infrasruktur serta Utilitas Kota. <br>
                                    2. Peningkatan akses dan kualitas pendidikan masyarakat.<br>
                                    3. Peningkatan akses dan kualitas kesehatan masyrakat. <br> 
                                    4. Peningkatan keterbukaan dan akuntabilitas publik. <br>
                                    5. Peningkatan penataan ruang dan kualitas lingkungan hidup.<br> 
                                    6. Peningkatan penanaman modal daerah serta kemudahan pelayanan perizinan / non perizinan.<br>
                                    7. Peningkatan efektifitas kelembagaan dan pelayanan kepegawaian daerah. <br>
                                    8. Peningkatan kesempatan kerja dan lapangan kerja.<br> 
                                    9. Peningkatan suasana kehidupan yang harmonis, saling menghormati, aman dan damai. <br>
                                    10.Sepuluh pengintegrasian program penangulangan kemiskinan kota  
                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>