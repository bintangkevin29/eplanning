<?php

use yii\helpers\Html;
use common\components\Helper;
use yii\grid\GridView;
use yii\widgets\LinkPager;

$this->registerJsFile(
    '@web/js/musrenbang/lihat_usulan.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>

<!-- page wrapper -->
<div class="dev-page">
    
    <!-- page header -->    
    <div class="dev-page-header">
        <div class="dph-logo">
            <img src="img/logo.png" height="40">
            <span class="judul-logo">E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></span>
            <a class="dev-page-sidebar-collapse">
                <div class="dev-page-sidebar-collapse-icon">
                    <span class="line-one"></span>
                    <span class="line-two"></span>
                    <span class="line-three"></span>
                </div>
            </a>
        </div>
    </div>
    <!-- ./page header -->
    
    <!-- page container -->
    <div class="dev-page-container">

        <!-- page sidebar -->
        <?php include "leftpage.php"; ?>    
        <!-- ./page sidebar -->
        
        <!-- page content -->
        <div class="dev-page-content">                    
            <!-- page content container -->
            <div class="container">

                <!-- page title -->
                <div class="page-title" id="tour-step-4">
                    <h1>E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></h1>
                    <h5>E-Perencanaan <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?> merupakan sebuah aplikasi yang dibangun untuk membuat rencana pembangunan daerah mulai dari tingkat lingkungan sampai kota.</h5>
                </div>
                <!-- ./page title -->
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper wrapper-white">
                            <div class="page-subtitle">
                                <h3>Laporan Rencana Kerja</h3>
                            </div>
                            <div class="row">
                                <form id="form_cari">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >Sub Unit</label>
                                            <select class="form-control" name="Kd_Sub" id="sub-unit">
                                                <option value="">- Pilih Sub Unit -</option>
                                                <?php foreach ($subunit as $key => $value): ?>
                                                    <option value="<?= $value->Kd_Urusan.'.'.$value->Kd_Bidang.'.'.$value->Kd_Unit.'.'.$value->Kd_Sub ?>"><?= $value->Nm_Sub_Unit ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label >&nbsp;</label>
                                            <button type="button" class="form-control btn btn-success" id="btn-lihat-laporan-renja">Lihat</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            
                            <div class="row" id="isi-wrap">

                            </div>

                        </div>
                    </div>
                </div>

                <!-- Copyright -->
                <div class="copyright">
                    <div class="pull-left">
                         &copy; <?= Yii::$app->pengaturan->getTahun() ?> <strong>BAPPEDA <?= Yii::$app->pengaturan->Kolom('Nm_Pemda'); ?></strong>. All rights reserved.
                    </div>
                </div>
                <!-- ./Copyright -->

            </div>
            <!-- ./page content container -->
                                
        </div>
        <!-- ./page content -->                                               
    </div>  
    <!-- ./page container -->
    
    <!-- page footer -->    
    <div class="dev-page-footer dev-page-footer-fixed"> <!-- dev-page-footer-closed dev-page-footer-fixed -->
    </div>
    <!-- ./page footer -->
    
</div>
<!-- ./page wrapper -->