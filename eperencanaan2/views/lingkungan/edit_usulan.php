<?php
$request = Yii::$app->request;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefStandardSatuan; 
use common\models\RefJalan; 

//use common\models\RefKamusUsulan;
use common\models\RefKamusUsulanBilbar;
use common\models\RefKamusUsulanBilhil;
use common\models\RefKamusUsulanBilhul;
use common\models\RefKamusUsulanPanhil;
use common\models\RefKamusUsulanPanhul;
use common\models\RefKamusUsulanPanteng;
use common\models\RefKamusUsulanPngkatan;
use common\models\RefKamusUsulanRansel;
use common\models\RefKamusUsulanRanut;
use yii\helpers\Json;
use kartik\select2\Select2;
//$user=Yii::$app->levelcomponent->getKelompok();


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\Lingkungan */
/* @var $form yii\widgets\ActiveForm */
/*
$this->registerJsFile(
    '@web/js/sistem/jquery.number.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/jquery.priceformat.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
*/
$this->registerJsFile(
    '@web/js/sistem/jquery.number.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/lingkungan_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->title = 'Edit Usulan Rembuk Warga';
$this->params['subtitle'] = 'Usulan';

//$this->params['breadcrumbs'][] ='';
$this->params['breadcrumbs'][] = ['label' => 'Usulan', 'url' => ['lingkungan/usulan']];
$this->params['breadcrumbs'][] = $this->title;

//$pesan = $request->get('pesan');
$PC_Kelompok = Yii::$app->levelcomponent->getKelompok();
$PC_InfoUser = Yii::$app->levelcomponent->getProfile();
$PC_NamaLingkungan =  Yii::$app->levelcomponent->getNamaLingkungan();
$nama_user='';
$nama_lingkungan='';
$Nm_Kec='';
$Nm_Kec2 = '';
$Nm_Kel='';

?>

<div class="row">
    <div class="col-md-12">
        <div class="box-widget widget-module">
            <div class="widget-container">
                <div class=" widget-block">
                    <?php 
                        //'Tahun', 'Kd_Prov', 'Kd_Kab', 'Kd_Kec', 'Kd_Kel', 'Kd_Urut_Kel', 
                        //'Kd_Lingkungan', 'Kd_Jalan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 
                        //'Kd_Keg', 'Nm_Permasalahan', 'Kd_Klasifikasi', 'Jenis_Usulan', 
                        //'Jumlah', 'Kd_Satuan', 'Harga_Satuan', 'Harga_Total', 'Kd_Sasaran', 'Tanggal' 
                    ?>
                    <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>
                    <?= $form->field($model, 'Kd_Urusan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Bidang', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Prog', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Keg', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Klasifikasi', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                    <?= $form->field($model, 'Kd_Sasaran', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>

                    <?= $form->field($model, 'Nm_Permasalahan')->textarea(['maxlength' => true, ]) ?>

                    <?php
                        if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Utara'){
                            $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                          //$Nm_Kec2 = "Berhasil Kecamatan 1";
                    ?>
                                       
                                        <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                            'data'=>$Iwankamus1, 
                                            'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus1'],
                                            'pluginOptions' => ['select2-allow-clear'=>true],

                                        ])->label('Kamus Usulan'); ?>
                    <?php
                      
                    }?>
                    <?php

                if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Selatan'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 2";
                ?>
                                
                                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus2,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus2'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php

                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Barat'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 3";
                ?>
                    
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus3,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus3'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hilir'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 4";
                ?>
                    
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus4,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus4'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hulu'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 5";
                ?>
               
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus5,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus5'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Pangkatan'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 6";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus6,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus6'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Tengah'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 7";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus7,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus7'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hilir'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 8";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus8,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus8'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hulu'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 9";
                ?>
               
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Kecamatan Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 10";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

                    
                    <?= $form->field($model, 'Jenis_Usulan')->textarea(['maxlength' => true, 'readonly'=>true]) ?>
		            
                    <?= $form->field($model, 'Kd_Pem')->dropdownList(
                            $ZULbidpem, ['prompt' => 'Pilih Bidang', 'class' => 'form-control', 'id' => 'bidpem', 'readonly' => true]) 
                    ?>

                    <?= $form->field($model, 'Jumlah')->textInput(['maxlength' => true, 'class' => 'form-control hitung', 'id' => 'jumlah', 'autocomplete' => 'off']) ?>
                    <?= 
                        $form->field($model, 'Kd_Satuan')->dropdownList(
                           $ZULsatuan,
                            ['prompt'=>'Pilih Satuan', 'class' => 'form-control select2-allow-clear', 'readonly'=>true])
                    ?>
                    
                    <?= $form->field($model, 'Harga_Satuan')->textInput(['maxlength' => true, 'class' => 'form-control nomor hitung', 'id' => 'harga', 'autocomplete' => 'off','readonly'=>true]) ?>

                    <?php /* $form->field($model, 'Harga_Satuan')->widget(MaskMoney::classname(), [
                                                                    'pluginOptions' => [
                                                                        'precision' => 2,
                                                                        'thousands' => '.',
                                                                        'decimal' => ',',
                                                                    ],
                                                                    'options' => [ 
                                                                        'id' => 'harga_satuan',
                                                                        'class' => 'hitung',
                                                                    ]
                                                                ]);
                         */                                       
                    ?>
 
                    <?= $form->field($model, 'Harga_Total')->textInput(['maxlength' => true, 'class' => 'form-control nomor', 'id' => 'total', 'readonly' => true]) ?>
                    
                    <?php /* $form->field($model, 'Harga_Total')->widget(MaskMoney::classname(), [
                                                                    'pluginOptions' => [
                                                                        'precision' => 2,
                                                                        'thousands' => '.',
                                                                        'decimal' => ',',
                                                                    ],
                                                                    'options' => [ 
                                                                        'id' => 'total',
                                                                        'readonly' => true,
                                                                    ]
                                                                ]);  
                        */  
                    ?>

                    <?= $form->field($model, 'Detail_Lokasi')->textarea(['maxlength' => true]) ?>

                    <?php
                    $dl_jalan = Yii::$app->levelcomponent->getKelompok();
                    echo $form->field($model, 'Kd_Jalan')->dropdownList(
                            ArrayHelper::map(RefJalan::find()->orderBy('Nm_Jalan')->where(
                                            [ 'Kd_Prov' => $dl_jalan->Kd_Prov,
                                                'Kd_Kab' => $dl_jalan->Kd_Kab,
                                                'Kd_Kec' => $dl_jalan->Kd_Kec,
                                                'Kd_Kel' => $dl_jalan->Kd_Kel,
                                                'Kd_Urut_Kel' => $dl_jalan->Kd_Urut_Kel,
                                                'Kd_Lingkungan'=>$dl_jalan->Kd_Lingkungan]
                                    )->all(), 'Kd_Jalan', 'Nm_Jalan'), ['prompt' => 'Pilih Jalan', 'class' => 'form-control select2-allow-clear'])
                    ?>
                    <!--
                    <input type="text" name="" value="<?= $dl_jalan->Kd_Prov.",".
                                                            $dl_jalan->Kd_Kab.",".
                                                            $dl_jalan->Kd_Kec.",".
                                                            $dl_jalan->Kd_Kel.",".
                                                            $dl_jalan->Kd_Urut_Kel.",".
                                                            $dl_jalan->Kd_Lingkungan 
                                                        ?>">
                    -->
                    <!--
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <?= Html::submitButton('Tambah', ['class' =>  'btn btn-primary']) ?>
                    </div>
                    -->
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                    <?php
                        /*
                        'Tahun' => 'Tahun',
                        'Kd_Ta_Forum_Lingkungan' => 'Kd  Ta  Forum  Lingkungan',
                        'Kd_Prov' => 'Kd  Prov',
                        'Kd_Kab' => 'Kd  Kab',
                        'Kd_Kec' => 'Kd  Kec',
                        'Kd_Kel' => 'Kd  Kel',
                        'Kd_Urut_Kel' => 'Kd  Urut  Kel',
                        'Kd_Lingkungan' => 'Kd  Lingkungan',
                        'Kd_Jalan' => 'Jalan',
                        'Kd_Urusan' => 'Kd  Urusan',
                        'Kd_Bidang' => 'Kd  Bidang',
                        'Kd_Prog' => 'Kd  Prog',
                        'Kd_Keg' => 'Kd  Keg',
                        'Nm_Permasalahan' => ' Permasalahan',
                        'Kd_Klasifikasi' => 'Kd  Klasifikasi',
                        'Jenis_Usulan' => 'Jenis  Usulan',
                        'Jumlah' => 'Jumlah',
                        'Kd_Satuan' => 'Satuan',
                        'Harga_Satuan' => 'Harga  Satuan',
                        'Harga_Total' => 'Harga  Total',
                        'Kd_Sasaran' => 'Kd  Sasaran',
                        */
                    ?>
                </div>
            </div>
        </div>
       
    </div>

</div>
<?php
    $kamususulan = <<< JS
    $('#kdKamus').change(function(){
         var idKamus = $(this).val();
         //alert(kdKamus);
         $.get('index.php?r=lingkungan/kamus',{ idKamus : idKamus },function(data){
            var data = $.parseJSON(data);
            //alert(data.nama_kamus_usulan);
            $('#coba').attr('value',data.nama_kamus_usulan);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',1);
            //alert(satuan1);
            //$('#satuan option:[value = "2"]').attr("selected", "selected");
             $.get('index.php?r=lingkungan/satuan',{ satuan1 : satuan1 },function(data1){
                //alert(data1.Uraian);
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');            
                //alert(data1.Uraian);
            });
            $.get('index.php?r=lingkungan/bidpem',{ bidpem1 : bidpem1 },function(data2){
                //alert(data2.Bidang_Pembangunan);
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');            
                //alert(data1.Uraian);
            });
         });
    });
JS;
$this->registerJs($kamususulan);
?>