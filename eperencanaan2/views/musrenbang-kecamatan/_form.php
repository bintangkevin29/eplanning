<?php
$request = Yii::$app->request;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefStandardSatuan; 
use common\models\RefJalan; 

//use common\models\RefKamusUsulan;
use common\models\RefKamusUsulanBilbar;
use common\models\RefKamusUsulanBilhil;
use common\models\RefKamusUsulanBilhul;
use common\models\RefKamusUsulanPanhil;
use common\models\RefKamusUsulanPanhul;
use common\models\RefKamusUsulanPanteng;
use common\models\RefKamusUsulanPngkatan;
use common\models\RefKamusUsulanRansel;
use common\models\RefKamusUsulanRanut;
use yii\helpers\Json;
use kartik\select2\Select2;
//$user=Yii::$app->levelcomponent->getKelompok();


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\Lingkungan */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(
    '@web/js/sistem/jquery.number.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/sistem/lingkungan_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

// $this->registerJsFile(
//     '@web/js/sistem/usulankec.js',
//     ['depends' => [\yii\web\JqueryAsset::className()]]
// );

// $Kd_Sub = array();
// $Kd_Bidang = array();
// $Kd_Asal = array();
// $dataSH = array();
// $Kd_1 = array();
// $Kd_2 = array();
// $Kd_3 = array();
// $Kd_4 = array();
// $Kd_5 = array();
// $Kd_6 = array();

$this->title = 'Tambah Usulan Kecamatan';
$this->params['subtitle'] = 'Usulan';


$this->registerJs("
    $('#asalsatuan').on('click', function(e){
        e.preventDefault();
        $('#satuan').toggle();
    })
");

$this->registerJs("
    $('#asalmanual').on('click', function(e){
        e.preventDefault();
        $('#manual').toggle();
    })
");


$PC_Kelompok = Yii::$app->levelcomponent->getKelompok();
$PC_InfoUser = Yii::$app->levelcomponent->getProfile();
$PC_NamaLingkungan =  Yii::$app->levelcomponent->getNamaLingkungan();
$nama_user='';
$nama_lingkungan='';
$Nm_Kec='';
$Nm_Kec2 = '';
$Nm_Kel='';

?>
<div class="ta-musrenbang-form">
<div class="row">
    <div class="col-md-12">
        <div class="box-widget widget-module">
            <div class="widget-container">
                <div class=" widget-block">
                   
               
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>

                
                <?= $form->field($model, 'Kd_Prog', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                <?= $form->field($model, 'Kd_Keg', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                <?= $form->field($model, 'Kd_Sasaran', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                <?= $form->field($model, 'Status_Usulan', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>
                <?= $form->field($model, 'Kd_Klasifikasi', ['options' => ['class' => 'form-group-hide']])->textInput(['value'=>'0'])->label(false); ?>

                <?= $form->field($model, 'Kd_Urusan')->textInput(['maxlength' => true, 'class' => 'form-group-hide', 'id' => 'urusan', 'autocomplete' => 'off'])->label(false) ?>
                <?= $form->field($model, 'Kd_Bidang')->textInput(['maxlength' => true, 'class' => 'form-group-hide', 'id' => 'bidang', 'autocomplete' => 'off'])->label(false) ?>
                <?= $form->field($model, 'Kd_Unit')->textInput(['maxlength' => true, 'class' => 'form-group-hide', 'id' => 'unit', 'autocomplete' => 'off'])->label(false) ?>
                <?= $form->field($model, 'Kd_Sub')->textInput(['maxlength' => true, 'class' => 'form-group-hide', 'id' => 'sub', 'autocomplete' => 'off'])->label(false) ?>
                <!-- Unit -->
                <?php // $form->field($model, 'Kd_Sub')->dropDownList($dataunit, ['prompt'=>'Pilih Sub Unit', 'id'=>'Kd_Sub']) ?>
                

                

                <!-- Bidang -->

                <?= $form->field($model, 'Nm_Permasalahan')->textarea(['maxlength' => true]) ?>

                <?php
                        if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Utara'){
                            $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                          //$Nm_Kec2 = "Berhasil Kecamatan 1";
                    ?>
                                        
                                        <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                            'data'=>$Iwankamus1,
                                            'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus1'],
                                            'pluginOptions' => ['select2-allow-clear'=>true],

                                        ])->label('Kamus Usulan'); ?>
                    <?php
                      
                    }?>
                    <?php

                if($PC_Kelompok->kdKec->Nm_Kec == 'Rantau Selatan'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 2";
                ?>
                                
                                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus2,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus2'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php

                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Barat'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 3";
                ?>
                   
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus3,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus3'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hilir'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 4";
                ?>
                    
                    <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus4,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus4'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Bilah Hulu'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 5";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus5,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus5'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Pangkatan'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                 // $Nm_Kec2 = "Berhasil Kecamatan 6";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus6,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus6'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Tengah'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 7";
                ?>
               
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus7,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus7'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hilir'){
                    $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 8";
                ?>
               
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus8,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus8'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                  
                }
                ?>
                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Panai Hulu'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 9";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

                <?php
                if($PC_Kelompok->kdKec->Nm_Kec == 'Kecamatan Coba'){
                     $Nm_Kec=$PC_Kelompok->kdKec->Nm_Kec;
                  //$Nm_Kec2 = "Berhasil Kecamatan 10";
                ?>
                
                <?= $form->field($model, 'id_kamus_usulan')->widget(Select2::classname(),[
                                        'data'=>$Iwankamus9,
                                        'options'=> ['placeholder' => 'Pilih Kamus','id'=>'kdKamus9'],
                                        'pluginOptions' => ['select2-allow-clear'=>true],

                                    ])->label('Kamus Usulan'); ?>
                <?php
                 
                }
                ?>

               <?= $form->field($model, 'Jenis_Usulan')->textInput(['maxlength' => true, 'id' => 'coba', 'readonly' => true]) ?>

                <?= $form->field($model, 'Kd_Pem')->dropdownList(
                            $NASbidangpem, ['prompt' => 'Pilih Bidang', 'class' => 'form-control', 'id' => 'bidpem', 'readonly' => true]) 
                ?>

                <?= 
                    $form->field($model, 'Kd_Prioritas_Pembangunan_Daerah')->dropdownList(
                       $NASrpjmd,
                        ['prompt'=>'Pilih Prioritas', 'class' => 'form-control select2-allow-clear','id' => 'priopem', 'readonly' => true])->label("Prioritas Pembangunan Daerah"); ?>
                
               
                
                <?= $form->field($model, 'id_skpd')->dropdownList( $Iwanskpd,
                        ['prompt'=>'Pilih Perangkat Daerah', 'class' => 'form-control select2-allow-clear','id' => 'pdaerah', 'readonly' => true])->label("Perangkat Daerah"); ?>
                
                

                  <!--   <label class="control-label col-sm-3">Satuan </label>
                    <a href="#" id="asalmanual" role="button" class="btn btn-success">Input Manual</a>
                     <label class="control-label col-sm-2"></label>
                    <a href="#" id="asalsatuan" role="button" class="btn btn-info">Input dari Standard </a> -->
                    <?= $form->field($model, 'Jumlah')->textInput(['maxlength' => true, 'class' => 'form-control hitung', 'id' => 'jumlah', 'autocomplete' => 'off']) ?>
                
                


                   <?= $form->field($model, 'Kd_Satuan')->dropdownList(
                            $NASsatuan, ['prompt' => 'Pilih Satuan', 'class' => 'form-control', 'id' => 'satuan', 'readonly' => true]) 
                    ?>

                    <?= $form->field($model, 'Harga_Satuan')->textInput(['maxlength' => true, 'class' => 'form-control nomor hitung', 'id' => 'harga', 'autocomplete' => 'off', 'readonly' => true]) ?> 

                    <?= $form->field($model, 'Harga_Total')->textInput(['maxlength' => true, 'class' => 'form-control nomor', 'id' => 'total', 'readonly' => true]) ?>

                 <!--    <?= $form->field($model, 'Kd_Asal_Usulan')->dropDownList([ 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', ], ['prompt' => '']) ?> -->

                    <div class="form-group">
                    <label class="control-label col-sm-3"></label>
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>    
    </div>
</div>
</div>

<!-- modal musrenbang kelurahan -->
<!-- /.modal form -->
<?php
    $kamususulan1 = <<< JS
    $('#kdKamus1').change(function(){
         var idKamus1 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus1',{ idKamus1 : idKamus1 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=lingkungan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

            $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

             
         });
    });
JS;
$this->registerJs($kamususulan1);
?>


<?php
    $kamususulan2 = <<< JS
    $('#kdKamus2').change(function(){
         var idKamus2 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus2',{ idKamus2 : idKamus2 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');  
                $('#jumlah').attr('value',0);          
            });

            $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            
         });
    });
JS;
$this->registerJs($kamususulan2);
?>


<?php
    $kamususulan3 = <<< JS
    $('#kdKamus3').change(function(){
         var idKamus3 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus3',{ idKamus3 : idKamus3 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            
         });
    });
JS;
$this->registerJs($kamususulan3);
?>


<?php
    $kamususulan4 = <<< JS
    $('#kdKamus4').change(function(){
         var idKamus4 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus4',{ idKamus4 : idKamus4 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr(0);         
            });

            $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            
         });
    });
JS;
$this->registerJs($kamususulan4);
?>


<?php
    $kamususulan5 = <<< JS
    $('#kdKamus5').change(function(){
         var idKamus5 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus5',{ idKamus5 : idKamus5 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            
         });
    });
JS;
$this->registerJs($kamususulan5);
?>


<?php
    $kamususulan6 = <<< JS
    $('#kdKamus6').change(function(){
         var idKamus6 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus6',{ idKamus6 : idKamus6 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            
         });
    });
JS;
$this->registerJs($kamususulan6);
?>


<?php
    $kamususulan7 = <<< JS
    $('#kdKamus7').change(function(){
         var idKamus7 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus7',{ idKamus7 : idKamus7 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected'); 
                $('#jumlah').attr('value',0);           
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            

         });
    });
JS;
$this->registerJs($kamususulan7);
?>


<?php
    $kamususulan8 = <<< JS
    $('#kdKamus8').change(function(){
         var idKamus8 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus8',{ idKamus8 : idKamus8 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });
            
             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                $('#jumlah').attr('value',0);
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');   
                $('#jumlah').attr('value',0);         
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

            

         });
    });
JS;
$this->registerJs($kamususulan8);
?>


<?php
    $kamususulan9 = <<< JS
    $('#kdKamus9').change(function(){
         var idKamus9 = $(this).val();
         
         $.get('index.php?r=musrenbang-kecamatan/kamus9',{ idKamus9 : idKamus9 },function(data){
            var data = $.parseJSON(data);
           
            $('#coba').attr('value',data.nama_kamus_usulan);
            $('#harga').attr('value',data.harga_satuan);
            $('#jumlah').attr('value',0);
            var satuan1 = data.satuan;
            var bidpem1 = data.kd_bidang_pem;
            var priopem1 = data.kd_prioritas_pem;
            var skpd1 = data.id_skpd;
            
            $.get('index.php?r=musrenbang-kecamatan/pdaerah',{ skpd1 : skpd1 },function(data4){  
                var data4 = $.parseJSON(data4);
                $('#pdaerah option[value = '+data4.id+']').attr('selected', 'selected');  
            });

            $.get('index.php?r=musrenbang-kecamatan/urusan1',{ skpd1 : skpd1 },function(data5){  
                 var data5 = $.parseJSON(data5);
                $('#urusan').attr('value',data5);
                //alert(data5);

            });

            $.get('index.php?r=musrenbang-kecamatan/bidang1',{ skpd1 : skpd1 },function(data6){  
                 var data6 = $.parseJSON(data6);
                $('#bidang').attr('value',data6);
                //alert(data6);

            });

            $.get('index.php?r=musrenbang-kecamatan/unit1',{ skpd1 : skpd1 },function(data7){  
                 var data7 = $.parseJSON(data7);
                $('#unit').attr('value',data7);
                //alert(data7);

            });

            $.get('index.php?r=musrenbang-kecamatan/sub1',{ skpd1 : skpd1 },function(data8){  
                 var data8 = $.parseJSON(data8);
                $('#sub').attr('value',data8);
               // alert(data8);

            });

             $.get('index.php?r=musrenbang-kecamatan/satuan',{ satuan1 : satuan1 },function(data1){ 
                var data1 = $.parseJSON(data1);
                $('#satuan option[value = '+data1.Kd_Satuan+']').attr('selected', 'selected');
                
            });

            $.get('index.php?r=musrenbang-kecamatan/bidpem',{ bidpem1 : bidpem1 },function(data2){  
                var data2 = $.parseJSON(data2);
                $('#bidpem option[value = '+data2.Kd_Pem+']').attr('selected', 'selected');    
                  
            });

             $.get('index.php?r=musrenbang-kecamatan/priopem',{ priopem1 : priopem1 },function(data3){  
                var data3 = $.parseJSON(data3);
                $('#priopem option[value = '+data3.No_Prioritas+']').attr('selected', 'selected');  
            });

           

         });
    });
JS;
$this->registerJs($kamususulan9);
?>
