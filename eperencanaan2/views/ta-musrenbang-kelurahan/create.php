<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\TaMusrenbangKelurahan */



?>
<div class="ta-musrenbang-kelurahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'NASsatuan'=>$NASsatuan,
        'NASbidangpem'=>$NASbidangpem,
        'NASrpjmd'=>$NASrpjmd,
        //'Iwankamus' => $Iwankamus,
        'Iwankamus1' => $Iwankamus1,
                        'Iwankamus2' => $Iwankamus2,
                        'Iwankamus3' => $Iwankamus3,
                        'Iwankamus4' => $Iwankamus4,
                        'Iwankamus5' => $Iwankamus5,
                        'Iwankamus6' => $Iwankamus6,
                        'Iwankamus7' => $Iwankamus7,
                        'Iwankamus8' => $Iwankamus8,
                        'Iwankamus9' => $Iwankamus9

    ]) ?>

</div>
