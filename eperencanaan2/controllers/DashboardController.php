<?php

namespace eperencanaan\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use eperencanaan\models\TaKecamatanPaguIndikatif;
use eperencanaan\models\TaForumLingkungan;
use eperencanaan\models\TaKritikSaran;
use eperencanaan\models\search\TaAgendaPerencanaanKelurahanSearch;
use eperencanaan\models\search\TaAgendaPerencanaanLingkunganSearch;
use common\models\RefSubUnit;
use emusrenbang\models\TaMusrenbangSearch;
use eperencanaan\models\TaMusrenbang;
use common\models\RefKecamatan;
use common\models\RefKelurahan;
use common\models\RefLingkungan;
use common\models\RefDapil;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use common\models\RefBidangPembangunan;
use common\models\RefRPJMD;
use common\models\RefStandardSatuan;
use common\models\TaSubUnit;
use common\models\TaProgram;
use common\models\TaKegiatan;
use common\models\TaUserDapil;
use eperencanaan\models\TaForumLingkunganMedia;
use eperencanaan\models\TaMusrenbangKelurahan;
use eperencanaan\models\TaMusrenbangKelurahanMedia;
use eperencanaan\models\TaMusrenbangKecamatanMedia;
use eperencanaan\models\TaUsulanLingkunganMedia;
use common\models\RefJalan;
use eperencanaan\models\search\TaForumLingkunganSearch;
use common\models\TaIdentitas;
use common\models\TaPemda;
use common\models\RefProvinsi;
use common\models\RefKabupaten;
use eperencanaan\models\TaKelurahanVerifikasiUsulanLingkungan;
use kartik\mpdf\Pdf;

class DashboardController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function getKota($wil=null) {
        if ($wil) return [
                "Kab" => Yii::$app->pengaturan->Kolom('Nm_Pemda'),
                "Prov" => Yii::$app->pengaturan->nmProvinsi(),
            ];
        return Yii::$app->pengaturan->Kolom('Nm_Pemda');
    }

    public $layout = "main_dashboard";

    public function actionIndex() {
        $searchModel = new TaMusrenbangSearch();
        $dataProvider = $searchModel->searchLingkungan(Yii::$app->request->queryParams);

        $model = new LoginForm();
        $paguKec = TaKecamatanPaguIndikatif::find([['Kd_Prov' => '12', 'Kd_Kab' => '10']])
                ->sum('Pagu_Indikatif');

        $totalPaguLing = TaForumLingkungan::find(['Kd_Prov' => '12', 'Kd_Kab' => '10'])
                ->sum('Harga_Total');

        $usulanLing = TaForumLingkungan::find()
                    ->count();

        $usulan_kel1 = TaMusrenbangKelurahan::find()
                    ->leftJoin('Ta_Relasi_Musrenbang_Kelurahan', 'Ta_Relasi_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan = Ta_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan')
                    ->andwhere(['IS', 'Ta_Relasi_Musrenbang_Kelurahan.Kd_Ta_Musrenbang_Kelurahan', NULL])
                    ->count();
            

        $usulan_kel2 = TaKelurahanVerifikasiUsulanLingkungan::find()
                    ->count();

        $usulanKel = $usulan_kel1 + $usulan_kel2;

        $usulanKec = TaMusrenbang::find()
                    ->where(['Kd_Prov' => 12])
                    ->andwhere(['Kd_Kab' => 10])
                    ->andWhere(['or',
                        ['Kd_Asal_Usulan' => '1'],
                        ['Kd_Asal_Usulan' => '2'],
                        ['Kd_Asal_Usulan' => '3']
                    ])
                    ->andWhere(['or',
                        ['Status_Penerimaan_Kelurahan' => '1'],
                        ['Status_Penerimaan_Kelurahan' => '2']
                    ])
                    ->andWhere(['or',
                        ['Status_Penerimaan_Kecamatan' => '1'],
                        ['Status_Penerimaan_Kecamatan' => '2']
                    ])
                    ->count();

        $jumlahKegiatan = TaKegiatan::find()->count();

        $dataKec = RefKecamatan::find()
                ->where(['Kd_Prov' => '12', 'Kd_Kab' => '10'])
                ->all();

        $dataIdentitas = TaPemda::find()
                        ->where(['Tahun'=>'2017'])
                        ->one();        

        if (!isset($dataIdentitas)) {
            return $this->redirect(['dashboard/identitas']);
        } 
        else {

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $DNJadwal = Yii::$app->levelcomponent->getJadwal();
                $PC_Level_data = Yii::$app->levelcomponent->getLevel();
                if ($PC_Level_data != '') {
                    $PC_Level = $PC_Level_data->Kd_Level;
                } else {
                    $PC_Level = 3;
                }

                if ($PC_Level == 1) {
                    return $this->redirect(['laporan/index']);
                }
                $ZULuser = Yii::$app->levelcomponent->getKelompok();
                if ($ZULuser['Kd_Kel'] == 0) {
                    return $this->redirect(['ta-musrenbang-kecamatan/index']);
                } else if ($ZULuser['Kd_Lingkungan'] == 0) {
                    return $this->redirect(['ta-musrenbang-kelurahan/index']);
                } else {
                    return $this->redirect(['lingkungan/index']);
                }
            } 
            else {
                if (!Yii::$app->user->isGuest) {
                    //tambahan
                    $ZULuser = Yii::$app->levelcomponent->getKelompok();
                    $DNJadwal = Yii::$app->levelcomponent->getJadwal();
                    if ($ZULuser['Kd_Lingkungan'] == 0) {
                        return $this->redirect(['ta-musrenbang-kelurahan/index']);
                    } else {

                        return $this->redirect(['lingkungan/index']);
                    }
                } 
                else {
                    return $this->render('index', [
                                'totalPaguLing' => $totalPaguLing,
                                'paguKec' => $paguKec,
                                'usulanLing' => $usulanLing,
                                'usulanKel' => $usulan_kel2,
                                'usulanKec' => $usulanKec,
                                'jumlahKegiatan' => $jumlahKegiatan,
                                'model' => $model,
                                'dataKec' => $dataKec,
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                'dataIdentitas' => $dataIdentitas,
                    ]);
                }
            }
        } 
        
    }

    public function actionLoginLingkungan() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['lingkungan/index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLoginMusrenbangKelurahan() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['ta-musrenbang-kelurahan/index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLoginMusrenbangKecamatan() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['ta-musrenbang-kecamatan/index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLoginSkpd() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['ta-sub-unit/index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLoginPokir() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['pokir/index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }


    public function actionLoginRkpd() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->redirect(['emusrenbang/site/index']);
   
            $url = $_POST['/site/index']; // www.example.com/get_response/
            $arr = array('response' => 'Failed', 'msg' => 'login success', 'tokenKey' => 'token');

            $this->redirect('http://' . $url . '?' . http_build_query($arr));
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }


    public function actionAgendakel() {

        $searchModel = new TaAgendaPerencanaanKelurahanSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('agendakel', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAgendawarga() {

        $searchModel = new TaAgendaPerencanaanLingkunganSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('agendawarga', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSaran() {

        $model = new TaKritikSaran();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Tambah Usulan Lingkungan Berhasil');
            return $this->redirect(['dashboard/saran']);
        } else {
            return $this->render('saran', [
                        'model' => $model,
            ]);
        }
    }

    public function actionPanduanRembukWarga() {
        return $this->render('panduan-rembuk-warga');
    }

    public function actionPanduanMusrenbangKelurahan() {
        return $this->render('panduan-musrenbang-kelurahan');
    }

    public function actionPanduanMusrenbangKecamatan() {
        return $this->render('panduan-musrenbang-kecamatan');
    }

    public function actionSopRembukWarga() {
        return $this->render('sop-rembuk-warga');
    }

    public function actionSopMusrenbangKelurahan() {
        return $this->render('sop-musrenbang-kelurahan');
    }

    public function actionSopMusrenbangKecamatan() {
        return $this->render('sop-musrenbang-kecamatan');
    }

    public function actionBerita() {
        $ZULmodel = new \yii\base\DynamicModel(['kecamatan',
            'kelurahan'
        ]);
        $ZULmodel->addRule(['kecamatan', 'kelurahan'], 'required');
        if ($ZULmodel->hasErrors()) {
            // validation fails
        } else {
            // validation succeeds
        }

        $dataKec = ArrayHelper::map(RefKecamatan::find()->where(['Kd_Prov' => '12', 'Kd_Kab' => '10'])->all(), 'Kd_Kec', 'Nm_Kec');
        $ZULsearchModel = new \common\models\search\RefLingkunganSearch();
        if ($ZULmodel->load(Yii::$app->request->post())) {
            $ZULdataprovider = $ZULsearchModel->Zulsearch(Yii::$app->request->queryParams, $ZULmodel->kecamatan, $ZULmodel->kelurahan);
        } else {
            $ZULdataprovider = $ZULsearchModel->Zulsearch(Yii::$app->request->queryParams, 0, 0);
        }
        return $this->render('berita', ['ZULmodel' => $ZULmodel, 'dataProvider' => $ZULdataprovider, 'searchModel' => $ZULsearchModel, 'dataKec' => $dataKec]);
    }

    public function actionHasilrapat() {

        return $this->render('hasilrapat');
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionLaporan() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['laporan/index']);
        } else {
            return $this->render('login_bappeda', [
                        'model' => $model
            ]);
        }
    }

    public function actionStatistik() {
        $model = array();
        return $this->render('statistik', [
                    'model' => $model
        ]);
    }

    public function actionLihatUsulan() {
        $ZULnmkel = '';
        $ZULmodel = new \yii\base\DynamicModel(['kelurahan']);
        $ZULmodel->addRule(['kelurahan'], 'required');
        if ($ZULmodel->load(\Yii::$app->request->get())) {
            //print_r($ZULmodel);exit;
            $ZULtemp = \common\models\search\RefKelurahan::findOne(['Kd_Prov' => 12, 'Kd_Kab' => 10, 'Nm_Kel' => $ZULmodel->kelurahan]);
            //print_r($ZULtemp);exit;
            $ZULusulan = TaForumLingkungan::find()->where([
                'Kd_Prov' => $ZULtemp->Kd_Prov,
                'Kd_Kab' => $ZULtemp->Kd_Kab,
                'Kd_Kec' => $ZULtemp->Kd_Kec,
                'Kd_Urut_Kel' => $ZULtemp->Kd_Urut,
            ]);
            $ZULnmkel = $ZULmodel->kelurahan;
        } else {
            $ZULusulan = TaForumLingkungan::find();
        }
        $ZULcount = clone $ZULusulan;
        $pages = new \yii\data\Pagination(['totalCount' => $ZULcount->count()]);
        $pages->pageSize = 5;
        $models = $ZULusulan->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        return $this->render('lihat_usulan', [
                    'models' => $models,
                    'pages' => $pages,
                    'kelurahan' => ArrayHelper::getColumn(\common\models\RefKelurahan::find()
                                    ->where(['Kd_Prov' => 12, 'Kd_Kab' => 10])->all(), 'Nm_Kel'),
                    'model' => $ZULmodel,
                    'nm_kel' => $ZULnmkel
        ]);
    }

    public function actionMusrenbang()
    {
        $RefSubUnit = RefSubUnit::find()->all();

        return $this->render('musrenbang', [
          'RefSubUnit' => $RefSubUnit,
        ]);
    }

    public function actionLihatUsulanSpesifik($id) {

        $usulan = TaForumLingkungan::find()->where(['SHA2(Kd_Ta_Forum_Lingkungan, 256)' => $id])->one();
        $foto = \eperencanaan\models\TaUsulanLingkunganMedia::find()->where(['SHA2(Kd_Ta_Forum_Lingkungan, 256)' => $id])->all();
        return $this->render('lihat_usulan_spesifik', ['usulan' => $usulan, 'foto' => $foto]);
    }

    public function actionGetKelurahan($Kd_Kec)
    {
        $Kd_Prov = 12;
        $Kd_Kab = 10;

        $kelurahan = RefKelurahan::find()
                    ->where(['Kd_Prov' => $Kd_Prov])
                    ->andWhere(['Kd_Kab' => $Kd_Kab])
                    ->andwhere(['=', 'Kd_Kec', $Kd_Kec])
                    ->all();
        
        echo '<option value="0">- Pilih Kelurahan -</option>';
        foreach ($kelurahan as $key => $value) {
            echo '<option value="'.$value->Kd_Urut.'">'.$value->Nm_Kel.'</option>';
        }
    }

    public function actionGetLingkungan($Kd_Kec, $Kd_Kel)
    {
        $Kd_Prov = 12;
        $Kd_Kab = 10;

        $lingkungan = RefLingkungan::find()
                    ->where(['Kd_Prov' => $Kd_Prov])
                    ->andWhere(['Kd_Kab' => $Kd_Kab])
                    ->andwhere(['=', 'Kd_Kec', $Kd_Kec])
                    ->andwhere(['=', 'Kd_Urut_Kel', $Kd_Kel])
                    ->all();
        
        echo '<option value="0">- Pilih Lingkungan -</option>';
        foreach ($lingkungan as $key => $value) {
            echo '<option style="text-transform: capitalize;" value="'.$value->Kd_Lingkungan.'">'.$value->Nm_Lingkungan.'</option>';
        }
    }

    public function actionGetKelurahan2($kd_kec)
    {
        $countKel = RefKelurahan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $kd_kec])
                ->count();
 
        $Kels = RefKelurahan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $kd_kec])
                ->all();
 
        if($countKel > 0){
            foreach($Kels as $Kel){
                echo "<option value='".$Kel->Kd_Urut."'>".$Kel->Nm_Kel."</option>";
            }
        }
        else{
            echo "<option>-Pilih Kelurahan-</option>";
        }
 
    }

    public function actionGetLingkungan2($kd_kec, $kd_kel)
    {
        $countLing = RefLingkungan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $kd_kec])
                ->andwhere(['Kd_Urut_Kel' => $kd_kel])
                ->count();
 
        $Lings = RefLingkungan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $kd_kec])
                ->andwhere(['Kd_Urut_Kel' => $kd_kel])
                ->all();
 
        if($countLing > 0){
            foreach($Lings as $Ling){
                echo "<option value='".$Ling->Kd_Lingkungan."'>".$Ling->Nm_Lingkungan."</option>";
            }
        }
        else{
            echo "<option>-Pilih Lingkungan-</option>";
        }
 
    }

    public function actionLihatFile($nama_file)
    { 
        return $this->renderpartial('lihat_file', [
                'nama_file' => $nama_file,
        ]);
    }

    public function actionUsulanLingkungan()
    {    
        $searchModel = new TaForumLingkunganSearch();
        $dataProvider = $searchModel->searchLihatUsulan(Yii::$app->request->queryParams);

        $data_kec = ArrayHelper::map(
                        RefKecamatan::find()
                        ->where(['Kd_Prov' => 12])
                        ->andwhere(['Kd_Kab' => 10])
                        ->orderBy(['Nm_Kec' => SORT_ASC])
                        ->asArray()
                        ->all(),
                        'Kd_Kec',
                        'Nm_Kec'
                    );

        $ref_jalan = ArrayHelper::map(RefJalan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $searchModel->Kd_Kec])
                ->andwhere(['Kd_Urut_Kel' => $searchModel->Kd_Urut_Kel])
                ->andwhere(['Kd_Lingkungan' => $searchModel->Kd_Lingkungan])
                ->asArray()
                ->all(), 
                'Kd_Jalan', 
                'Nm_Jalan'
        );

        return $this->render('usulan-lingkungan', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
        ]);
    }

    public function actionUsulanKelurahan() 
    {
        $searchModel    = new TaMusrenbangSearch();
        $dataProvider   = $searchModel->searchUsulanKelurahan(Yii::$app->request->queryParams);

        $data_kec = ArrayHelper::map(
                        RefKecamatan::find()
                        ->where(['Kd_Prov' => 12])
                        ->andwhere(['Kd_Kab' => 10])
                        ->orderBy(['Nm_Kec' => SORT_ASC])
                        ->asArray()
                        ->all(),
                        'Kd_Kec',
                        'Nm_Kec'
                    );

        $ref_jalan = ArrayHelper::map(RefJalan::find()
                    ->where(['Kd_Prov' => 12])
                    ->andwhere(['Kd_Kab' => 10])
                    ->andwhere(['Kd_Kec' => $searchModel->Kd_Kec])
                    ->andwhere(['Kd_Urut_Kel' => $searchModel->Kd_Urut_Kel])
                    ->andwhere(['Kd_Lingkungan' => $searchModel->Kd_Lingkungan])
                    ->asArray()
                    ->all(), 
                    'Kd_Jalan', 
                    'Nm_Jalan'
        );

        return $this->render('usulan-kelurahan', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'data_kec' => $data_kec,
                'ref_jalan' => $ref_jalan,
        ]);
    }

    public function actionUsulanKecamatan() {

        $searchModel = new TaMusrenbangSearch();
        $dataProvider = $searchModel->searchUsulanKecamatan(Yii::$app->request->queryParams);

        $data_kec = ArrayHelper::map(
                        RefKecamatan::find()
                        ->where(['Kd_Prov' => 12])
                        ->andwhere(['Kd_Kab' => 10])
                        ->orderBy(['Nm_Kec' => SORT_ASC])
                        ->asArray()
                        ->asArray()
                        ->all(),
                        'Kd_Kec',
                        'Nm_Kec'
                    );

        $ref_jalan = ArrayHelper::map(RefJalan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $searchModel->Kd_Kec])
                ->andwhere(['Kd_Urut_Kel' => $searchModel->Kd_Urut_Kel])
                ->andwhere(['Kd_Lingkungan' => $searchModel->Kd_Lingkungan])
                ->asArray()
                ->all(), 
                'Kd_Jalan', 
                'Nm_Jalan'
        );

        return $this->render('usulan-kecamatan', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'data_kec' => $data_kec,
                'ref_jalan' => $ref_jalan,
        ]);

    }

    public function actionUsulanSemua() {

        $searchModelTerima = new TaMusrenbangSearch();
        $dataProviderTerima = $searchModelTerima->searchUsulanSemuaTerima(Yii::$app->request->queryParams);

        $searchModelTolak = new TaForumLingkunganSearch();
        $dataProviderTolak = $searchModelTolak->searchUsulanSemuaTolak(Yii::$app->request->queryParams);

        $data_kec = ArrayHelper::map(
                        RefKecamatan::find()
                        ->where(['Kd_Prov' => 12])
                        ->andwhere(['Kd_Kab' => 10])
                        ->orderBy(['Nm_Kec' => SORT_ASC])
                        ->asArray()
                        ->all(),
                        'Kd_Kec',
                        'Nm_Kec'
                    );

        $ref_jalan = ArrayHelper::map(RefJalan::find()
                ->where(['Kd_Prov' => 12])
                ->andwhere(['Kd_Kab' => 10])
                ->andwhere(['Kd_Kec' => $searchModelTerima->Kd_Kec])
                ->andwhere(['Kd_Urut_Kel' => $searchModelTerima->Kd_Urut_Kel])
                ->andwhere(['Kd_Lingkungan' => $searchModelTerima->Kd_Lingkungan])
                ->asArray()
                ->all(), 
                'Kd_Jalan', 
                'Nm_Jalan'
        );

        $model = TaMusrenbang::find()
                ->where(['or',
                    ['Kd_Asal_Usulan' => '1'],
                    ['Kd_Asal_Usulan' => '2'],
                    ['Kd_Asal_Usulan' => '3']
                ])
                ->andWhere(['or',
                    ['Status_Penerimaan_Kelurahan' => '1'],
                    ['Status_Penerimaan_Kelurahan' => '2']
                ])
                ->andWhere(['or',
                    ['Status_Penerimaan_Kecamatan' => '1'],
                    ['Status_Penerimaan_Kecamatan' => '2']
                ])
                ->all();

        return $this->render('usulan-semua', [
                'dataProviderTerima' => $dataProviderTerima,
                'dataProviderTolak' => $dataProviderTolak,
                'searchModelTerima' => $searchModelTerima,
                'searchModelTolak' => $searchModelTolak,
                'data_kec' => $data_kec,
                'ref_jalan' => $ref_jalan,
                // 'model' => $model,
        ]);

    }

    public function actionGetUsulanKelurahan()
    {
        $request = Yii::$app->request;

        $Kd_Prov    = 12;
        $Kd_Kab     = 10;
        $Kd_Kec     = $request->post('Kd_Kec');
        $Kd_Kel     = $request->post('Kd_Kel');        

        $data = TaMusrenbang::find()
                ->where(['Kd_Prov' => $Kd_Prov])
                ->andwhere(['Kd_Kab' => $Kd_Kab])
                ->andwhere(['Kd_Kec' => $Kd_Kec])
                ->andwhere(['Kd_Urut_Kel' => $Kd_Kel])
                ->andwhere(['or',
                    ['Kd_Asal_Usulan' => '1'],
                    ['Kd_Asal_Usulan' => '2']
                ])
                ->andwhere(['or',
                    ['Status_Penerimaan_Kelurahan' => '1'],
                    ['Status_Penerimaan_Kelurahan' => '2']
                ])
                ->all();

        return $this->renderpartial('get_usulan_kelurahan', [
                'data' => $data,
        ]);
    }

    public function actionGetUsulanKecamatan()
    {
        $request = Yii::$app->request;

        $Kd_Prov    = 12;
        $Kd_Kab     = 10;
        $Kd_Kec     = $request->post('Kd_Kec');

        $dokumen = TaMusrenbangKecamatanMedia::find()
                ->where(['Kd_Prov' => $Kd_Prov])
                ->andWhere(['Kd_Kab' => $Kd_Kab])
                ->andWhere(['=', 'Kd_Kec', $Kd_Kec])
                ->all();        

        $data = TaMusrenbang::find()
                ->where(['Kd_Prov' => $Kd_Prov])
                ->andWhere(['Kd_Kab' => $Kd_Kab])
                ->andWhere(['Kd_Kec' => $Kd_Kec])
                ->andWhere(['or',
                    ['Kd_Asal_Usulan' => '1'],
                    ['Kd_Asal_Usulan' => '2'],
                    ['Kd_Asal_Usulan' => '3']
                ])
                ->andWhere(['or',
                    ['Status_Penerimaan_Kelurahan' => '1'],
                    ['Status_Penerimaan_Kelurahan' => '2']
                ])
                ->andWhere(['or',
                    ['Status_Penerimaan_Kecamatan' => '1'],
                    ['Status_Penerimaan_Kecamatan' => '2']
                ])
                ->all();

        return $this->renderpartial('get_usulan_kecamatan', [
                'data' => $data,
                'dokumen' => $dokumen,
        ]);
    }

    public function actionGetUsulanKecamatan2($Kd_Kec)
    {
        $Kd_Prov        = 12;
        $Kd_Kab         = 10;

        $dataProvider = new ActiveDataProvider([
                        'query' => TaMusrenbang::find()
                            ->where(['Kd_Prov' => 12])
                            ->andWhere(['Kd_Kab' => 10])
                            ->andWhere(['Kd_Kec' => $Kd_Kec])
                            ->andWhere(['or',
                                ['Kd_Asal_Usulan' => 1],
                                ['Kd_Asal_Usulan' => 2],
                                ['Kd_Asal_Usulan' => 3]
                            ])
                            ->andWhere(['or',
                                ['Status_Penerimaan_Kelurahan' => '1'],
                                ['Status_Penerimaan_Kelurahan' => '2']
                            ])
                            ->andWhere(['or',
                                ['Status_Penerimaan_Kecamatan' => '1'],
                                ['Status_Penerimaan_Kecamatan' => '2']
                            ]),
                        'pagination' => false,
        ]);

        return $this->renderAjax('get_usulan_kecamatan2', [
                'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUsulanPokir()
    {
        $searchModel = new TaMusrenbangSearch();
        $dataProvider = $searchModel->searchPokir(Yii::$app->request->queryParams);
        $data_bidpem = ArrayHelper::map(
                              RefBidangPembangunan::find()
                                ->all(),
                              'Kd_Pem',
                              'Bidang_Pembangunan'
                            );

        $data_rpjmd = ArrayHelper::map(
                              RefRPJMD::find()
                                ->all(),
                              'Kd_Prioritas_Pembangunan_Kota',
                              'Nm_Prioritas_Pembangunan_Kota'
                            );

        $data_skpd = ArrayHelper::map(
                              RefSubUnit::find()
                                ->all(),
                              'Kd_Sub',
                              'Nm_Sub_Unit'
                            );


        // $data_satuan = ArrayHelper::map(RefStandardSatuan::find()->orderBy('Uraian')->all(), 'Kd_Satuan', 'Uraian');
        $data_dapil = ArrayHelper::map(RefDapil::find()->orderBy('Nm_Dapil')->all(),'Kd_Dapil','Nm_Dapil');

        return $this->render('usulan-pokir', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'data_bidpem' => $data_bidpem,
                    'data_rpjmd' => $data_rpjmd,
                    // 'data_satuan' => $data_satuan,
                    'data_dapil' => $data_dapil,
        ]);
    }

    public function actionGetUsulanPokir()
    {
        $post = $request = Yii::$app->request->post();
        $searchModel = new TaMusrenbangSearch();
        $dataProvider = $searchModel->searchPokir(Yii::$app->request->queryParams);

        return $this->render('get_usulan_pokir', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetUserDapil($Kd_Dapil)
    {
        $user_dapil = TaUserDapil::find()
                    ->where(['Kd_Dapil' => $Kd_Dapil])
                    ->all();
        
        echo '<option value="">- Pilih User Daerah Pemilihan -</option>';
        foreach ($user_dapil as $key => $value) {
            echo '<option style="text-transform: capitalize;" value="'.$value->Kd_User.'">'.$value->Nm_User_Dapil.'</option>';
        }
    }

    public function actionDokumenLingkungan()
    {
        $data = TaUsulanLingkunganMedia::find()
                        ->where(['Kd_Ta_Forum_Lingkungan' => 286])
                        ->all();

        return $this->render('dokumen-lingkungan', [
                'data' => $data,
        ]);
    }

    public function actionLaporanRenja() {
        $subunit = RefSubUnit::find()
                   ->where(['not', ['Nm_Sub_Unit' => '']])
                    ->all();
        return $this->render('laporan-renja', ['subunit' => $subunit]);
    } 

    public function actionGetLaporanRenja() {
        $post = explode('.', $request = Yii::$app->request->post('Kd_Sub'));
        if (Yii::$app->user->isGuest) {
            $this->layout = 'main-front';
        }
        $tahun = (date('Y') + 1);

        $TaSubUnit = TaSubUnit::find()->where(['Kd_Urusan' => $post[0], 'Kd_Bidang' => $post[1], 'Kd_Unit' => $post[2], 'Kd_Sub' => $post[3]])->one();


        $dataKegiatan = TaProgram::find()->where(['Tahun' => $TaSubUnit->Tahun, 'Kd_Urusan' => $TaSubUnit->Kd_Urusan, 'Kd_Bidang' => $TaSubUnit->Kd_Bidang, 'Kd_Unit' => $TaSubUnit->Kd_Unit, 'Kd_Sub' => $TaSubUnit->Kd_Sub])->all();

        $dataKeteranganKeg = TaKegiatan::find()->where(['Tahun' => $TaSubUnit->Tahun, 'Kd_Urusan' => $TaSubUnit->Kd_Urusan, 'Kd_Bidang' => $TaSubUnit->Kd_Bidang, 'Kd_Unit' => $TaSubUnit->Kd_Unit, 'Kd_Sub' => $TaSubUnit->Kd_Sub])->all();



        return $this->renderPartial('get_laporan_renja', [
                    'tahun' => $tahun,
                    'subunit' => $TaSubUnit,
                    'dataKegiatan' => $dataKegiatan,
                    'dataKeteranganKeg' => $dataKeteranganKeg
        ]);
    }

    public function actionCetakLaporanRenja($urusan, $bidang, $unit, $sub) {

        $tahun = (date('Y') + 1);
        $kelompok = $this->getKota(true);


        $TaSubUnit = TaSubUnit::find()->where(['Kd_Urusan' => $urusan, 'Kd_Bidang' => $bidang, 'Kd_Unit' => $unit, 'Kd_Sub' => $sub])->one();

        $dataKegiatan = TaProgram::find()->where(['Tahun' => $TaSubUnit->Tahun, 'Kd_Urusan' => $TaSubUnit->Kd_Urusan, 'Kd_Bidang' => $TaSubUnit->Kd_Bidang, 'Kd_Unit' => $TaSubUnit->Kd_Unit, 'Kd_Sub' => $TaSubUnit->Kd_Sub])->all();

        $dataKeteranganKeg = TaKegiatan::find()->where(['Tahun' => $TaSubUnit->Tahun, 'Kd_Urusan' => $TaSubUnit->Kd_Urusan, 'Kd_Bidang' => $TaSubUnit->Kd_Bidang, 'Kd_Unit' => $TaSubUnit->Kd_Unit, 'Kd_Sub' => $TaSubUnit->Kd_Sub])->all();



        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_FOLIO,
            'content' => $this->renderPartial('cetak_laporan_renja', [
                'tahun' => $tahun,
                'kelompok' => $kelompok,
                'subunit' => $TaSubUnit,
                'dataKegiatan' => $dataKegiatan,
                'dataKeteranganKeg' => $dataKeteranganKeg
                    // 'dataUrusBidang'=>$dataUrusBidang
            ]),
            'options' => [
                'title' => 'Laporan Renja',
            //'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'methods' => [
                'SetHeader' => ['Dicetak dari: Sistem e-Perencanaan '.$this->getKota().'||Dicetak tanggal: ' .
                    Yii::$app->zultanggal->ZULgethari(date('N')) . ', ' . (date('j')) . ' ' .
                    Yii::$app->zultanggal->ZULgetbulan(date('n')) . ' ' . (date('Y')) . '/' .
                    (date('H:i:s'))
                ],
                'SetFooter' => ['SKPD : '.$TaSubUnit->namaSub->Nm_Sub_Unit.'|Halaman {PAGENO}|Tvic10'],
            ]
        ]);
        return $pdf->render();
    }

    public function actionIdentitas(){

      
        $model = new TaPemda();
        
        // $modelProvinsi = RefProvinsi::find()->all();
        
        $modelProvinsi = ArrayHelper::map(RefProvinsi::find()
                          ->all(), 'Kd_Prov', 'Nm_Prov');
   
        $Tahun = 2017;
      

        if($model->load(Yii::$app->request->post())){

        $prov = $model->Kd_Prov;
        $kab = $model->Kd_Kab;
        $hostname = Yii::$app->levelcomponent->getServerName();
        $ippublic = Yii::$app->levelcomponent->getUserHost();

        $modelProvinsi= RefKabupaten::find()->where(['Kd_Prov' => $prov, 'Kd_Kab' => $kab])->one();

        $kabupaten = $modelProvinsi->Nm_Kab;

        $model->Tahun = $Tahun;
        $model->Nm_Pemda = $modelProvinsi;
        $model->Created_At = time();
        $model->Status = 0;
        $model->Nm_Pemda = $kabupaten;
        $model->Hostname = $hostname;
        $model->Ip_Public = $ippublic;      

        // $kabupaten =[];
        // foreach ($modelProvinsi as $value) {
                // $kabupaten[$value['Nm_Kab']]=$value['Nm_Kab'];
        // }

        // print_r($kabupaten); exit();   
        if ($model->save(false)) {
          return $this->redirect(['index']);
         }
        } else {

        return $this->render('form_identitas', [
        'model'=>$model,
        'modelProvinsi'=>$modelProvinsi,
        ]);
        }
    }

      public function actionKirimidentitas($Kd_Prov, $Kd_Kab, $Email,$Token
        ){

      
        $model = new TaPemda();
        
        // $modelProvinsi = RefProvinsi::find()->all();
        
        $modelProvinsi = ArrayHelper::map(RefProvinsi::find()
                          ->all(), 'Kd_Prov', 'Nm_Prov');
   
        $Tahun = 2017;
      

        if($model->load(Yii::$app->request->post())){

        $prov = $model->Kd_Prov;
        $kab = $model->Kd_Kab;
        $hostname = Yii::$app->levelcomponent->getServerName();
        $ippublic = Yii::$app->levelcomponent->getUserHost();

        $modelProvinsi= RefKabupaten::find()->where(['Kd_Prov' => $prov, 'Kd_Kab' => $kab])->one();

        $kabupaten = $modelProvinsi->Nm_Kab;

        $model->Tahun = $Tahun;
        $model->Nm_Pemda = $modelProvinsi;
        $model->Created_At = time();
        $model->Status = 0;
        $model->Nm_Pemda = $kabupaten;
        $model->Hostname = $hostname;
        $model->Ip_Public = $ippublic;      

        // $kabupaten =[];
        // foreach ($modelProvinsi as $value) {
                // $kabupaten[$value['Nm_Kab']]=$value['Nm_Kab'];
        // }

        // print_r($kabupaten); exit();   
        if ($model->save(false)) {
          return $this->redirect(['index']);
         }
        } else {

        return $this->render('form_identitas', [
        'model'=>$model,
        'modelProvinsi'=>$modelProvinsi,
        ]);
        }
    }

}