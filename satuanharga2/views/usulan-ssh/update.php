<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UsulanSsh */

$this->title = 'Update Usulan Ssh: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usulan Sshes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usulan-ssh-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
