<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RefSsh1;
use common\models\RefSsh2;

$this->registerJs(
    "$('#ssh1').change(function(){
        var ssh1 = $(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getssh2')."&Kd_Ssh1='+ssh1,function(data){
            $('#ssh2').html(data);
        });
    });"
);

$this->registerJs(
    "$('#ssh2').change(function(){
        var ssh1 = $('#ssh1').val();
        var ssh2 = $(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getssh3')."&Kd_Ssh1='+ssh1+'&Kd_Ssh2='+ssh2,function(data){
            $('#ssh3').html(data);
        });
    });"
);

$this->registerJs(
    "$('#ssh3').change(function(){
        var ssh1 = $('#ssh1').val();
        var ssh2 = $('#ssh2').val();
        var ssh3 = $(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getssh4')."&Kd_Ssh1='+ssh1+'&Kd_Ssh2='+ssh2+'&Kd_Ssh3='+ssh3,function(data){
            $('#ssh4').html(data);
        });
    });"
);

$this->registerJs(
    "$('#ssh4').change(function(){
        var ssh1 = $('#ssh1').val();
        var ssh2 = $('#ssh2').val();
        var ssh3 = $('#ssh3').val();
        var ssh4 = $(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getssh5')."&Kd_Ssh1='+ssh1+'&Kd_Ssh2='+ssh2+'&Kd_Ssh3='+ssh3+'&Kd_Ssh4='+ssh4,function(data){
            $('#ssh5').html(data);
        });
    });"
);


/* @var $this yii\web\View */
/* @var $model common\models\UsulanSsh */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usulan-ssh-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-option">
        <?= $form->field($model, 'Kd_Ssh1')->dropdownlist($ssh1,
        [
            'prompt' => 'Pilih Ssh1',
            'class' => 'dependent-input form-control',
            'id' => 'ssh1'
        ]); ?>
    </div>

    <div class="form-option">
        <?= $form->field($model, 'Kd_Ssh2')->dropdownlist([],
        [
            'prompt' => 'Pilih Ssh2',
            'class' => 'dependent-input form-control',
            'id' => 'ssh2'
        ]); ?>
    </div>

    <div class="form-option">
        <?= $form->field($model, 'Kd_Ssh3')->dropdownlist([],
        [
            'prompt' => 'Pilih Ssh3',
            'class' => 'dependent-input form-control',
            'id' => 'ssh3'
        ]); ?>
    </div>

    <div class="form-option">
        <?= $form->field($model, 'Kd_Ssh4')->dropdownlist([],
        [
            'prompt' => 'Pilih Ssh4',
            'class' => 'dependent-input form-control',
            'id' => 'ssh4'
        ]); ?>
    </div>

    <div class="form-option">
        <?= $form->field($model, 'Kd_Ssh5')->dropdownlist([],
        [
            'prompt' => 'Pilih Ssh5',
            'class' => 'dependent-input form-control',
            'id' => 'ssh5'
        ]); ?>
    </div>

    <?= $form->field($model, 'Kd_Ssh6')->textInput() ?>

    <?= $form->field($model, 'Nama_Barang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Spesifikasi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Kd_Satuan')->dropdownlist($dataSatuan, ['prompt'=>'Pilih Satuan'])->label('Satuan') ?>

    <?php // $form->field($model, 'Satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Harga_Satuan')->textInput() ?>

    <?php // $form->field($model, 'Kd_User')->textInput() ?>

    <?php // $form->field($model, 'Status')->dropDownList([ '0', '1', '2', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
