<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
 use common\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UsulanSshSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usulan Ssh';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-danger">
    <div class="box-body">        
        <div class="usulan-ssh-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?php 
                    if (Helper::checkRoute('create')) {
                        echo Html::a('Tambah Usulan Ssh', ['create'], ['class' => 'btn btn-success']);
                    }
                ?>
                <!-- <?= Html::a('Tambah Usulan Ssh', ['create'], ['class' => 'btn btn-success']) ?> -->
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    // 'Kd_Ssh1',
                    // 'Kd_Ssh2',
                    // 'Kd_Ssh3',
                    // 'Kd_Ssh4',
                    // 'Kd_Ssh5',
                    // 'Kd_Ssh6',
                    [
                        'attribute'=>'Kd_Ssh6',
                        'value' => function ($model) {return $model->kdSsh5->Kd_Ssh1.'.'.$model->Kd_Ssh2.'.'.$model->Kd_Ssh3.'.'.$model->Kd_Ssh4.'.'.$model->Kd_Ssh5.'.'.$model->Kd_Ssh6;},
                        'label' => 'Kode SSH'
                    ],
                    
                    'Nama_Barang',
                    'Spesifikasi:ntext',
                    [
                        'value' => 'kdSatuan.Uraian',
                        'label' => 'Satuan'
                    ],
                    'Harga_Satuan',
                    // 'Kd_User',

                    'Status',

                    // ['class' => 'yii\grid\ActionColumn'],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => Helper::filterActionColumn('{view}{update}{delete}'),
                        // 'options' => ['class'=>'col-md-1']
                    ],

                    [
                        'format' => 'raw',
                        // 'label' => 'Aksi',
                        'value' => function($model) {
                            $url1 = Url::to(['usulan-ssh/terima-usulan',
                                          'id' => $model->id,
                                          'Kd_Ssh1' => $model->Kd_Ssh1,
                                          'Kd_Ssh2' => $model->Kd_Ssh2,
                                          'Kd_Ssh3' => $model->Kd_Ssh3,
                                          'Kd_Ssh4' => $model->Kd_Ssh4,
                                          'Kd_Ssh5' => $model->Kd_Ssh5,
                                          'Kd_Ssh6' => $model->Kd_Ssh6,
                                          'Kd_Satuan' => $model->Kd_Satuan,
                                          'Kd_User' => $model->Kd_User,
                                    ]);

                            $url2 = Url::to(['usulan-ssh/tolak-usulan',
                                          'id' => $model->id,
                                          'Kd_Ssh1' => $model->Kd_Ssh1,
                                          'Kd_Ssh2' => $model->Kd_Ssh2,
                                          'Kd_Ssh3' => $model->Kd_Ssh3,
                                          'Kd_Ssh4' => $model->Kd_Ssh4,
                                          'Kd_Ssh5' => $model->Kd_Ssh5,
                                          'Kd_Ssh6' => $model->Kd_Ssh6,
                                          'Kd_Satuan' => $model->Kd_Satuan,
                                          'Kd_User' => $model->Kd_User,
                                    ]);

                            $btn1 = '<a href="'.$url1.'" title="Terima" class="terima_usulanssh" data-tujuan="'.$url1.'"><i class="fa fa-check-square-o"></i></a>';

                            $btn2 = '<a href="'.$url2.'" title="Tolak" class="tolak_usulanssh" data-tujuan="'.$url2.'"><i class="fa fa-times"></i></a>';

                            $btn = $btn1." ".$btn2;

                            if (Helper::checkRoute('usulan-ssh/terima-usulan')) : 
                                return $btn;
                            endif;


                            // $btn = $btn1." ".$btn2;
                            // return $btn;

                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>   
</div>