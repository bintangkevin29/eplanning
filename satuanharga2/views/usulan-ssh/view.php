<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UsulanSsh */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usulan Sshes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usulan-ssh-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'Kd_Ssh1',
            'Kd_Ssh2',
            'Kd_Ssh3',
            'Kd_Ssh4',
            'Kd_Ssh5',
            'Kd_Ssh6',
            'Nama_Barang',
            'Spesifikasi:ntext',
            'Kd_Satuan',
            'Satuan',
            'Harga_Satuan',
            'Kd_User',
            'Status',
        ],
    ]) ?>

</div>
