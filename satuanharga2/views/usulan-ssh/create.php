<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UsulanSsh */

$this->title = 'Create Usulan Ssh';
$this->params['breadcrumbs'][] = ['label' => 'Usulan Sshes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usulan-ssh-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ssh1' => $ssh1,
        'dataSatuan' => $dataSatuan,
    ]) ?>

</div>
