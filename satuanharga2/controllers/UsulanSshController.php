<?php

namespace satuanharga\controllers;

use Yii;
use common\models\UsulanSsh;
use common\models\search\UsulanSshSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\RefSsh1;
use common\models\RefSsh;
use yii\helpers\ArrayHelper;
use common\models\RefStandardSatuan;

/**
 * UsulanSshController implements the CRUD actions for UsulanSsh model.
 */
class UsulanSshController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsulanSsh models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsulanSshSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsulanSsh model.
     * @param integer $id
     * @param integer $Kd_Ssh1
     * @param integer $Kd_Ssh2
     * @param integer $Kd_Ssh3
     * @param integer $Kd_Ssh4
     * @param integer $Kd_Ssh5
     * @param integer $Kd_Ssh6
     * @param integer $Kd_Satuan
     * @param integer $Kd_User
     * @return mixed
     */
    public function actionView($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User),
        ]);
    }

    /**
     * Creates a new UsulanSsh model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsulanSsh();

        $model->Status = '0';
        $model->Kd_User = Yii::$app->user->identity->id;

        $ssh = RefSsh1::find()->all();
        $ssh1 = [];
        foreach($ssh as $key => $value){
            $ssh1[$value['Kd_Ssh1']]=$value['Kd_Ssh1'].". ".$value['Nm_Ssh1'];
        }

        $dataSatuan = ArrayHelper::map(RefStandardSatuan::find()
                    ->orderBy('Uraian')
                    ->all(),
                    'Kd_Satuan', 'Uraian');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'ssh1' => $ssh1,
                'dataSatuan' => $dataSatuan,
            ]);
        }
    }

    public function actionTerimaUsulan($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User) {

        $model = $this->findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User);

        $model->Status = '1';

        if ($model->save()) {

            $ssh6 = new RefSsh;
            $ssh6->Kd_Ssh1 = $model->Kd_Ssh1;
            $ssh6->Kd_Ssh2 = $model->Kd_Ssh2;
            $ssh6->Kd_Ssh3 = $model->Kd_Ssh3;
            $ssh6->Kd_Ssh4 = $model->Kd_Ssh4;
            $ssh6->Kd_Ssh5 = $model->Kd_Ssh5;
            $ssh6->Kd_Ssh6 = $model->Kd_Ssh6;
            $ssh6->Nama_Barang  = $model->Nama_Barang;
            $ssh6->Kd_Satuan    = $model->Kd_Satuan;
            $ssh6->Satuan       = $model->Satuan;
            $ssh6->Harga_Satuan = $model->Harga_Satuan;
            $ssh6->save(false);

            return $this->redirect(['view', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User]);
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionTolakUsulan($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User) {

        $model = $this->findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User);

        $model->Status = '2';

        if ($model->save()) {
            
            return $this->redirect(['view', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing UsulanSsh model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $Kd_Ssh1
     * @param integer $Kd_Ssh2
     * @param integer $Kd_Ssh3
     * @param integer $Kd_Ssh4
     * @param integer $Kd_Ssh5
     * @param integer $Kd_Ssh6
     * @param integer $Kd_Satuan
     * @param integer $Kd_User
     * @return mixed
     */
    public function actionUpdate($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User)
    {
        $model = $this->findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'Kd_Ssh1' => $model->Kd_Ssh1, 'Kd_Ssh2' => $model->Kd_Ssh2, 'Kd_Ssh3' => $model->Kd_Ssh3, 'Kd_Ssh4' => $model->Kd_Ssh4, 'Kd_Ssh5' => $model->Kd_Ssh5, 'Kd_Ssh6' => $model->Kd_Ssh6, 'Kd_Satuan' => $model->Kd_Satuan, 'Kd_User' => $model->Kd_User]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsulanSsh model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $Kd_Ssh1
     * @param integer $Kd_Ssh2
     * @param integer $Kd_Ssh3
     * @param integer $Kd_Ssh4
     * @param integer $Kd_Ssh5
     * @param integer $Kd_Ssh6
     * @param integer $Kd_Satuan
     * @param integer $Kd_User
     * @return mixed
     */
    public function actionDelete($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User)
    {
        $this->findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsulanSsh model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $Kd_Ssh1
     * @param integer $Kd_Ssh2
     * @param integer $Kd_Ssh3
     * @param integer $Kd_Ssh4
     * @param integer $Kd_Ssh5
     * @param integer $Kd_Ssh6
     * @param integer $Kd_Satuan
     * @param integer $Kd_User
     * @return UsulanSsh the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $Kd_Ssh1, $Kd_Ssh2, $Kd_Ssh3, $Kd_Ssh4, $Kd_Ssh5, $Kd_Ssh6, $Kd_Satuan, $Kd_User)
    {
        if (($model = UsulanSsh::findOne(['id' => $id, 'Kd_Ssh1' => $Kd_Ssh1, 'Kd_Ssh2' => $Kd_Ssh2, 'Kd_Ssh3' => $Kd_Ssh3, 'Kd_Ssh4' => $Kd_Ssh4, 'Kd_Ssh5' => $Kd_Ssh5, 'Kd_Ssh6' => $Kd_Ssh6, 'Kd_Satuan' => $Kd_Satuan, 'Kd_User' => $Kd_User])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
