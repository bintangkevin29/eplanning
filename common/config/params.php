<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'softwareBy' => 'Software By Team IT Bappeda Kabupaten Labuhanbatu</a>. ',
    'websiteCustomer' => 'http://labuhanbatu.go.id/',
    'version' => '1.0-2.1.10',
    
    'sessionTimeoutSeconds' => '100000',
];
