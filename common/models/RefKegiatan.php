<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ref_Kegiatan".
 *
 * @property integer $Kd_Urusan
 * @property integer $Kd_Bidang
 * @property integer $Kd_Prog
 * @property integer $Kd_Keg
 * @property string $Ket_Kegiatan
 *
 * @property RefJenisUsulan[] $refJenisUsulans
 * @property RefProgram $kdUrusan
 */
class RefKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 'Kd_Keg', 'Ket_Kegiatan'], 'required'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 'Kd_Keg'], 'integer'],
            [['Ket_Kegiatan'], 'string'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Prog'], 'exist', 'skipOnError' => true, 'targetClass' => RefProgram::className(), 'targetAttribute' => ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Prog' => 'Kd_Prog']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Urusan' => 'Kode  Urusan',
            'Kd_Bidang' => 'Kode  Bidang',
            'Kd_Prog' => 'Kode  Program',
            'Kd_Keg' => 'Kode  Kegiatan',
            'Ket_Kegiatan' => 'Kegiatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefJenisUsulans()
    {
        return $this->hasMany(RefJenisUsulan::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Prog' => 'Kd_Prog', 'Kd_Keg' => 'Kd_Keg']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdUrusan()
    {
        return $this->hasOne(RefProgram::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Prog' => 'Kd_Prog']);
    }

    public function getUrusan()
    {
        return $this->hasOne(RefUrusan::className(), ['Kd_Urusan' => 'Kd_Urusan']);
    }

    public function getBidang()
    {
        return $this->hasOne(RefBidang::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang']);
    }

    public function getProgram()
    {
        return $this->hasOne(RefProgram::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Prog' => 'Kd_Prog']);
    }




    /**
     * @inheritdoc
     * @return \common\models\query\RefKegiatanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RefKegiatanQuery(get_called_class());
    }
}
