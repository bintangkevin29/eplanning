<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ref_kamus_usulan".
 *
 * @property integer $id_kamus_usulan
 * @property string $nama_kamus_usulan
 * @property string $satuan
 * @property string $harga_satuan
 * @property string $skpd_pelaksana
 */
class RefKamusUsulan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_kamus_usulan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_kamus_usulan', 'satuan', 'harga_satuan', 'skpd_pelaksana'], 'required'],
            [['harga_satuan'], 'integer'],
            [['nama_kamus_usulan', 'skpd_pelaksana'], 'string', 'max' => 100],
            [['satuan'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kamus_usulan' => 'Id Kamus Usulan',
            'nama_kamus_usulan' => 'Nama Kamus Usulan',
            'satuan' => 'Satuan',
            'harga_satuan' => 'Harga Satuan',
            'skpd_pelaksana' => 'Skpd Pelaksana',
        ];
    }
}
