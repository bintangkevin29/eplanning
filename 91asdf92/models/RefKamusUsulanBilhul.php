<?php

namespace app\models;
use app\models\Skpds;
use app\models\RefStandardSatuan;
use app\models\RefBidangPembangunan;
use app\models\TaRpjmdPrioritasPembangunanDaerah;
use Yii;

/**
 * This is the model class for table "ref_kamus_usulan_bilhul".
 *
 * @property integer $id_kamus_usulan
 * @property string $nama_kamus_usulan
 * @property integer $satuan
 * @property double $harga_satuan
 * @property integer $id_skpd
 * @property integer $kd_bidang_pem
 * @property string $asal_usulan
 */
class RefKamusUsulanBilhul extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_kamus_usulan_bilhul';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_kamus_usulan', 'satuan', 'harga_satuan', 'id_skpd', 'kd_bidang_pem','kd_prioritas_pem' ,'asal_usulan'], 'required'],
            [['satuan', 'id_skpd', 'kd_bidang_pem','kd_prioritas_pem'], 'integer'],
            [['harga_satuan'], 'number'],
            [['nama_kamus_usulan', 'asal_usulan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kamus_usulan' => 'Id Kamus',
            'nama_kamus_usulan' => 'Kamus Usulan',
            'satuan' => 'Satuan',
            'harga_satuan' => 'Harga Satuan',
            'id_skpd' => 'Skpd',
            'kd_bidang_pem' => 'Bidang Pembangunan',
            'kd_prioritas_pem' => 'Prioritas Pembangunan',
            'asal_usulan' => 'Asal Usulan',
        ];
    }

     public function getSkpd(){
        return $this->hasOne(Skpds::className(),['id' => 'id_skpd']);
    }

    public function getSatuan1(){
        return $this->hasOne(RefStandardSatuan::className(),['Kd_Satuan' => 'satuan']);
    }

    public function getPembangunan(){
        return $this->hasOne(RefBidangPembangunan::className(),['Kd_Pem' => 'kd_bidang_pem']);
    }

    public function getPrioritas(){
        return $this->hasOne(TaRpjmdPrioritasPembangunanDaerah::className(),['No_Prioritas' => 'kd_prioritas_pem']);
    }
}
