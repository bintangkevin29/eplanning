<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RefKamusUsulanBilhul;

/**
 * RefKamusUsulanBilhulSearch represents the model behind the search form about `app\models\RefKamusUsulanBilhul`.
 */
class RefKamusUsulanBilhulSearch extends RefKamusUsulanBilhul
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kamus_usulan', 'satuan', 'id_skpd', 'kd_bidang_pem'], 'integer'],
            [['nama_kamus_usulan', 'asal_usulan'], 'safe'],
            [['harga_satuan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefKamusUsulanBilhul::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kamus_usulan' => $this->id_kamus_usulan,
            'satuan' => $this->satuan,
            'harga_satuan' => $this->harga_satuan,
            'id_skpd' => $this->id_skpd,
            'kd_bidang_pem' => $this->kd_bidang_pem,
        ]);

        $query->andFilterWhere(['like', 'nama_kamus_usulan', $this->nama_kamus_usulan])
            ->andFilterWhere(['like', 'asal_usulan', $this->asal_usulan]);

        return $dataProvider;
    }
}
