<?php

namespace app\models;
use app\models\TaSkpd;
use app\models\RefStandardSatuan;
use app\models\RefBidangPembangunan;
use app\models\TaRpjmdPrioritasPembangunanDaerah;
use app\models\Skpds;
use Yii;

/**
 * This is the model class for table "ref_kamus_usulan".
 *
 * @property integer $id_kamus_usulan
 * @property string $nama_kamus_usulan
 * @property string $satuan
 * @property string $harga_satuan
 * @property string $id_skpd
 */
class RefKamusUsulan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_kamus_usulan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_kamus_usulan', 'satuan', 'harga_satuan','kd_bidang_pem','kd_prioritas_pem','id_skpd'], 'required'],
            [['harga_satuan'], 'number'],
            [['satuan','kd_bidang_pem','kd_prioritas_pem','id_skpd'], 'integer'],
            [['nama_kamus_usulan'], 'string', 'max' => 100],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kamus_usulan' => 'Id Kamus',
            'nama_kamus_usulan' => 'Nama Kamus',
            'satuan' => 'Satuan',
            'harga_satuan' => 'Harga Satuan',
            'id_skpd' => 'Sub Unit/SKPD',
            'kd_bidang_pem' => 'Bidang Pembangunan',
            'kd_prioritas_pem' => 'Prioritas Pembangunan'
        ];
    }
    // id = ta_skpd dan id_skpd = refkamususulan
   
    public function getSatuan1(){
        return $this->hasOne(RefStandardSatuan::className(),['Kd_Satuan' => 'satuan']);
    }

    public function getPembangunan(){
        return $this->hasOne(RefBidangPembangunan::className(),['Kd_Pem' => 'kd_bidang_pem']);
    }

    public function getPrioritas(){
        return $this->hasOne(TaRpjmdPrioritasPembangunanDaerah::className(),['No_Prioritas' => 'kd_prioritas_pem']);
    }

    public function getSkpd(){
        return $this->hasOne(Skpds::className(),['id' => 'id_skpd']);
    }
}
