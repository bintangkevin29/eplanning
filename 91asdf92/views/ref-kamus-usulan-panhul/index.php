<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\i18n\numberFormatter;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RefKamusUsulanPanhulSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile(
    '@web/js/sistem/kamus_skrip.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->title = 'Kamus Usulan Panai Hulu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-panhul-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Kamus Usulan Panai Hulu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kamus_usulan',
            'nama_kamus_usulan',
            // 'satuan',
            // 'harga_satuan',
            // 'id_skpd',
            // 'kd_bidang_pem',
            [
                'attribute' => 'satuan',
                'value' => function($data){return $data->satuan1->Uraian;}
            ],

            //'harga_satuan',
            [
                //'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'harga_satuan',
                'value' => function ($model) { return number_format("".$model->harga_satuan, "2", ",", ".");}
                
            ],

            [
                'attribute' => 'id_skpd',
                'value' => function($data){return $data->skpd->skpd;}
            ],
            //'skpd_pelaksana',
            //'kd_bidang_pem',
            [
                'attribute' => 'kd_bidang_pem',
                'value' => function($data){return $data->pembangunan->Bidang_Pembangunan;}
            ],

            [
                'attribute' => 'kd_prioritas_pem',
                'value' => function($data){return $data->prioritas->Prioritas_Pembangunan_Daerah;}
            ],
            
            'asal_usulan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
