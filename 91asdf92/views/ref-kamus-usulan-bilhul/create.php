<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanBilhul */

$this->title = 'Tambah Kamus Usulan Bilah Hulu';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Bilah Hulu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-bilhul-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
