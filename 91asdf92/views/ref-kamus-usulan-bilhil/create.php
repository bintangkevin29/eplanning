<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanBilhil */

$this->title = 'Tambah Kamus Usulan Bilah Hilir';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Bilah Hilir', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-bilhil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
