<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widget\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanteng */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-kamus-usulan-panteng-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= $form->field($model, 'nama_kamus_usulan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                   
                         <?= $form->field($model, 'satuan')->dropDownList(
                            ArrayHelper::map(app\models\RefStandardSatuan::find()->all(),'Kd_Satuan','Uraian'),['prompt' => 'Pilih Satuan', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'harga_satuan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

                        <?= $form->field($model, 'id_skpd')->dropDownList(
                            ArrayHelper::map(app\models\Skpds::find()->all(),'id','skpd'),['prompt' => 'Pilih SKPD', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'kd_bidang_pem')->dropDownList(ArrayHelper::map(app\models\RefBidangPembangunan::find()->all(),'Kd_Pem','Bidang_Pembangunan'),['prompt' => 'Pilih Bidang', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'kd_prioritas_pem')->dropDownList(ArrayHelper::map(app\models\TaRpjmdPrioritasPembangunanDaerah::find()->all(),'No_Prioritas','Prioritas_Pembangunan_Daerah'),['prompt' => 'Pilih Bidang', 'class' => 'form-control select2-allow-clear']) 
                        ?>

    <?= $form->field($model, 'asal_usulan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
