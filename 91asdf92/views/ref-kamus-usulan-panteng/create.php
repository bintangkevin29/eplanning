<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanteng */

$this->title = 'Tambah Kamus Usulan Panai Tengah';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Panai Tengah', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-panteng-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
