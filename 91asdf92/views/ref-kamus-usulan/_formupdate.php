<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widget\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulan */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="ref-kamus-usulan-form">
    <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-container">
                    <div class=" widget-block">


                        
                        <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>

                        
                        
                                                
                        <?= $form->field($model, 'nama_kamus_usulan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                   
                         <?= $form->field($model, 'satuan')->dropDownList(
                            ArrayHelper::map(app\models\RefStandardSatuan::find()->all(),'Kd_Satuan','Uraian'),['prompt' => 'Pilih Satuan', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'harga_satuan')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

                        
                        <div class="form-group ">
                    <label class="control-label col-sm-3">PD Penanggungjawab</label>
                    <div class="col-sm-6">
                        <?= Html::dropDownList('skpd', null, $dataunit, ['prompt'=>'Pilih SKPD','class' => 'form-control select2-allow-clear','id' => 'pdaerah', 'readonly' => true]) ?>
                    </div>
                </div>

                        <?= $form->field($model, 'kd_bidang_pem')->dropDownList(ArrayHelper::map(app\models\RefBidangPembangunan::find()->all(),'Kd_Pem','Bidang_Pembangunan'),['prompt' => 'Pilih Bidang', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <?= $form->field($model, 'kd_prioritas_pem')->dropDownList(ArrayHelper::map(app\models\TaRpjmdPrioritasPembangunanDaerah::find()->all(),'No_Prioritas','Prioritas_Pembangunan_Daerah'),['prompt' => 'Pilih Bidang', 'class' => 'form-control select2-allow-clear']) 
                        ?>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="total"></label>
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                 </div>
            </div>
       
         </div>

    </div>

</div>

<div class="modal fade" id="modal_ssh" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <?php $form = ActiveForm::begin([
                            'method' => 'post',
                            'action' => ['refkamususulan/pilihssh'],
                        ]) 
          ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pilih SSH</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="kd_usulan" id="kd_usulan_input">
                <div class="form-group">
                  <label >Nama Jalan</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Jalan">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tutup</button>
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
            </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>



<div class="modal fade" id="modal_hspk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pilih HSPK</h4>
            </div>
            <div class="modal-body">
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tutup</button>
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
            </div>
          
        </div>
    </div>
</div>