<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RefKamusUsulanRanselSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kamus Usulan Rantau Selatan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-ransel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Kamus Usulan Rantau Selatan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kamus_usulan',
            'nama_kamus_usulan',
            // 'satuan',
            // 'harga_satuan',
            // 'id_skpd',
            // 'kd_bidang_pem',

            [
                'attribute' => 'satuan',
                'value' => function($data){return $data->satuan1->Uraian;}
            ],

            //'harga_satuan',
            [
                //'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'harga_satuan',
                'value' => function ($model) { return number_format("".$model->harga_satuan, "2", ",", ".");}
                
            ],

            [
                'attribute' => 'id_skpd',
                'value' => function($data){return $data->skpd->skpd;}
            ],
            //'skpd_pelaksana',
            //'kd_bidang_pem',
            [
                'attribute' => 'kd_bidang_pem',
                'value' => function($data){return $data->pembangunan->Bidang_Pembangunan;}
            ],
            
            [
                'attribute' => 'kd_prioritas_pem',
                'value' => function($data){return $data->prioritas->Prioritas_Pembangunan_Daerah;}
            ],
            
             'asal_usulan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
