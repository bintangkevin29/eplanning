<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanRansel */

$this->title = 'Tambah Kamus Usulan Rantau Selatan';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Rantau Selatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-ransel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
