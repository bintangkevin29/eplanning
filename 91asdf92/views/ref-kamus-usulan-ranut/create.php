<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanRanut */

$this->title = 'Tambah Kamus Usulan Rantau Utara';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Rantau Utara', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-ranut-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
