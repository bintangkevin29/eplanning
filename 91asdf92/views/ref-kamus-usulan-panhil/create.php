<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanPanhil */

$this->title = 'Tambah Kamus Usulan Panai Hilir';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Panai Hilir', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-panhil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
