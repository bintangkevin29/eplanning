<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKamusUsulanBilbar */

$this->title = 'Tambah Kamus Usulan Bilah Barat';
$this->params['breadcrumbs'][] = ['label' => 'Kamus Usulan Bilbars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kamus-usulan-bilbar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
