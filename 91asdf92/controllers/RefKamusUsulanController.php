<?php

namespace app\controllers;

use Yii;
use app\models\RefKamusUsulan;
use app\models\search\RefKamusUsulanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RefSsh;
use app\models\RefHspk;
use app\models\RefAsb;

use yii\helpers\ArrayHelper;
use app\models\RefSubUnit;

/**
 * RefKamusUsulanController implements the CRUD actions for RefKamusUsulan model.
 */
class RefKamusUsulanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefKamusUsulan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RefKamusUsulanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPilihssh()
    {
        $model = new RefKamusUsulan();
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamus_usulan]);
        } else {

            return $this->render('_formssh', [
                'model' => $model,
            ]);
        }
        //return $this->render('_formssh');
    }

    
    /**
     * Displays a single RefKamusUsulan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefKamusUsulan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefKamusUsulan();
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id_kamus_usulan]);
            
            
        } else {

            return $this->render('create', [
                'model' => $model,
                //'dataunit' => $dataunit,
            ]);
        }
    }

    /**
     * Updates an existing RefKamusUsulan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamus_usulan]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefKamusUsulan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefKamusUsulan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefKamusUsulan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefKamusUsulan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
