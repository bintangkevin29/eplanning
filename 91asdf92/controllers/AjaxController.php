<?php

namespace kamususulan\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RefBidang;
use app\models\TaSubUnitJab;
use common\models\Ta;
use app\models\ProgramNasional;
use app\models\PrioritasNasional;
use app\models\Nawacita;
use app\models\Urusan;
use app\models\Misi;
use app\models\RefUnit;
use app\models\RefKamusProgram;
use common\models\RefRekAset1;
use common\models\RefRekAset2;
use common\models\RefRekAset3;
use common\models\RefRekAset4;
use common\models\RefRekAset5;
use common\models\RefStandardHarga1;
use common\models\RefStandardHarga2;
use common\models\RefSsh1;
use common\models\RefSsh2;
use common\models\RefSsh3;
use common\models\RefSsh4;
use common\models\RefSsh5;
use common\models\RefSsh;
use common\models\RefHspk1;
use common\models\RefHspk2;
use common\models\RefHspk3;
use common\models\RefHspk;
use common\models\RefAsb1;
use common\models\RefAsb2;
use common\models\RefAsb3;
use common\models\RefAsb4;
use common\models\RefAsb;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\helpers\Json;
/**
 * TaSubUnitController implements the CRUD actions for TaSubUnit model.
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionProvinsi(){
        $items = \yii\helpers\ArrayHelper::map(\common\models\RefProvinsi::find()->all(),'Kd_Prov','Nm_Prov');
        return $items;
    }

}
