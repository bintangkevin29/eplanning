<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'username')->textInput(['readonly'=>'readonly']) ?>
                <?= $form->field($model, 'password_hash')->passwordInput()->label('Password') ?>
                <div class="form-group">
                    <?php echo Html::submitButton('Reset', [ 'class' => 'btn btn-primary', 'name' => 'selesai' ]); ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
