<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Edit User';

$this->registerJs(
    "$('#kec').change(function(){
        var prov=$('#prov').val();
        var kab=$('#kab').val();
        var kec=$(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getkel')."&Kd_Kec='+kec,function(data){
            $('#kel').html(data);
        });
    });"
);

$this->registerJs(
    "$('#kel').change(function(){
        var prov=$('#prov').val();
        var kab=$('#kab').val();
        var kec=$('#kec').val();
        var kel=$(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getling')."&Kd_Kec='+kec+'&Kd_Urut_Kel='+kel,function(data){
            $('#ling').html(data);
        });
    });"
);

$this->registerJs(
    "$('#dapil').change(function(){
        var dapil=$(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/getdewan')."&Kd_Dapil='+dapil,function(data){
            $('#dewan').html(data);
        });
    });"
);

$this->registerJs(
    "$('#jenis_user').change(function(){
        var jenis=$(this).val();
        $.post('".Yii::$app->urlManager->createUrl('ajax/setlevel')."&jenis='+jenis,function(data){
            $('#level').html(data);
        });
        
    });"
);

// $this->registerJs(
//     "$('#komisi').change(function(){
//         alert($(this).val());
//     });"
// );

$this->registerJs(
    "$('#aks').change(function(){
        var aks = $(this).val();
        if (aks == 'Operator_Kecamatan') {
            $('#dropKec').show();
            $('#dropKel').hide();
            $('#dropLing').hide();
            $('#dropSkpd').hide();
            $('#dropDapil').hide();
            $('#dropDewan').hide();
            $('#dropFraksi').hide();
            $('#dropKomisi').hide();
        }
        else if (aks == 'Operator_Kelurahan') {
            $('#dropKec').show();
            $('#dropKel').show();
            $('#dropLing').hide();
            $('#dropSkpd').hide();
            $('#dropDapil').hide();
            $('#dropDewan').hide();
            $('#dropFraksi').hide();
            $('#dropKomisi').hide();
        }
        else if (aks == 'Operator_Lingkungan') {
            $('#dropKec').show();
            $('#dropKel').show(); 
            $('#dropLing').show();
            $('#dropSkpd').hide();
            $('#dropDapil').hide();
            $('#dropDewan').hide();
            $('#dropFraksi').hide();
            $('#dropKomisi').hide();
        }
        else if (aks == 'Operator_Skpd') {
            $('#dropKec').hide();
            $('#dropKel').hide(); 
            $('#dropLing').hide();
            $('#dropSkpd').show();
            $('#dropDapil').hide();
            $('#dropDewan').hide();
            $('#dropFraksi').hide();
            $('#dropKomisi').hide();
        }
        else if (aks == 'Operator_Bappeda') {
            $('#dropKec').hide();
            $('#dropKel').hide(); 
            $('#dropLing').hide();
            $('#dropSkpd').hide();
            $('#dropDapil').hide();
            $('#dropDewan').hide();
            $('#dropFraksi').hide();
            $('#dropKomisi').hide();
        }
        else if (aks == 'Operator_Pokir') {
            $('#dropDapil').show();
            $('#dropDewan').show();
            $('#dropFraksi').show();
            $('#dropKomisi').show();
            $('#dropKec').hide();
            $('#dropKel').hide(); 
            $('#dropLing').hide();
            $('#dropSkpd').hide();
        }
    });"
);

// use yii\helpers\ArrayHelper;
// use common\models\RefKecamatan;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="user-form">
                    <?php $form = ActiveForm::begin(); ?>

                        <?php echo $form->field($model_user, 'username')->textInput(['autofocus' => true]) ?>

                        <?php echo $form->field($model_user, 'email') ?>

                        <div class="form-option" id="dropSkpd" style="padding-bottom: 10px;">
                            <?= $form->field($model, 'skpd')->dropdownlist($dataSkpd,
                            [
                                'prompt' => 'Pilih Skpd',
                                'class' => 'dependent-input form-control',
                                'id' => 'skpd'
                            ]); ?>
                        </div>

                       <div class="form-group">
                            <?php echo Html::submitButton('Simpan', [ 'class' => 'btn btn-primary', 'name' => 'selesai' ]); ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


