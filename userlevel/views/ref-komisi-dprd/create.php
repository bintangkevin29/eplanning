<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RefKomisiDprd */

$this->title = 'Create Ref Komisi Dprd';
$this->params['breadcrumbs'][] = ['label' => 'Ref Komisi Dprds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-komisi-dprd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
