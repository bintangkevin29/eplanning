<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefKomisiDprd */

$this->title = 'Update Ref Komisi Dprd: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ref Komisi Dprds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Komisi' => $model->Kd_Komisi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-komisi-dprd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
