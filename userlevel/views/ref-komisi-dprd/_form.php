<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RefKomisiDprd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-komisi-dprd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Komisi')->textInput() ?>

    <?= $form->field($model, 'Nm_Komisi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Keterangan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
