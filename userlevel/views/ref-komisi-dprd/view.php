<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RefKomisiDprd */

$this->title = $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Ref Komisi Dprds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-komisi-dprd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tahun' => $model->Tahun, 'Kd_Komisi' => $model->Kd_Komisi], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tahun' => $model->Tahun, 'Kd_Komisi' => $model->Kd_Komisi], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_Komisi',
            'Nm_Komisi:ntext',
            'Keterangan:ntext',
        ],
    ]) ?>

</div>
